@extends('layouts.app',['page'=>'edituser-view'])

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-16 col-sm-18 col-md-offset-4 col-sm-offset-3">
                <hr>
                <div class="row">
                    <div class="col-sm-8">
                        <ul class="nav nav-pills nav-stacked" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#basic" aria-controls="basic" role="tab" data-toggle="tab">Informações básicas</a>
                            </li>
                            <li role="presentation">
                                <a href="#pass" aria-controls="pass" role="tab" data-toggle="tab">Alterar a senha</a>
                            </li>
                            <li role="presentation">
                                <a href="#social" aria-controls="social" role="tab" data-toggle="tab">Redes sociais</a>
                            </li>
                            <li role="presentation">
                                <a href="#account" aria-controls="account" role="tab" data-toggle="tab">Conta</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-sm-16">
                        <div class="row">
                            <div class="container-fluid">
                                @if (Session::has('message'))
                                    <div class="alert alert-success dismissable">{{ Session::get('message') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                @if (Session::has('error'))
                                    <div class="alert alert-danger dismissable">{{ Session::get('error') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane active" id="basic">
                                                <h4 class="title">Informações básicas</h4>
                                                {{Form::open(['url'=>url('users/store?t=basic'),'files'=>'true'])}}
                                                    <input type="hidden" name="rotation">
                                                    <div class="form-group">
                                                        <label for="inputName" class="control-label">Nome</label>
                                                        <input name="name" id="inputName" type="text" class="form-control" value="{{$user->name}}">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="inputSurname" class="control-label">Sobrenome</label>
                                                        <input name="surname" id="inputSurname" type="text" class="form-control" value="{{$user->surname}}">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="" class="control-label">Nascimento</label>
                                                        <input name="birth" id="birthdate" type="text" class="form-control" value="{{$user->birthdate}}" >
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="inputImage" class="control-label">Imagem</label>
                                                        <div class="row">
                                                            <div class="container-fluid text-center">
                                                                <div id="user-image-cropper">
                                                                    <div class="row">
                                                                        <div class="col-xs-24 clearfix">
                                                                            <label for="cropit-image-input-user" class="btn btn-default select-image-btn">Escolher uma imagem</label>
                                                                            <input name="user_image" type="file" id="cropit-image-input-user" class="cropit-image-input" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="container-fluid">
                                                                            <div class="cropit-preview-container">
                                                                                <div class="cropit-preview"></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="image-controls hidden">
                                                                        <div class="row">
                                                                            <div class="container-fluid">
                                                                                <div class="crop-range-container">
                                                                                    <i class="fa fa-image small"></i><input type="range" class="cropit-image-zoom-input" />
                                                                                    <i class="fa fa-image"></i>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="container-fluid">
                                                                                <div class="rotation-btns clearfix">
                                                                                    <button type="button" class="btn btn-default btn-sm rotate-ccw-btn">
                                                                                        <span class="fa fa-rotate-left"></span>
                                                                                    </button>
                                                                                    <button type="button" class="btn btn-default btn-sm rotate-cw-btn rotate-cw-btn">
                                                                                        <span class="fa fa-rotate-right"></span>
                                                                                    </button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <hr>
                                                    <div class="form-group">
                                                        <button type="submit" class="btn btn-danger pull-right">Salvar</button>
                                                    </div>
                                                {{Form::close()}}
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="pass">
                                                <h4 class="title">Alterar a senha</h4>
                                                {{Form::open(['url'=>url('users/store?t=pass')])}}
                                                    <div class="form-group">
                                                        <label for="inputCurPass" class="control-label">Senha atual</label>
                                                        <input name="current_pass" id="inputCurPass" type="password" class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="inputPass" class="control-label">Nova senha</label>
                                                        <input name="password" id="inputPass" type="password" class="form-control">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="inputPassConf" class="control-label">Repita a nova senha</label>
                                                        <input name="password_confirmation" id="inputPassConf" type="password" class="form-control">
                                                    </div>
                                                    <hr>
                                                    <div class="form-group">
                                                        <button class="btn btn-danger pull-right">Salvar</button>
                                                    </div>
                                                {{Form::close()}}
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="social">
                                                <h4 class="title">Redes sociais</h4>
                                                {{Form::open(['url'=>url('users/store?t=social')])}}
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <div class="input-group-addon addon-social">
                                                                <i class="fa fa-globe"></i>
                                                            </div>
                                                            <input value="{{@$user->social->website}}" type="text" name="link[website]" class="form-control" placeholder="Website">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <div class="input-group-addon addon-social btn-facebook">
                                                                <i class="fa fa-facebook"></i>
                                                            </div>
                                                            <input value="{{@$user->social->facebook}}" type="text" name="link[facebook]" class="form-control" placeholder="Facebook">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <div class="input-group-addon addon-social btn-twitter">
                                                                <i class="fa fa-twitter"></i>
                                                            </div>
                                                            <input value="{{@$user->social->twitter}}" type="text" name="link[twitter]" class="form-control" placeholder="Twitter">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <div class="input-group-addon addon-social btn-google-plus">
                                                                <i class="fa fa-google-plus"></i>
                                                            </div>
                                                            <input value="{{@$user->social->google_plus}}" type="text" name="link[google_plus]" class="form-control" placeholder="Google+">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <div class="input-group-addon addon-social btn-pinterest">
                                                                <i class="fa fa-pinterest"></i>
                                                            </div>
                                                            <input value="{{@$user->social->pinterest}}" type="text" name="link[pinterest]" class="form-control" placeholder="Pinterest">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <div class="input-group-addon addon-social btn-linkedin">
                                                                <i class="fa fa-linkedin"></i>
                                                            </div>
                                                            <input value="{{@$user->social->linkedin}}" type="text" name="link[linkedin]" class="form-control" placeholder="Linkedin">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <div class="input-group-addon addon-social addon-social btn-behance">
                                                                <i class="fa fa-behance"></i>
                                                            </div>
                                                            <input value="{{@$user->social->behance}}" type="text" name="link[behance]" class="form-control" placeholder="Behance">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <div class="input-group-addon addon-social btn-instagram">
                                                                <i class="fa fa-instagram"></i>
                                                            </div>
                                                            <input value="{{@$user->social->instagram}}" type="text"  name="link[instagram]"class="form-control" placeholder="Instagram">
                                                        </div>
                                                    </div>
                                                    <hr>
                                                    <div class="form-group">
                                                        <button class="btn btn-danger pull-right">Salvar</button>
                                                    </div>
                                                {{Form::close()}}
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="account">
                                                <h4 class="title">Informações da conta</h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script-bottom')
    <script>
        $(function(){

            $('#user-image-cropper').cropit({
                //exportZoom: 1.25,
                initialZoom:'min',
                imageBackground: true,
                //imageBackgroundBorderWidth: 20,
                imageState: {
                  src: '{{$user->image_path}}',
                },
                onZoomChange: function(zoom){
                    var form = $('#user-image-cropper').closest('form');
                    var z = form.find('input[name=zoom]');
                    if(!z.length){
                        form.prepend($('<input name="zoom" type="hidden">'));
                        var z = form.find('input[name=zoom]');
                    }

                    z.val(zoom);
                },
                onOffsetChange: function(offset){
                    var form = $('#user-image-cropper').closest('form');
                    var x = form.find('input[name=x]');
                    var y = form.find('input[name=y]');
                    if(!x.length){
                        form.prepend($('<input name="x" type="hidden">'));
                        var x = form.find('input[name=x]');
                    }
                    if(!y.length){
                        form.prepend($('<input name="y" type="hidden">'));
                        var y = form.find('input[name=y]');
                    }

                    x.val(offset.x);
                    y.val(offset.y);
                },
                onImageLoaded: function(){
                    var form = $('#user-image-cropper').closest('form');

                    $('.image-controls').removeClass('hidden');
                },
                onImageError:function(e){
                    var form = $('#user-image-cropper').closest('form');

                    var $el = form.find('#cropit-image-input-user');
                    $el.wrap('<form>').closest('form').get(0).reset();
                    $el.unwrap();

                    if(e.code == 1){
                        $('.select-image-btn').before('<div class="alert alert-warning alert-small-image">A imagem é muito pequena</div>');
                        setTimeout(function(){
                            $('.alert-small-image').remove();
                        },5000);
                    }
                }
            });

            // Handle rotation
            $('.rotate-cw-btn').on('click',function() {
                $('#user-image-cropper').cropit('rotateCW');
                var r = getRotationDegrees($('#user-image-cropper .cropit-preview-image'));
                $('input[name=rotation]').val(r);
            });
            $('.rotate-ccw-btn').on('click',function() {
                $('#user-image-cropper').cropit('rotateCCW');
                var r = getRotationDegrees($('#user-image-cropper .cropit-preview-image'));
                $('input[name=rotation]').val(r);
            });


            function getRotationDegrees(obj) {
                var matrix = obj.css("-webkit-transform") ||
                obj.css("-moz-transform")    ||
                obj.css("-ms-transform")     ||
                obj.css("-o-transform")      ||
                obj.css("transform");
                if(matrix !== 'none') {
                    var values = matrix.split('(')[1].split(')')[0].split(',');
                    var a = values[0];
                    var b = values[1];
                    var angle = Math.round(Math.atan2(b, a) * (180/Math.PI));
                } else { var angle = 0; }
                return (angle < 0) ? angle + 360 : angle;
            }

        }); 
    </script>
@endpush
