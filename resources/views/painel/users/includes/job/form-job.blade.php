
{{Form::open(['url'=>url('/user/resume/job/'.@$job->id)])}}
@if(@$job)
	{{Form::hidden('_method','PUT')}}
@endif
<div class="form-group">
	<label for="" class="control-label">
		Título
	</label>
	{{Form::text('job_name',@$job->title,['class'=>'form-control input-sm','tabindex'=>'1'])}}
</div>
<div class="form-group">
	<label for="" class="control-label">
		Empresa
	</label>
	{{Form::text('company_name',@$job->company_name,['class'=>'form-control input-sm','tabindex'=>'1'])}}
</div>
<div class="form-group">
	<label for="" class="control-label">
		Cidade
	</label>
	<p class="clearfix">
		{{Form::text('job_city',@$job->city ? @$job->city->name.' - '.@$job->city->state->code:'',['class'=>'form-control input-sm city-typeahead','tabindex'=>'1'])}}
	</p>
</div>
<div class="form-group">
	<label for="" class="control-label">
		Período
	</label>
	<div class="row">
		<div class="time-range">
			<div class="container-fluid">
				<div class="time-from">
					<div class="month-from">
						{{Form::select('job_month_from',months(),date('m',strtotime(@$job->start)),['class'=>'form-control input-sm','tabindex'=>'1'])}}
					</div>
					<div class="year-from">
						{{Form::select('job_year_from',years(),date('Y',strtotime(@$job->start)),['class'=>'form-control input-sm','tabindex'=>'1'])}}
					</div>
				</div>
				<div class="range-until">
					até
				</div>
				<div class="time-to">
					<div class="time-to-container @if(!@$job->end) hidden @endif">
						<div class="month-to">
							{{Form::select('job_month_to',months(),date('m',strtotime(@$job->end)),['class'=>'form-control input-sm','tabindex'=>'1'])}}
						</div>
						<div class="year-to">
							{{Form::select('job_year_to',years(),date('Y',strtotime(@$job->end)),['class'=>'form-control input-sm','tabindex'=>'1'])}}
						</div>
					</div>

					<div class="until-now-text @if(@$job->end) hidden @endif">agora</div>

					<label for="{{@$job?@$job->id.'-':''}}job-until-now-select" class="until-now-label">
						<input @if(!@$job->end) checked @endif tabindex="1" class="styled-checkbox until-now-checkbox" id="{{@$job?@$job->id.'-':''}}job-until-now-select" type="checkbox" name="until_now">
						Estou neste trabalho
					</label>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="form-group">
	<label for="" class="control-label">
		Descrição
	</label>
	{{Form::textarea('job_description',@$job->description,['rows'=>'4','class'=>'form-control input-sm','tabindex'=>'1'])}}
</div>
<hr>
@include('painel.users.includes.resume-item-form-buttons',['item'=>@$job])
@if(old('city_id') || @$job)
    {{Form::hidden('city_id',old('city_id')?:$job->city_id)}}
@endif
{{Form::close()}}