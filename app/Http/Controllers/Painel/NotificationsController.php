<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class NotificationsController extends Controller
{
    public function getLasts($page = 1)
    {
        $type = \Request::get('type'); // notnotified

        // MUDA S NOTIFICAÇÕES PARA NOTIFICADAS PORQUE O USUÁRIO CLICOU NO INCONE DO GLOBO
        $notNotifiedNotifications = \App\Models\Notification::where('notified',0)
        ->where('receiver_id',@\Auth::user()->id)
        ->update(['notified' => TRUE ]);

        // CARREGA AS ULTIMAS NOTIFICAÇÕES PARA MOSTRAR PAR AO USUÁRIO
    	$perPage = 10;
    	$notifications = \App\Models\Notification::where('receiver_id',\Auth::user()->id)
            ->with('sender')
    		->skip($perPage * ($page-1))
    		->take($perPage)
            ->orderBy('id', 'DESC')
    		->get();

    	return response()->json(['notifications'=>$notifications]);
    }
}
