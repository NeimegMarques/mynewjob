<div class="">
    {{Form::open(['url'=>route('painel'),'method'=>'GET','id'=>'sidebar-job-search-form'])}}
        <div class="form-group clearfix">
            <button class="btn btn-success btn-block">
                Buscar
            </button>
        </div>
        <div class="form-group">
            <label for="" class="control-label">Palavras chave</label>
            <input type="text" name="k" value="{{@Input::get('k')}}" class="form-control input-sm">
        </div>
        <div class="form-group clearfix select-city-after-container">
            <label for="" class="control-label">Cidade</label>
            <input type="text" name="cn" value="{{@Input::get('cn')}}" class="city-typeahead form-control input-sm" placeholder="Cidade">
        </div>
        <!-- <div class="form-group">
            <hr>
            <label for="" class="control-label">Distância - <span id="show-distance"></span></label>
            <div id="slider-job-distance"></div>
        </div> -->
        <div class="form-group">
            <label for="" class="control-label">Regime de trabalho</label>
            <ul class="nav nav-stacked">
                @foreach($jobtypes as $jt)
                <li>
                    <label for="checkbox-jobtype-{{$jt->id}}">
                        <input name="jt[]" value="{{$jt->id}}" class="styled-checkbox" type="checkbox" id="checkbox-jobtype-{{$jt->id}}" @if(@in_array($jt->id, @Input::get('jt'))) checked @endif>
                        {{$jt->name}}
                    </label>
                </li>
                @endforeach
            </ul>
        </div>
        <div class="form-group">
            <label for="" class="control-label">Faixa salarial</label>
            <ul class="nav nav-stacked">
                <?php $salaries = ['1000','1500','2000','2500','3000','3500']?>
                @foreach($salaries as $sl)
                <li>
                    <label for="radio-salary-{{$sl}}">
                        <input name="sl" value="{{$sl}}" class="styled-radio" type="radio" id="radio-salary-{{$sl}}" {{@Input::get('sl') == $sl ? 'checked' :''}}>
                        A partir de R${{number_format($sl,2,',','.')}}
                    </label>
                </li>
                @endforeach
                <li>
                    <label for="radio-salary-comb">
                        <input name="sl" value="combinar" class="styled-radio" type="radio" id="radio-salary-comb" {{@Input::get('sl') == 'combinar' ? 'checked' :''}}>
                        A combinar
                    </label>
                </li>
            </ul>
        </div>
        <div class="form-group">
            <label for="" class="control-label">Data de publicação</label>
            <ul class="nav nav-stacked">
                <li>
                    <label for="radio-pub-date-ind">
                        <input class="styled-radio" name="pd" value="ind" type="radio" id="radio-pub-date-ind" {{@Input::get('pd') == 'ind' ? 'checked' :''}}>
                        Indiferente
                    </label>
                </li>
                <li>
                    <label for="radio-pub-date-1-month">
                        <input class="styled-radio" name="pd" value="1m" type="radio" id="radio-pub-date-1-month" {{@Input::get('pd') == '1m' ? 'checked' :''}}>
                        Último mês
                    </label>
                </li>
                <li>
                    <label for="radio-pub-date-1-week">
                        <input class="styled-radio" name="pd" value="1w" type="radio" id="radio-pub-date-1-week" {{@Input::get('pd') == '1w' ? 'checked' :''}}>
                        Última semana
                    </label>
                </li>
                <li>
                    <label for="radio-pub-date-2-days">
                        <input class="styled-radio" name="pd" value="2d" type="radio" id="radio-pub-date-2-days" {{@Input::get('pd') == '2d' ? 'checked' :''}}>
                        Últimos 2 dias
                    </label>
                </li>
                <li>
                    <label for="radio-pub-date-1-day">
                        <input class="styled-radio" name="pd" value="1d" type="radio" id="radio-pub-date-1-day" {{@Input::get('pd') == '1d' ? 'checked' :''}}>
                        Último dia
                    </label>
                </li>
            </ul>
        </div>
        <div class="form-group clearfix">
            <hr>
            <button class="btn btn-success btn-block">
                Buscar
            </button>
        </div>
        {{Form::hidden('city_id',@Input::get('city_id'))}}
    {{Form::close()}}
</div>