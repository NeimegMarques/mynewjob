<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('city_id')->nullable()->unsigned();
            $table->string('neighborhood')->default('');
            $table->string('street')->default('');
            $table->string('number')->default('');
            $table->string('complement')->default('');
            $table->string('zip_code')->default('');
            $table->string('longitude')->default('');
            $table->string('latitude')->default('');
            $table->string('image')->default('');
            $table->timestamps();

            $table->foreign('city_id')->references('id')->on('cities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('companies');
    }
}
