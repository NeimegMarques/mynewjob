<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAddressFieldsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function ($table) {
            $table->string('complement')->after('zip_code')->nullable();
            $table->string('neighborhood')->after('zip_code')->nullable();
            $table->string('number')->after('zip_code')->nullable();
            $table->string('street')->after('zip_code')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function ($table) {
            $table->dropColumn('complement');
            $table->dropColumn('neighborhood');
            $table->dropColumn('number');
            $table->dropColumn('street');
        });
    }
}
