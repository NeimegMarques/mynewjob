<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PortfolioController extends Controller
{
    public function show($portfolio)
    {
    	$portfolio = \App\Models\Portfolio::where('slug',$portfolio)->first();
		return view('painel.portfolio.show')
            ->withPortfolio($portfolio);
    }

    public function edit($portfolio)
    {
    	$portfolio = \Auth::user()->portfolios->where('slug',$portfolio)->first();
		return view('painel.portfolio.show')
            ->withPortfolio($portfolio)
            ->withEditing(true);
    }
}
