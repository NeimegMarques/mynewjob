<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Follow extends Model
{
    public function follower()
    {
    	return $this->belongsTo('App\User');
    }

    public function followed()
    {
    	return $this->hasMany('App\User','followed_id');
    }
}
