<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationsTable extends Migration
{

    public function up()
    {
        Schema::create('notifications', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('sender_id')->unsigned()->index()->nullable();
            $table->integer('receiver_id')->unsigned()->index();
            $table->string('type');
            $table->string('message');
            $table->string('link');
            $table->boolean('seen')->default(0);
            $table->boolean('notified')->default(0);
            $table->timestamps();

            $table->foreign('sender_id')->references('id')->on('users')->onDelete('CASCADE')->onUpdate('cascade');
            $table->foreign('receiver_id')->references('id')->on('users')->onDelete('CASCADE')->onUpdate('cascade');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('notifications');
    }
}
