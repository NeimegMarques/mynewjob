<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\City;

class CitiesController extends Controller
{
    public function getByState(){

    	$state_id = \Input::get('state_id');
    	$cities = City::where('state_id',$state_id)->get();
    	if(\Request::json()){
    		return $cities->toJson();
    	}
    }

    public function getByQuery($query){
        if($query == 'São Paulo'){
            $query = 'SãoPaulo';
        }
    	$cities = City::where('name','LIKE','%'.$query.'%')->with('state')->limit(10)->get();
    	if(\Request::json()){
    		return $cities->toJson();
    	}	
    }
}
