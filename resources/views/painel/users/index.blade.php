@extends('layouts.app',['page'=>'user-view'])

@section('content')
<section class="bg-primary">
    <div class="container">
        {{Form::open(['url'=>url('/'),'class'=>'home-search-form'])}}
            <div class="row">
                <div class="col-sm-11">
                    <input placeholder="Palavras chave" type="text" class="form-control input-lg">
                </div>
                <div class="col-sm-11">
                    <input placeholder="Cidade" type="text" class="city-typeahead tt-query form-control input-lg">
                </div>
                <div class="col-sm-2">
                    <button class="btn btn-success btn-lg btn-block">
                        <i class="fa fa-search"></i>
                    </button>
                </div>
            </div>
        {{Form::close()}}
    </div>
</section>
<section class="jobs">
	<div class="container">
        <div class="row">
            <div class="col-sm-5">
                <div class="">
                    {{Form::open(['url'=>route('painel'),'method'=>'GET','id'=>'sidebar-job-search-form'])}}
                        <div class="form-group clearfix">
                            <hr>
                            <button class="btn btn-success btn-block">
                                Buscar
                            </button>
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label">Palavras chave</label>
                            <input type="text" class="form-control input-sm">
                        </div>
                        <div class="form-group">
                            <label for="" class="control-label">Estado</label>
                            <select name="" id="select-state-before" class="form-control input-sm">
                                @foreach($states as $est)
                                    <option value="{{$est->id}}">{{$est->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group select-city-after-container">
                            <label for="" class="control-label">Cidade</label>
                            <select name="" id="select-city-after" class=" form-control input-sm">
                                <option value="">Antes selecione o estado</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <hr>
                            <label for="" class="control-label">Distância - <span id="show-distance"></span></label>
                            <div id="slider-job-distance"></div>
                        </div>
                        <div class="form-group">
                            <hr>
                            <label for="" class="control-label">Regime de trabalho</label>
                            <ul class="nav nav-stacked">
                                @foreach($jobtypes as $jt)
                                <li>
                                    <label for="checkbox-jobtype-{{$jt->id}}">
                                        <input class="styled-checkbox" type="checkbox" id="checkbox-jobtype-{{$jt->id}}">
                                        {{$jt->name}}
                                    </label>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="form-group">
                            <hr>
                            <label for="" class="control-label">Faixa salarial</label>
                            <ul class="nav nav-stacked">
                                <?php $salaries = ['1000','1500','2000']?>
                                @foreach($salaries as $sl)
                                <li>
                                    <label for="radio-salary-{{$sl}}">
                                        <input name="salary" class="styled-radio" type="radio" id="radio-salary-{{$sl}}">
                                        A partir de R${{number_format($sl,2,',','.')}}
                                    </label>
                                </li>
                                @endforeach
                                <li>
                                    <label for="radio-salary-comb">
                                        <input name="salary" class="styled-radio" type="radio" id="radio-salary-comb">
                                        A combinar
                                    </label>
                                </li>
                            </ul>
                        </div>
                        <div class="form-group">
                            <hr>
                            <label for="" class="control-label">Data de publicação</label>
                            <ul class="nav nav-stacked">
                                <li>
                                    <label for="radio-pub-date-1-month">
                                        <input class="styled-radio" name="pub_date" type="radio" id="radio-pub-date-1-month">
                                        Último mês
                                    </label>
                                </li>
                                <li>
                                    <label for="radio-pub-date-1-week">
                                        <input class="styled-radio" name="pub_date" type="radio" id="radio-pub-date-1-week">
                                        Última semana
                                    </label>
                                </li>
                                <li>
                                    <label for="radio-pub-date-2-days">
                                        <input class="styled-radio" name="pub_date" type="radio" id="radio-pub-date-2-days">
                                        Últimos 2 dias
                                    </label>
                                </li>
                                <li>
                                    <label for="radio-pub-date-1-day">
                                        <input class="styled-radio" name="pub_date" type="radio" id="radio-pub-date-1-day">
                                        Último dia
                                    </label>
                                </li>
                            </ul>
                        </div>
                        <div class="form-group clearfix">
                            <hr>
                            <button class="btn btn-success btn-block">
                                Buscar
                            </button>
                        </div>
                    {{Form::close()}}
                </div>
            </div>
            <div class="col-sm-19">
                <div class="job-list">
                    @for($i=0;$i<4;$i++)
                    <div class="media job well job--summary">
                        
                        <div class="job-options">
                            <button class="btn btn-default dropdown-toggle" type="button" id="job-{{$i}}-option-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                <i class="fa fa-chevron-down"></i>
                            </button>
                            <ul class="dropdown-menu pull-right" aria-labelledby="job-{{$i}}-option-dropdown">
								<li><a href="#">Denunciar</a></li>
                                <li><a href="#">Sugerir correção</a></li>
                          </ul> 
                        </div>

                        <div class="media-left">
                            <a href="#">
                                <img class="media-object" src="http://vignette4.wikia.nocookie.net/smurfs/images/a/a1/Netflix-logo.png/revision/latest?cb=20150508223333" alt="...">
                            </a>
                        </div>
                        <div class="media-body">
                            <h3 class="media-heading">Avaliador de Séries</h3>
                            <p>Netflix <span class="text-muted"><i class="text-info fa fa-map-marker"></i> Rio de Janiero - RJ</span></p>

                            <div class="job-description">
                                Lorem ipsum Sint sint aliqua qui dolor Excepteur sed amet ullamco minim ullamco in do esse nulla dolor nulla consequat veniam sint dolore ea sit dolore aute.
                            </div>
                            <hr>
                            <div class="job-info">
                                <p>3 horas atrás</p>
                            </div>
                            <div class="job-tags">
                                <div class="label label-primary">PHP</div>
                                <div class="label label-primary">CSS</div>
                                <div class="label label-primary">LESS</div>
                                <div class="label label-primary">SASS</div>
                            </div>
                        </div>
                    </div>
                    @endfor
                </div> 
            </div>
        </div>  
    </div>
</section>
@endsection
