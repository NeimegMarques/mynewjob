<?php

use Illuminate\Database\Seeder;

class ExperienceLevelsTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('experiencelevels')->delete();

		$experiencelevels = [
			['name'=>'Voluntário','slug'=>str_slug('Voluntário')],
			['name'=>'Iniciante','slug'=>str_slug('Iniciante')],
			['name'=>'Estagiário','slug'=>str_slug('Estagiário')],
			['name'=>'Trainee','slug'=>str_slug('Trainee')],
			['name'=>'Júnior','slug'=>str_slug('Júnior')],
			['name'=>'Pleno','slug'=>str_slug('Pleno')],
			['name'=>'Sênior','slug'=>str_slug('Sênior')],
			['name'=>'Diretor','slug'=>str_slug('Diretor')],
			['name'=>'Gerente','slug'=>str_slug('Gerente')],
			['name'=>'Sócio','slug'=>str_slug('Sócio')]
		];

		DB::table('experiencelevels')->insert($experiencelevels);
    }
}
