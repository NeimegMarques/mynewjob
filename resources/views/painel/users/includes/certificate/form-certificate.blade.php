
{{Form::open(['url'=>url('/user/resume/certificate/'.@$certificate->id)])}}
@if(@$certificate)
	{{Form::hidden('_method','PUT')}}
@endif
<div class="form-group">
	<label for="" class="control-label">
		Curso
	</label>
	{{Form::text('certificate_name',@$certificate->title,['class'=>'form-control input-sm','tabindex'=>'1'])}}
</div>
<div class="form-group">
	<label for="" class="control-label">
		Entidade
	</label>
	{{Form::text('entity_name',@$certificate->entity_name,['class'=>'form-control input-sm','tabindex'=>'1'])}}
</div>
<div class="form-group">
	<label for="" class="control-label">
		Descrição
	</label>
	{{Form::textarea('certificate_description',@$certificate->description,['rows'=>'4','class'=>'form-control input-sm','tabindex'=>'1'])}}
</div>
<hr>
@include('painel.users.includes.resume-item-form-buttons',['item'=>@$certificate])
{{Form::close()}}