@extends('layouts.front',['page'=>'front'])
@section('content')
<section class="banner">
    <h1 class="banner-frase">As melhores vagas estão aqui</h1>
    <div class="banner-btns">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-sm-offset-6">
                    <div class="row">
                        <div class="col-sm-12 col-sm-offset-6">
                            <a class="btn btn-block btn-lg btn-danger float-right" href="{{url('register')}}">Quero uma vaga</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="callout">
    <div class="container">
        <div class="row">
            <div class="col-sm-18 col-sm-offset-3">
                <div class="row">
                    <div class="col-sm-12 text-right">
                        <h1>Aumente suas chances</h1>
                        <p>Publique um vídeo de apresentação, assim você aumenta suas chances de contratação.</p>
                        <p>Quanto mais detalhes você fornecer, mais completa será sua apresentação e sua entrevista online será um sucesso.</p>
                        <p>
                            <br>
                            <a href="{{url('register')}}" class="btn btn-danger btn-lg">Cadastre seu currículo</a>
                        </p>
                    </div>
                    <div class="col-sm-12">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item" width="100%" src="https://www.youtube.com/embed/zPWHHhKqUiQ" frameborder="0" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="counters">
    <div class="container">
        <div class="row">
            <div class="col-md-18 col-md-offset-3">
                <div class="row">
                    <div class="col-sm-8">
                        <div class="counter text-center">
                            <div class="counter-number" data-number="{{$jobs}}">
                                <span></span>
                            </div>
                            <div class="counter-text">vagas</div>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="counter text-center">
                            <div class="counter-number" data-number="{{$companies}}">
                                <span></span>
                            </div>
                            <div class="counter-text">empresas</div>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="counter text-center">
                            <div class="counter-number" data-number="{{ $users*2 /* Hehehe*/ }}">
                                <span></span>
                            </div>
                            <div class="counter-text">usuários</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-md-offset-9">
                <a href="{{url('register')}}" class="join btn btn-danger btn-lg btn-block">Junte-se a nós</a>
            </div>
        </div>
    </div>
</section>
<section class="callout">
    <div class="container text-upper text-center">
        <h3>novas vagas todos os dias, a próxima pode ser sua!</h3>
    </div>
</section>
<section class="vacancies">
    <div class="container">
        <h1 class="section-title text-center text-primary">Vagas recentes</h1>
        <div class="row">
            <div class=" col-md-20 col-md-offset-2 col-sm-22 col-sm-offset-1">
                <table class="table new-vacancies">
                    <tbody>
                         <tr class="job">
                            <th class="job-name"><span class="text-muted">Vaga</span></td>
                            <th class="job-location"><span class="text-muted">Cidade</span></th>
                            <th class="job-type"><span class="text-muted">Tipo</span></th>
                            <th class="job-salary"><span class="text-muted">Salário</span></th>
                        </tr>
                        @foreach($new_vacancies as $vacancy)
                        <tr class="job">
                            <td class="job-name">
                                <a href="{{url('v/'.@$vacancy->id)}}">{{@$vacancy->name}}</a>
                            </td>
                            <td class="job-location">
                                @if(@$vacancy->city)
                                    <i class="fa fa-map-marker"></i>
                                    {{@$vacancy->city->name}}</td>
                                @endif
                            <td class="job-type"><span class="label label-danger">{{@$vacancy->jobtype->name}}</span></td>
                            <td class="job-salary text-success"><b>{{ @$vacancy->salary_description}}</b></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
<section class="pricings">
    <div class="container">
        <div class="row">
            <div class="col-sm-18 col-sm-offset-3">
            	<!-- Tab panes -->
            	<div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="seeking">
                        <div class="row">
                            <div class="col-sm-8">
                                <!-- PRICE ITEM -->
                                <div class="panel price panel-default">
                                    <div class="panel-heading  text-center">
                                        <h3>GRÁTIS</h3>
                                    </div>
                                    <div class="panel-body text-center">
                                        <p class="price-value">R$0,00<span class="mes">/mês</span></p>
                                    </div>
                                    <ul class="price-details list-group list-group-flush text-center">
                                        <li class="list-group-item">
                                            <i class="icon-ok text-danger"></i>
                                            Uma disputa por vaga por semana
                                        </li>
                                        <li class="list-group-item">
                                            <i class="icon-ok text-danger"></i> 
                                            Sem envio de emails
                                        </li>
                                        <li class="list-group-item">
                                            <i class="icon-ok text-danger"></i>
                                            Sem acesso ao mapa de vagas
                                        </li>
                                        <li class="list-group-item">
                                            <i class="icon-ok text-danger"></i>
                                            Sem chat
                                        </li>
                                    </ul>
                                    <div class="panel-footer">
                                        <a class="btn btn-lg btn-block btn-default" href="{{url('register?p=gratis')}}">
                                            Entrar agora
                                        </a>
                                    </div>
                                </div>
                                <!-- /PRICE ITEM -->
                            </div>
                            <div class="col-sm-8">
                                <!-- PRICE ITEM -->
                                <div class="panel price price--main panel-success">
                                    <div class="panel-heading  text-center">
                                        <h3>PRÓ</h3>
                                    </div>
                                    <div class="panel-body text-center">
                                        <p class="price-value">R${{number_format(env('PRO_PRICE')/100,2,',','.')}}<span class="mes">/mês</span></p>
                                    </div>
                                    <ul class="price-details list-group list-group-flush text-center" style="color:#ccc;">
                                        <li class="list-group-item">
                                            <i class="icon-ok text-danger"></i>
                                            Acesso 30 disputas por semana
                                        </li>
                                        <li class="list-group-item">
                                            <i class="icon-ok text-danger"></i> 
                                            Emails de todas as transações
                                        </li>
                                        <li class="list-group-item">
                                            <i class="icon-ok text-danger"></i> 
                                            Acesso ao mapa de vagas
                                        </li>
                                        <li class="list-group-item">
                                            <i class="icon-ok text-danger"></i>
                                            Acesso ao chat
                                        </li>
                                        <li class="list-group-item">
                                            <i class="icon-ok text-danger"></i>
                                            Pode salvar e favoritar vagas
                                        </li>
                                        <li class="list-group-item">
                                            <i class="icon-ok text-danger"></i>
                                            E mais uma porrada de coisas
                                        </li>
                                    </ul>
                                    <div class="panel-footer">
                                        <a class="btn btn-lg btn-block btn-success" href="{{url('register?p=pro')}}">
                                            Contratar agora
                                        </a>
                                    </div>
                                </div>
                                <!-- /PRICE ITEM -->
                            </div>
                            <div class="col-sm-8">
                                <!-- PRICE ITEM -->
                                <div class="panel price panel-info">
                                    <div class="panel-heading  text-center">
                                        <h3>VIP</h3>
                                    </div>
                                    <div class="panel-body text-center">
                                        <p class="price-value">R${{number_format(env('VIP_PRICE')/100,2,',','.')}}<span class="mes">/mês</span></p>
                                    </div>
                                    <ul class="price-details list-group list-group-flush text-center" style="color:#ccc;">
                                        <li class="list-group-item">
                                            <i class="icon-ok text-danger"></i>
                                            Disputas ilimitas por vagas
                                        </li>
                                        <li class="list-group-item">
                                            <i class="icon-ok text-danger"></i> 
                                            Emails de todas as transações
                                        </li>
                                        <li class="list-group-item">
                                            <i class="icon-ok text-danger"></i>
                                            Acesso ao mapa de vagas
                                        </li>
                                        <li class="list-group-item">
                                            <i class="icon-ok text-danger"></i>
                                            Todas os recursos PRO
                                        </li>
                                    </ul>
                                    <div class="panel-footer">
                                        <a class="btn btn-lg btn-block btn-info" href="{{url('register?p=vip')}}">
                                            Contratar agora
                                        </a>
                                    </div>
                                </div>
                                <!-- /PRICE ITEM -->
                            </div>
                        </div>
                    </div>
            	</div>
            </div>
        </div>

    </div>
</section>
@endsection
