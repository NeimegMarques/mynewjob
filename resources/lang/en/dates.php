<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'week'  =>  [
        'Sunday',
        'Monday',
        'Tuesday',
        'Wednesday',
        'Thursday',
        'Friday',
        'Saturday'
    ],

    'months' =>  [
        1       =>  "January",
        2       =>  "February",
        3       =>  "March",
        4       =>  "April",
        5       =>  "May",
        6       =>  "June",
        7       =>  "July",
        8       =>  "August",
        9       =>  "September",
        10      =>  "October",
        11      =>  "November",
        12      =>  "December"
    ]

];
