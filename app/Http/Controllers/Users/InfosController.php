<?php

namespace App\Http\Controllers\Users;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\User;
use App\Models\State;

class InfosController extends Controller
{
    public function getInfoForm($info){

        switch ($info) {
            case 'zip_code':
                return view('registering.zip-code-form');
                break;
            case 'tipo':
                return view('registering.account-type-form');
                break;
            case 'location':
                $brasil = \App\Models\Country::where('name','Brazil')->first();
                $states = $brasil->states;
                return view('registering.location-form')->with(compact('states'));
                break;
        }

    	// $states = State::all();
    	// return view('registering.location-form')
    	// 	->withStates($states);
    }
}
