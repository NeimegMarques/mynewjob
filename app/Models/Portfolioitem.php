<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Portfolioitem extends Model
{
	public $appends = ['imagePath','thumb'];
    public function getImagePathAttribute(){
        return asset('storage/uploads/portfolios/'.$this->portfolio_id.'/'.@$this->image);
    }
    public function getThumbAttribute(){
    	$arr = explode('.', $this->image);
    	$ext = array_pop($arr);
    	$name = implode('.', $arr);
        return asset('storage/uploads/portfolios/'.$this->portfolio_id.'/'.@$name.'_thumb.'.$ext);
    }
}
