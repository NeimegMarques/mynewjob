<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Benefit;

class BenefitsController extends Controller
{
    public function getByQueryWithFake($query){

    	$benefit = Benefit::where('name', $query)->limit(10)->first();
    	$benefits = Benefit::where('name','LIKE','%'.$query.'%')->get();

    	$benefits = $benefits->map(function($item){
    		return $item->setAttribute('str_id',$item->id);
    	});

    	if(!$benefit)
    	{
    		$free = new Benefit;
	    	$free->name = $query.' (Criar)';
	    	$free->str_id = $query;
	    	$benefits->add($free);
    	}

    	if(\Request::json()){
    		return $benefits->toJson();
    	}
    }
}
