<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Jobtype;
use App\Models\State;

use Carbon\Carbon;

class HomeController extends Controller
{
    public function searchByQuery(Request $request, $query)
    {
        return $this->index($request, $query);
    }
    public function index(Request $request, $query = NULL){
        $search = \App\Models\Vacancy::orderBy('created_at','DESC')->active();

    	if($request->k || $query)
    	{
            $query = $request->k?: $query;
    		$ks = explode(' ',$query);
    		$search->where(function($search) use ($ks){
    			$search->where(function($search) use ($ks){
	    			foreach($ks as $i => $k)
		    		{
		    			if($i==0)
		    			{
			    			$search->where('name','LIKE','%'.$k.'%');	
		    			}
		    			else
		    			{
			    			$search->orWhere('name','LIKE','%'.$k.'%');	
		    			}
		    		}
	    		})->orWhere(function($search) use ($ks){
	    			$search->whereHas('tags', function ($q) use ($ks) {
						foreach($ks as $i => $k)
		    			{
		    				if($i==0)
			    			{	
				    			$q->where('name', 'LIKE', '%'.$k.'%');
			    			}
			    			else
			    			{
				    			$q->orWhere('name', 'LIKE', '%'.$k.'%');
			    			}   
						}
					});
	    		})->orWhere(function($search) use ($ks){
                    $search->whereHas('company', function ($q) use ($ks) {
                        foreach($ks as $i => $k)
                        {
                            if($i==0)
                            {   
                                $q->where('name', 'LIKE', '%'.$k.'%');
                            }
                            else
                            {
                                $q->orWhere('name', 'LIKE', '%'.$k.'%');
                            }   
                        }
                    });
                });
    		});
    	}

    	if($request->city_id)
    	{
            $city_id = $request->city_id;
            $cn = $request->cn;
            $search->where(function($search) use ($request){
                if($request->cn)
        		{
    	    		$search->where('city_id',$request->city_id);
        		}
        		else
        		{
        			$request->merge(array('city_id' => ''));
                }
            });
    	}

    	if($request->jt)
    	{
            $search->where(function($search) use ($request){
        		foreach($request->jt as $i => $jt)
        		{
        			if($i==0)
        			{
    	    			$search->where('jobtype_id',$jt);	
        			}
        			else
        			{
    	    			$search->orWhere('jobtype_id',$jt);	
        			}
        		}
            });
    	}

    	if($request->sl)
    	{
            $search->where(function($search) use ($request){
        		if($request->sl == 'combinar')
        		{
        			$search->where('salary',0);	
        		}
        		else
        		{
        			$search->where('salary','>=',$request->sl);	
        		}
            });
    	}

    	if($request->pd)
    	{
            $search->where(function($search) use ($request){
        		$today = Carbon::today();
        		switch($request->pd)
        		{
        			case '1m': $date = $today->subMonths(1);
        				break;
        			case '1w': $date = $today->subWeeks(1);
        				break;
        			case '2d': $date = $today->subDays(2);
        				break;
        			case '1d': $date = $today->subDays(1);
        				break;	
        		}

        		if($request->pd != 'ind')
        		{
        			$search->whereDate('created_at','>=',$today->toDateString());	
        		}
            });
    	}

    	//\DB::enableQueryLog();
    	$vacancies = $search->paginate(8);
    	//echo '<pre>'; 
    	//print_r(\DB::getQueryLog());
    	//echo '</pre>';

        $jobtypes = Jobtype::all();
        $states = State::all();

        if($request->ajax())
        {
            return $vacancies->toJson();    
        }
        return view('painel.vacancy.index')
            ->withJobtypes($jobtypes)
            ->withStates($states)
            ->withVacancies($vacancies);
    }
}
