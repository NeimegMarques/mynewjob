<?php

namespace App\Http\Controllers\Posts;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PostsController extends Controller
{
    public function store( Request $request)
    {
    	$content = $request->post['text'];
        $image = $request->post['image'];
    	$hashtags = $request->post['tags'];
    	$post = new \App\Models\Post;
    	$post->content = sanitizeContent($content);
    	$post->user_id = \Auth::user()->id;

    	if($post->save()){
            if(count($hashtags)){
                $tags_ids = [];
                foreach ($hashtags as $key => $tag) {
                    $ht = \App\Models\Posttag::where('name',$tag)->first();
                    if($ht){
                        $tags_ids[] = $ht->id;
                    } else {
                        $ht = new \App\Models\Posttag;
                        $ht->name = sanitizeContent($tag);
                        $ht->save();
                        $tags_ids[] = $ht->id;
                    }
                }
                $post->hashtags()->sync($tags_ids);
            }

            @\File::makeDirectory(storage_path('app/public/uploads'));
            @\File::makeDirectory(storage_path('app/public/uploads/posts'));
            @\File::makeDirectory(storage_path('app/public/uploads/posts/'.$post->id));
            if($image != ''){
                $image = sanitizeContent($image);
                list($imgtype, $data) = explode(';', $image);
                list( ,$data)      = explode(',', $data);

                $filename = md5(date('ymdHis')).'-'.str_random(6);
                $extension = '.'.explode('/',$imgtype)[1];

                file_put_contents(storage_path('app/public/uploads/posts/'.$post->id.'/'.$filename.$extension), base64_decode($data));
                $img = \Image::make(storage_path('app/public/uploads/posts/'.$post->id.'/'.$filename.$extension));
                $img_w = $img->width();
                $img_h = $img->height();

                $img->widen(600, function ($constraint) {
                    $constraint->upsize();
                });
                $img->save(storage_path('app/public/uploads/posts/'.$post->id.'/'.$filename.$extension));

                $image = new \App\Models\Image;
                $image->user_id = \Auth::user()->id;
                $image->image = $filename.$extension;
                $post->images()->save($image);
            }

            return response()->json(['post'=>$post->load('user')]);
        }
        return response()->json(['errors'=>['Houve um erro ao tentar salvar o post']]);
    }

    public function getPosts($start = null)
    {
        $search = \App\Models\Post::orderBy('id','DESC');
    	if($start){
    		$search->where('id', '>', $start);
    	}
    	$posts = $search->limit(10)->get();

    	return response()->json(['posts'=>$posts->load('images')->load('user')->load('likes')->load('comments')->load('comments.comments')]);
    }

    public function like($post_id)
    {
        $post = \App\Models\Post::find($post_id);
        $liked = in_array(\Auth::user()->id, $post->likes()->pluck('user.id')->toArray());
        if($liked){
            $post->likes()->detach([\Auth::user()->id]);
            $liked = false;
        }else{
            $post->likes()->attach([\Auth::user()->id]);
            $liked = true;

            if(\Auth::user()->id != $post->user_id){
                $notification = [
                    'sender_id'=>@\Auth::user()->id,
                    'receiver_id'=>$post->user_id,
                    'type'=>'notification',
                    'message'=> 'curtiu a sua publicação',
                    'link'=>url('/timeline'),
                    'created_at'=>date('Y-m-d H:i:s'),
                    'updated_at'=>date('Y-m-d H:i:s')
                ];
                \App\Models\Notification::insert(@$notification);
            }
        }
        return response()->json(['liked'=>$liked]);
    }
}
