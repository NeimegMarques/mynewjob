@if(@\Auth::user()->is_me)
	<div class="resume-item-edit">
	    <button v-on:click="editResumeItem($event)" class="btn btn-edit-resume-item btn-default" type="button">
	        <i class="fa fa-pencil"></i>
	    </button>
	</div>
@endif