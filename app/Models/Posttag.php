<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Posttag extends Model
{
    public function users()
    {
    	return $this->belongsToMany('App\User');
    }

    public function posts()
    {
    	return $this->belongsToMany('App\Models\Post');
    }
}
