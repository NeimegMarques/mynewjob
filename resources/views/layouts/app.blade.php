
<!DOCTYPE html>
<html lang="en"
    stringdate="{{date('Y,n,j,G,i,s')}}"
    url="{{url('/')}}"
    uid="{{@Auth::user()->id}}"
    :me='{   
        id: "{{@Auth::user()->id}}",
        name: "{{@Auth::user()->name}}",
        surname: "{{@Auth::user()->surname}}",
        image: "{{@Auth::user()->image}}",
        slug: "{{@Auth::user()->slug}}"
    }'
    socket_id="{{ \Session::getId() }}"
    notnotifiednotifications="{{@$notnotifiednotifications}}"
    {{-- 
    notreadmessages="{{ @$notreadmessages}}" --}}
    v-bind:class="{'showchat':showchat}"
>
<head>
    @section('head_tags')
        <?php 
        $title = "MyNewJob - Novas vagas de emprego todos os dias. Garanta a sua!";
        $description = "MyNewJob é um site de vagas de emprego onde você pode facilmente criar seu currículo e aparecer para as empresas que estão procurando profisionais como você.";
        $keywords = "Empregos, Currículo, Vagas, Empresas, Recrutamento, RH, Recolocação";
        $url = \Request::fullUrl();
        $type = "website";
        $image = asset("/img/logo-icon.png");
        ?>
        @include("includes.head_tags")               
    @show
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-59248202-7', 'auto');
      ga('send', 'pageview');

    </script>
    @stack('script-head')
</head>
<body 
    data-url="{{url('/')}}" 
    data-uid="{{@Auth::user()->id}}" 
    data-sid="{{ \Session::getId() }}"
    class="
        @if(Request::is('portfolio/*')) portfolio @endif 
        @if(Request::is('u/*') && !Request::is('u/*/*')) user @endif 
    ">
    <div id="wrapper">
        <div class="main-wrapper" v-cloak>
            @include('includes._topbar')
            @if(Auth::check())
                <div class="suggestion-form-container hidden-xs">
                    {{Form::open(['url'=>url('/suggestion'), 'class'=>'suggestion-form'])}}
                        <div class="suggestion-header" v-bind:class="{'bg-blue':suggestion_type == 'suggestion','bg-red':suggestion_type == 'error'}" v-on:click="showSuggestionForm = !showSuggestionForm">
                            @{{suggestion_type == 'suggestion'? 'Tem alguma sugestão':'Encontrou algum erro'}}?
                            <span class="pull-right glyphicon glyphicon-menu-@{{showSuggestionForm ? 'down':'up'}}"></span>
                        </div>
                        <div class="suggestion-body text-center" v-if="suggestion_message">
                            @{{{suggestion_message}}}
                        </div>
                        <div class="suggestion-body" v-if="showSuggestionForm && !suggestion_message.length">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label class="form-label" for="suggestion_type2">
                                            <input type="radio" checked class="styled-radio" v-model="suggestion_type" value="suggestion" name="suggestion_type" id="suggestion_type2"> Sugestão
                                        </label>
                                    </div>
                                    <div class="col-sm-12">
                                        <label class="form-label" for="suggestion_type1">
                                            <input type="radio" class="styled-radio" v-model="suggestion_type" value="error" name="suggestion_type" id="suggestion_type1"> Erro
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <textarea name="suggestion" v-model="suggestion_text" id="" rows="8" class="form-control" placeholder="Qual é @{{suggestion_type == 'suggestion' ? 'a sugestão':'o erro'}}?"></textarea>
                            </div>
                            <div class="form-group clearfix">
                                <button v-on:click.prevent="sendSuggestion()" type="submit" v-bind:disabled="!suggestion_text.length || submiting_suggestion" v-bind:class="{'btn-info':suggestion_type == 'suggestion','btn-danger':suggestion_type == 'error', 'disabled':!suggestion_text.length || submiting_suggestion}"  class="btn pull-right">
                                    <span v-if="!submiting_suggestion">Enviar</span>
                                    <span v-else>Enviando...</span>
                                </button>
                            </div>
                        </div>
                    {{Form::close()}}
                </div>
            @endif
            <div class="main-messages-container">
            @if(session()->has('global-message'))
                <div class="main-messages-container-item">
                    <div class="container">
                        <div class="alert alert-success">
                            {{session('global-message')}}
                        </div>
                    </div>
                </div>
            @endif
            </div>
            <main>
                <component is="{{$page?:'home-view'}}" inline-template
                @if(count(@$props))
                    @foreach($props as $k => $prop)
                        {{$k}}="{{$prop}}"
                    @endforeach
                @endif
                :url="url"
                >
                @yield('content')
                </component>
            </main>
            <footer>
                <div class="container">
                    <div class="row xs-align-center">
                        <div class="col-sm-12">
                            &copy;2016 MyNewJob. Todos os direitos reservados
                        </div>
                        <div class="col-sm-12">
                            <ul class="nav nav-pills pull-right">
                                <li><a href="{{url('sobre')}}">sobre nós</a></li>
                                <li><a href="{{url('contato')}}">contato</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </footer>
            @if(@Auth::check())
                @include('includes._chat')
            @endif
            <!-- JavaScripts -->
            @if ( env('APP_ENV') == 'local' )
                <script type="text/javascript" src="{{ asset('js/socket.io.js') }}"></script>
            @endif

            <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/1.4.5/socket.io.min.js"></script>
            <script src="{{ elixir('js/script.js') }}"></script>
            @if ( Config::get('app.debug') )
              <script type="text/javascript">
                document.write('<script src="//localhost:35729/livereload.js?snipver=1" type="text/javascript"><\/script>')
              </script> 
            @endif
            
            <script> 
                // $(".send-msg").click(function(e){
                //     e.preventDefault();
                //     var token = $("input[name='_token']").val();
                //     var user = $("input[name='user']").val();
                //     var msg = $(".msg").val();
                //     if(msg != ''){
                //         $.ajax({
                //             type: "POST",
                //             url: '{!! URL::to("sendmessage") !!}',
                //             dataType: "json",
                //             data: {'_token':token,'message':msg,'user':user},
                //             success:function(data){
                //                 console.log(data);
                //                 $(".msg").val('');
                //             }
                //         });
                //     }else{
                //         alert("Please Add Message.");
                //     }
                // })
            </script>
            
            @stack('script-bottom')
            {{Form::input('hidden','csrf-token',csrf_token())}}
        </div>
    </div>
    @if(env('APP_ENV') != 'local')
    <script type="text/javascript">
        var _mfq = _mfq || [];
        (function() {
            var mf = document.createElement("script");
            mf.type = "text/javascript"; mf.async = true;
            mf.src = "//cdn.mouseflow.com/projects/b1a12b67-9d13-4af6-81a0-d68539383d9f.js";
            document.getElementsByTagName("head")[0].appendChild(mf);
        })();
    </script>
    @endif
</body>
</html>
