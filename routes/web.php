<?php

Route::post('/suggestion','Controller@suggestion');

Route::group(['prefix'=>'contratar'],function(){
	Route::get('/','SubscriptionsController@index');
	Route::post('plano','SubscriptionsController@setPlan');
});

Auth::routes();

Route::get('/','HomeController@index');

Route::get('/avatar','HomeController@avatar');

Route::get('/home',function(){
    return redirect('/painel');
});

Route::group(['namespace'=>'Timeline'], function(){
    Route::get('/timeline', 'TimelineController@index');
});

Route::group(['prefix'=>'post'], function(){
    Route::post('{id}/comment', 'CommentsController@store');
});

Route::group(['namespace'=>'Posts'], function(){
    Route::group(['prefix'=>'post'], function(){
        Route::post('/store', 'PostsController@store');
        Route::post('{id}/like', 'PostsController@like');
    });
});

Route::group(['middleware'=>['notConfirmedAccount']], function(){
    Route::get('confirm','HomeController@confirm');
    Route::get('resend','HomeController@resendConfirmEmail');
    Route::get('resend','HomeController@resendConfirmEmail');
    Route::get('register/confirm','HomeController@confirmAccount');
});

Route::group(['prefix'=>'/portfolio'], function(){
    Route::get('{portfolio}','PortfolioController@show');
    Route::get('{portfolio}/editar','PortfolioController@edit');
});

Route::group(['middleware'=>['auth']], function(){
    Route::get('pagar','PaymentController@index');
    Route::post('pagar','PaymentController@postPayment');

    Route::group(['prefix'=>'info','namespace'=>'Users'], function(){
        Route::get('{info}','InfosController@getInfoForm');
        Route::post('address','UsersController@setAddress');
        Route::post('account-type','UsersController@setAccontType');
        Route::post('city','UsersController@setCity');
    });

    Route::group(['prefix'=>'get'], function(){
        Route::get('cities-by-state','CitiesController@getByState');
        Route::get('cities-by-query/{query}','CitiesController@getByQuery');
        
        Route::get('users-by-query/{query}','Users\UsersController@getByQuery');
        Route::get('companies-by-query/{query}','Companies\CompaniesController@getByQuery');
        Route::get('vacancies-by-query/{query}','Painel\HomeController@searchByQuery');
        Route::get('tags/{query}','Painel\TagsController@getByQuery');
        // ESSA ROTA É ESPECIALMENTE PARA A EDIÇÂO DE TAGS
        // PORQUE RETORNA O TEXTO INSERIDO COMO SE FOSSE UMA TAG EXISTENTE
        Route::group(['namespace'=>'Painel'], function(){
            Route::get('tags-with-fake/{query}','TagsController@getByQueryWithFake');
            Route::get('benefits-with-fake/{query}','BenefitsController@getByQueryWithFake');
            Route::get('notifications','NotificationsController@getLasts');
            Route::get('chats','ChatsController@getLasts');
            Route::get('chat/{id}','ChatsController@getByPartner');
        });
    });

    Route::group(['middleware'=>['confirmedAccount','subscribed','RecruiterOrCandidate']], function(){
        Route::group(['middleware'=>['hasAllInfos']], function(){
            Route::group(['prefix'=>'painel','namespace'=>'Painel'], function(){
                Route::get('/','HomeController@index')->name('painel');
            });

            Route::group(['namespace'=>'Users'], function(){
                Route::group(['prefix'=>'u'], function(){
                    Route::get('{user}/editar','UsersController@edit');
                    Route::get('{user}/page/{page}','UsersController@getPage');
                    Route::post('/pageview', 'UsersController@pageview');
                    Route::post('/follow', 'UsersController@follow');
                    Route::group(['middleware'=>['auth']], function(){
                        Route::get('{slug}/cv', 'UsersController@exportCv');
                    });
                });
                Route::get('profissionais', 'UsersController@profissionais')->name('profissionais');
                Route::get('/minhas-vagas','UsersController@minhasVagas');
                Route::get('/vagas-criadas','UsersController@vagasCriadas');
            });

            Route::group(['prefix'=>'c','namespace'=>'Companies'], function(){
                Route::get('{company}','CompaniesController@show');
                Route::get('{company}/editar','CompaniesController@edit');
            });

            Route::group(['namespace'=>'Painel'], function(){
                Route::get('v/new','VacanciesController@create')->name('nova-vaga');
                Route::post('v','VacanciesController@store');
                Route::put('v/{id}','VacanciesController@update');
                Route::get('v/{id}/exportcvs/{slug?}','VacanciesController@exportcvs');
                Route::get('v/{id}/edit','VacanciesController@edit')->name('editar-vaga');
                Route::get('v/{id}/toggle','VacanciesController@toggle');
                Route::post('v/apply','VacanciesController@newApply');
                Route::post('v/giveup','VacanciesController@giveUp');
            });

            Route::group(['prefix'=>'user','namespace'=>'Users'], function(){
                Route::post('store','UsersController@storeInfo');
                Route::post('setInfo', 'UsersController@setInfo');

                Route::group(['prefix'=>'resume'], function(){
                    Route::post('{type}', 'UsersController@storeResumeItem');
                    Route::put('{type}/{id}', 'UsersController@storeResumeItem');
                    Route::delete('{type}/{id}', 'UsersController@deleteResumeItem');
                });
            });

            Route::group(['prefix'=>'skill'], function(){
                Route::post('recommend','SkillsController@recommend');    
            });
        });    
    });
});
Route::group(['middleware'=>['confirmedAccount','subscribed','RecruiterOrCandidate']], function(){
    Route::group(['middleware'=>['hasAllInfos']], function(){
        Route::group(['namespace'=>'Users'], function(){
            Route::group(['prefix'=>'u'], function(){
                Route::get('{user}','UsersController@show');
            });
        });
        Route::group(['namespace'=>'Painel'], function(){
            Route::get('v/{id}','VacanciesController@show')->where('id', '[0-9]+')->name('mostrar-vaga');
        });
    });
});
// Route::get('subscription',function(){
// 	Auth::user()->newSubscription('main','monthly')->create(env('STRIPE_KEY'), [
// 		'email'=>'neimeg@gmail.com',
// 	]);
// });


// Route::get('test-message-email', function(\Request $request){
//     \Mail::to(\App\User::find(1)->email)->queue( new \App\Mail\NewMessage(\App\User::find(42),$request));
// });

// Route::get('fix-repeated-tags', function(){
//     $tags = \App\Models\Tag::orderBy('id')->get();
//     $forgetted = [];
//     DB::raw('SET FOREIGN_KEY_CHECKS=0;');
//     DB::enableQueryLog();
//     foreach($tags as $tag){
//         echo $tag->id;
//         echo "-----------------------------";
//         $other = $tags->where('name', $tag->name)->where('id','!=',$tag->id);
//         foreach($other as $k => $o){
//             if( !in_array($tag->id, $forgetted) ){
//                 $taggables = DB::table('taggables')->where('tag_id',$o->id)->get();
//                 $desirebleskills = DB::table('desirebleskills')->where('tag_id',$o->id)->get();
//                 $skills = DB::table('skills')->where('tag_id',$o->id)->get();
                
//                 foreach($taggables as $t){
//                     DB::table('taggables')->where('id',$t->id)->update(['tag_id' => $tag->id]);
//                 }

//                 foreach($desirebleskills as $d){
//                     DB::table('desirebleskills')->where('id',$d->id)->update(['tag_id' => $tag->id]);
//                 }

//                 foreach($skills as $d){
//                     DB::table('skills')->where('id',$d->id)->update(['tag_id' => $tag->id]);
//                 }
                
//                 $forgetted[] = $o->id;
//                 DB::table('tags')->where('id',$o->id)->delete();
//             }
//         }
//         echo '<hr />';
//     }
//     DB::raw('SET FOREIGN_KEY_CHECKS=1;');
// });



// Route::get('fix-experiencelevels', function(){
//     $vacancies = \App\Models\Vacancy::all();

//     foreach($vacancies as $vacancy){
//         $vacancy->experiences()->attach($vacancy->experience_id);
//     }
// });

// Route::get('fix-user-images', function(){
//     $users = \App\User::all();
//     $colors = ['#aa00ff','#ff6600','#005ce6','#00b33c','#009999','#59b300','#4d4dff','#4d88ff','#66cc00','#006699','#6666ff','#B452CD','#483D8B','#3D59AB','#33A1C9','#00CED1','#03A89E','#00CD66','#388E8E','#9ACD32','#9BCD9B'];
//     foreach($users as $u)
//     {
//         $color = $colors[rand(0,count($colors)-1)];
//         $initials = $u->initials;
//         if( !$u->image && !\File::exists(storage_path('app/public/uploads/users/placeholders/'.$initials.'.jpg')) )
//         {

//             $image = \Image::canvas(295,295,$color);
//             for($i = 295; $i >= 148; $i-=2){
//                 $image->text(strtoupper($initials), $i, $i, function($font) use($color){
//                     $font->file('fonts/google/source_sans_pro/SourceSansPro-Regular.ttf');
//                     $font->size(200);
//                     $font->color(adjustBrightness($color, -30));
//                     $font->align('center');
//                     $font->valign('middle');
//                 });
//             }

//             for($i = 158; $i >= 148; $i-=2){
//                 $image->text(strtoupper($initials), $i, $i, function($font) use($color){
//                     $font->file('fonts/google/source_sans_pro/SourceSansPro-Regular.ttf');
//                     $font->size(200);
//                     $font->color(adjustBrightness($color, -50));
//                     $font->align('center');
//                     $font->valign('middle');
//                 });
//             }
//             $image->text(strtoupper($initials), 147.5, 147.5, function($font) {
//                 $font->file('fonts/google/source_sans_pro/SourceSansPro-Regular.ttf');
//                 $font->size(200);
//                 $font->color('#ffffff');
//                 $font->align('center');
//                 $font->valign('middle');
//             });

//             @\File::makeDirectory(storage_path('app/public/uploads/users/placeholders'));
//             $image->save(storage_path('app/public/uploads/users/placeholders/'.strtoupper($initials).'.jpg'));

//             $thumb = $image->resize(null, 60, function ($constraint) {
//                         $constraint->aspectRatio();
//                     });
//             $thumb->save(storage_path('app/public/uploads/users/placeholders/'.strtoupper($initials).'-thumb.jpg'));
//         }
//     }
// });

// Route::get('create-all-leters-images', function(){
//     $colors = ['#aa00ff','#ff6600','#005ce6','#00b33c','#009999','#59b300','#4d4dff','#4d88ff','#66cc00','#006699','#6666ff','#B452CD','#483D8B','#3D59AB','#33A1C9','#00CED1','#03A89E','#00CD66','#388E8E','#9ACD32','#9BCD9B'];
//     $alphas = range('A', 'Z');
//     foreach($alphas as $a)
//     {
//         foreach($alphas as $b)
//         {
//             $color = $colors[rand(0,count($colors)-1)];
//             $initials = strtoupper($a.$b);
//             if( !\File::exists(storage_path('app/public/uploads/users/placeholders/'.$initials.'.jpg')) )
//             {

//                 $image = \Image::canvas(295,295,$color);
//                 for($i = 295; $i >= 148; $i-=2){
//                     $image->text(strtoupper($initials), $i, $i, function($font) use($color){
//                         $font->file('fonts/google/source_sans_pro/SourceSansPro-Regular.ttf');
//                         $font->size(200);
//                         $font->color(adjustBrightness($color, -30));
//                         $font->align('center');
//                         $font->valign('middle');
//                     });
//                 }

//                 for($i = 158; $i >= 148; $i-=2){
//                     $image->text(strtoupper($initials), $i, $i, function($font) use($color){
//                         $font->file('fonts/google/source_sans_pro/SourceSansPro-Regular.ttf');
//                         $font->size(200);
//                         $font->color(adjustBrightness($color, -50));
//                         $font->align('center');
//                         $font->valign('middle');
//                     });
//                 }
//                 $image->text(strtoupper($initials), 147.5, 147.5, function($font) {
//                     $font->file('fonts/google/source_sans_pro/SourceSansPro-Regular.ttf');
//                     $font->size(200);
//                     $font->color('#ffffff');
//                     $font->align('center');
//                     $font->valign('middle');
//                 });

//                 @\File::makeDirectory(storage_path('app/public/uploads/users/placeholders'));
//                 $image->save(storage_path('app/public/uploads/users/placeholders/'.strtoupper($initials).'.jpg'));

//                 $thumb = $image->resize(null, 60, function ($constraint) {
//                             $constraint->aspectRatio();
//                         });
//                 $thumb->save(storage_path('app/public/uploads/users/placeholders/'.strtoupper($initials).'-thumb.jpg'));
//             }
//         }
//     }
// });