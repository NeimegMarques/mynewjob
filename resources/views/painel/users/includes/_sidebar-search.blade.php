<div class="">
    {{Form::open(['url'=>route('profissionais'),'method'=>'GET','id'=>'sidebar-user-search-form'])}}
        <div class="form-group clearfix">
            <button class="btn btn-success btn-block">
                Buscar
            </button>
        </div>
        <div class="form-group">
            <label for="" class="control-label">Nome</label>
            <input type="text" name="n" value="{{@Input::get('n')}}" class="form-control input-sm">
        </div>
        <div class="form-group">
            <label for="with-image" class="control-label">
                <input @if(@Input::get('i') == 1) checked @endif class="styled-checkbox" id="with-image" type="checkbox" name="i" value="1" class="">
                Com foto
            </label>
        </div>
        {{-- <div class="form-group">
            <label for="" class="control-label">Palavras chave</label>
            <input type="text" name="k" value="{{@Input::get('k')}}" class="form-control input-sm">
        </div> --}}
        <div class="form-group clearfix select-city-after-container">
            <label for="" class="control-label">Cidade</label>
            <input type="text" name="cn" value="{{@Input::get('cn')}}" class="city-typeahead form-control input-sm" placeholder="Cidade">
        </div>
        <!-- <div class="form-group">
            <hr>
            <label for="" class="control-label">Distância - <span id="show-distance"></span></label>
            <div id="slider-user-distance"></div>
        </div> -->
        <div class="form-group clearfix">
            <hr>
            <button class="btn btn-success btn-block">
                Buscar
            </button>
        </div>
        {{Form::hidden('city_id',@Input::get('city_id'))}}
    {{Form::close()}}
</div>