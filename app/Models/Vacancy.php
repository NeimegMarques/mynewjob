<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vacancy extends Model
{
	public function getIsMineAttribute()
    {
        //SE A EMPRESA É DO USUÁRIO LOGADO ENTÂO A VAGA TAMBÉM PERTENCE A ELE
        return $this->company->isMine;
    }

    public function candidacies()
    {
        return $this->hasMany('App\Models\Candidacy')->orderBy('created_at','DESC');
    }

    public function jobtype()
    {
        return $this->belongsTo('App\Models\Jobtype');
    }

    public function experiences()
    {
        return $this->belongsToMany('App\Models\Experiencelevel');
    }

    public function tags()
    {
        return $this->morphToMany('App\Models\Tag', 'taggable');
    }

    public function desiredskills()
    {
        return $this->morphToMany('App\Models\Tag', 'desirebleskills');
    }

    public function benefits()
    {
        return $this->belongsToMany('App\Models\Benefit');
    }
    
    public function sectors()
    {
        return $this->morphToMany('App\Models\Sector', 'sectorable');
    }

    public function company()
    {
        return $this->belongsTo('App\Models\Company');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function city()
    {
        return $this->belongsTo('App\Models\City');
    }

    public function scopeActive($query, $flag = 1)
    {
        return $query->where('active', $flag);
    }

    public function getSalaryDescriptionAttribute()
    {
        if($this->salary_from > 0){
            if($this->salary_from == $this->salary_to){
                return float2Real($this->salary_from);
            } else{
                return 'Entre '.float2Real($this->salary_from). ' e ' . float2Real($this->salary_to);
            }
        } else{
            return 'A combinar';
        }
    }
}
