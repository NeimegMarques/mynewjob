<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Education extends Model
{
    public function candidate()
    {
        return $this->belongsTo('App\Models\Candidate', 'user_id');
    }

    public function city()
    {
        return $this->belongsTo('App\Models\City');
    }
}
