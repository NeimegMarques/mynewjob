<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewFollower extends Mailable
{
    use Queueable, SerializesModels;

    public $follower;
    public $user;

    public function __construct($user, $follower)
    {
        $this->user = $user;
        $this->follower = $follower;
    }

    public function build()
    {
        return $this->subject('Você tem um novo seguidor')
                    ->view('emails.newFollower');
    }
}
