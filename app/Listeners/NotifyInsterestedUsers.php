<?php

namespace App\Listeners;

use App\Events\VacancyWasCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifyInsterestedUsers
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  VacancyWasCreated  $event
     * @return void
     */
    public function handle(VacancyWasCreated $event)
    {
        // ENVIAR EMAIL PARA AS PESSOAS INTERESSADAS EM VADAS DO TIPO DA QUE FOI CRIADA
    }
}
