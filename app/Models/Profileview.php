<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Profileview extends Model
{
    public function viewer()
    {
    	return $this->belongsTo('App\User','user_id');
    }

    public function user()
    {
    	return $this->belongsTo('App\User','profile_id');
    }
}
