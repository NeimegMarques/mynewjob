require('dotenv')
	.config({
		"path": "../.env"
	});

var fs =    require('fs');

var options = process.env.APP_ENV != 'local' ? {
    key:    fs.readFileSync('/etc/letsencrypt/live/mynewjob.com.br/privkey.pem'),
    cert:   fs.readFileSync('/etc/letsencrypt/live/mynewjob.com.br/cert.pem'),
    ca:     fs.readFileSync('/etc/letsencrypt/live/mynewjob.com.br/chain.pem')
}:{};

var https = require(process.env.APP_ENV != 'local' ? 'https' : 'http');

var app = https.createServer(process.env.APP_ENV != 'local' ? options : server)
	, io = require('socket.io')(app)
	, redis = require('redis')
	, unserialize = require('php-serialization').unserialize;

var validator = require('./bower_components/validator-js/validator.min.js');

var Sequelize = require('sequelize');
//console.log(process.env);
var sequelize = new Sequelize( process.env.DB_DATABASE, process.env.DB_USERNAME, process.env.DB_PASSWORD, null );

//MODELS
var Chat = sequelize.define('chats', {
  sender_id: {
    type: Sequelize.INTEGER,
  },
  receiver_id: {
    type: Sequelize.INTEGER,
  },
  message: {
    type: Sequelize.TEXT
  },
  read: {
    type: Sequelize.BOOLEAN
  },
  notified: {
    type: Sequelize.BOOLEAN
  }

}, {
  freezeTableName: true, // Model tableName will be the same as the model name,
  sender      : 'sender_id',
  receiver    : 'receiver_id',
  created_at  : 'created_at',
  updated_at  : 'updated_at',
  timestamps  : true,
  underscored : true
});


var User = sequelize.define('user', {
   id: {
    	type: Sequelize.INTEGER,
    	autoIncrement: true,
    	primaryKey: true
	},
	image: {
	    type: new Sequelize.STRING,
	},
	thumb: {
	    type: new Sequelize.VIRTUAL(Sequelize.STRING, ['image']),
	    get: function (val) {
	       	return process.env.ASSET_URL+'/uploads/users/'+this.get('id')+'/img/thumb_'+this.get('image');
	    }
	},
	name: {
    	type: Sequelize.STRING,
	},
	surname: {
    	type: Sequelize.STRING,
	},
	slug: {
    	type: Sequelize.STRING,
	},
	email: {
    	type: Sequelize.STRING,
	},
  	createdAt: {
    	type: Sequelize.DATE,
    	field: 'created_at'
  	},
  	updatedAt: {
    	type: Sequelize.DATE,
    	field: 'updated_at'
  	}
});

var Notification = sequelize.define('notification', {
   	id: {
    	type: Sequelize.INTEGER,
    	autoIncrement: true,
    	primaryKey: true
	},
	userId: {
    	type: Sequelize.INTEGER,
    	foreignKey: true,
    	field: 'receiver_id'
	},
	senderId: {
    	type: Sequelize.INTEGER,
    	foreignKey: true,
    	field: 'sender_id'
	},
	message: {
    	type: Sequelize.STRING,
	},
	type: {
    	type: Sequelize.STRING,
	},
	link: {
    	type: Sequelize.STRING,
	},
	seen: {
    	type: Sequelize.BOOLEAN,
	},
	notified: {
    	type: Sequelize.BOOLEAN,
	},
  	createdAt: {
    	type: Sequelize.DATE,
    	field: 'created_at'
  	},
  	updatedAt: {
    	type: Sequelize.DATE,
    	field: 'updated_at'
  	}
});

var Queuedjobs = sequelize.define('queuedjobs', {
   	id: {
    	type: Sequelize.INTEGER.UNSIGNED,
    	autoIncrement: true,
    	primaryKey: true
	},
	queue: {
    	type: Sequelize.STRING
	},
	payload: {
    	type: Sequelize.TEXT,
	},
	attempts: {
    	type: Sequelize.INTEGER.UNSIGNED,
	},
	created_at: {
    	type: Sequelize.INTEGER.UNSIGNED,
    	field: 'created_at'
  	},
  	available_at: {
    	type: Sequelize.INTEGER.UNSIGNED,
    	field: 'available_at'
  	},
  	reserved_at: {
    	type: Sequelize.INTEGER.UNSIGNED,
    	field: 'reserved_at'
  	}
},{
    underscored: true,
    timestamps:false
});

// Queuedjobs.create({
//     queue  		: "default",
//     payload		: "Teste",
//     attempts	: 0,
//     reserved_at	: 123,
//     available_at: 1234,
//     created_at	: 12345,
// });

User.hasMany(Notification,{});
Notification.belongsTo(User,{foreignKey:'receiver_id'});
Notification.belongsTo(User, {as:'Sender',foreignKey:'sender_id'});
Chat.belongsTo(User,{as:'Sender',foreignKey:'sender_id'});
Chat.belongsTo(User,{foreignKey:'receiver_id'});

app.listen(3001, function(){
	console.log('Escutando na porta 3001');
});

function server(req, res){
	res.end('Hi there :)');
}

function getSocketId(options){
    for ( sktid in onlineUsers ) {
        if(onlineUsers[sktid] == options.id_dest){
            return sktid;
        }
    }
}

var client = redis.createClient();

io.on('connection', function(socket){
	socket.on('set notification notified', function(data){
		client.get('laravel:' + data.key, function(err, reply){
			var session = {};

			session = unserialize(reply);
			session = unserialize(session);

			Notification.update({
				notified:1
			},{
				where:{
					receiver_id : session.user_id
				}
			});
		});
	})
	.on('set notification seen', function(data){
		client.get('laravel:' + data.key, function(err, reply){
			var session = {};

			session = unserialize(reply);
			session = unserialize(session);

			Notification.update({
				notified:1,
				seen:1
			},{
				where:{
					receiver_id : session.user_id,
					id:data.nid
				}
			});
		});
	})
	.on('get notifications', function(data){
		//console.log('Pegando notificações de', data);
		client.get('laravel:' + data.key, function(err, reply){
			var session = {};

			if(err) {

			}

			session = unserialize(reply);
			session = unserialize(session);

			Notification.findAll({
				attributes: 
		            [
		            'id',
		            [sequelize.literal('(SELECT image FROM users WHERE users.id = notification.sender_id)'), 'thumb'],
		            'message',
		            'sender_id',
		            'notified',
		            'link',
		            'type',
		            'seen'
		            ]
				,
				where:{
					receiver_id : session.user_id,
				},
				limit: 12,
				include:[
					{
						model:User
					},
					{
						model:User,
						as:'Sender'
					}
				]
			})
			.then(function (notifications) {
				Notification.findAll({
					attributes: [
						[sequelize.fn('COUNT', sequelize.col('id')),'new']
					],
					where:{
						receiver_id : session.user_id,
						notified : 0,
					},
					limit: 51
				})
				.then(function (new_notifications) {
					// PEGAR O SOCKET CONECTADO USANDO O ID DO USUÁRIO
					var roomId = 'room_' + session.user_id;
					var clients = io.sockets.adapter.rooms[roomId].sockets;

					for(clientSocket in clients ){
						var socketClient = io.sockets.connected[clientSocket];
						socketClient.emit('notifications',
							{
								notifications:notifications,
								new:new_notifications[0]
						});
					}
				});
			});
		});
	});

	socket.on('auth', function(data){
		//console.log(data);
		client.get('laravel:' + data.key, function(err, reply){
			var session = {};

			if(err) {

			}

			session = unserialize(reply);
			session = unserialize(session);

			//console.log(session.user_id);
		});
	});

	var redisClient = redis.createClient();

	// redisClient.monitor(function (err, res) {
	//     console.log("Entering monitoring mode.");
	// });

	// redisClient.on("monitor", function (time, args, raw_reply) {
	//     console.log(time + ": " + args);
	// });

  	redisClient.subscribe('message','follow');

  	redisClient.on("message", function(channel, data) {
  		//console.log(data);
  		var data = JSON.parse(data);
  		socket.broadcast.to(data.room).emit('message', data);
  	});

  	redisClient.on("follow", function(channel, data) {
  		console.log('----------------',data,'----------------');
  		var data = JSON.parse(data);
  		socket.broadcast.to(data.room).emit('new_follower', data);
  	});

  	redisClient.on('error', function(e) {
	   console.log('subscriber', e.stack); 
	});

  	socket.on('joinroom', function(uid) {
  		//onlineUsers[socket.id] = uid;
  		//console.log('Entrando na sala', room);
    	socket.join('room_'+uid);
    	socket.emit('roomJoined', {msg:'Entrou'});
    	//socket.broadcast.to('room_5').emit('message', {teste:'Teste'});
	});

	socket
	.on('user typing', function(data){
        if(data){
        	console.log('Usuário '+data.uid+' Digitando...');
        	var roomId = 'room_'+data.friend_id;
        	var room = io.sockets.adapter.rooms[roomId];
			var clients = undefined != room ? room.sockets : [];
			for(clientSocket in clients ){
				var socketClient = io.sockets.connected[clientSocket];
				socketClient.emit('user typing',data);
			}
        }
    })
    .on('user stop typing', function(data){

        if(data){
        	var roomId = 'room_'+data.friend_id;
			var room = io.sockets.adapter.rooms[roomId];
			var clients = undefined != room ? room.sockets : [];
			for(clientSocket in clients ){
				var socketClient = io.sockets.connected[clientSocket];
				socketClient.emit('user stop typing',data);
			}
        }
    })
    .on('message', function(data) {
    	var sender_id = validator.escape(data.myid);
    	var receiver_id = validator.escape(data.uid);
    	var message = validator.escape(data.message);
    	return sequelize.transaction(
    		function (t) {
	            return Chat.create({
	                sender_id  	: sender_id,
	                receiver_id	: receiver_id,
	                message		: message
	            });
	        }
        ).then(function (result) {
        	if(data.notify_by_email){
        		User.findById(receiver_id).then(function(user) {
        			var d = new Date(Date.now());
	        		var t = d.getTime().toString().substr(0,10);
	        		
	        		Queuedjobs.create({
		                queue  		: "default",
		                payload		: '{"job":"Illuminate\\\\Queue\\\\CallQueuedHandler@call","data":{"commandName":"Illuminate\\\\Mail\\\\SendQueuedMailable","command":"O:34:\\"Illuminate\\\\Mail\\\\SendQueuedMailable\\":1:{s:11:\\"\\u0000*\\u0000mailable\\";O:19:\\"App\\\\Mail\\\\NewMessage\\":17:{s:4:\\"user\\";O:45:\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\":2:{s:5:\\"class\\";s:8:\\"App\\\\User\\";s:2:\\"id\\";i:'+sender_id+';}s:7:\\"request\\";O:34:\\"Illuminate\\\\Support\\\\Facades\\\\Request\\":0:{}s:7:\\"\\u0000*\\u0000from\\";a:0:{}s:5:\\"\\u0000*\\u0000to\\";a:1:{i:0;a:2:{s:7:\\"address\\";s:'+user.email.length+':\\"'+user.email+'\\";s:4:\\"name\\";N;}}s:5:\\"\\u0000*\\u0000cc\\";a:0:{}s:6:\\"\\u0000*\\u0000bcc\\";a:0:{}s:10:\\"\\u0000*\\u0000replyTo\\";a:0:{}s:10:\\"\\u0000*\\u0000subject\\";N;s:7:\\"\\u0000*\\u0000view\\";N;s:11:\\"\\u0000*\\u0000textView\\";N;s:11:\\"\\u0000*\\u0000viewData\\";a:0:{}s:14:\\"\\u0000*\\u0000attachments\\";a:0:{}s:17:\\"\\u0000*\\u0000rawAttachments\\";a:0:{}s:12:\\"\\u0000*\\u0000callbacks\\";a:0:{}s:10:\\"connection\\";N;s:5:\\"queue\\";N;s:5:\\"delay\\";N;}}"}}',
		                attempts	: 0,
		                reserved_at	: null,
		                available_at: t,
		                created_at	: t,
		            });
	        		console.log('-----------------','Notificar por email','------------------');
				});
        	}
        	console.log(result.dataValues);
        	var roomId = 'room_'+data.uid;
			var clients = io.sockets.adapter.rooms[roomId].sockets;
			for(clientSocket in clients ){
				var socketClient = io.sockets.connected[clientSocket];
				socketClient.emit('incoming_msg',
					{
						result: result.dataValues,
						wdw: data.wdw
					}
				);
			}



            // Transaction has been committed
            //socket.broadcast.to('room_'+data.uid).emit('incoming_msg', JSON.stringify({msg:msg,sender:data.myid}));
            // result is whatever the result of the promise chain returned to the transaction callback
        }).catch(function (err) {
            // Transaction has been rolled back
            // err is whatever rejected the promise chain returned to the transaction callback
        });
	})
	.on('loadMessages', function(data){
		console.log('Loading messages');
		//var data = JSON.parse(data);

		console.log('Partner is '+data.partner);
        var partner = parseInt(data.partner);

        console.log('Person ID is '+data.my_id);
        var my_id = data.my_id;

        console.log('Set messages read between '+data.my_id+' and '+data.partner);
        Chat.update(
        {
        	read: '1'
        },{
        	where: [
                '( receiver_id = ? AND sender_id = ? ) OR ( receiver_id = ? AND sender_id = ? )',
                partner,
                my_id,
                my_id,
                partner
            ]
        })
        .then(function (res) { 
        	console.log(res);
        }, function (e) {
        	console.log(e);
        });

        console.log('Get messages');
        Chat.findAll({
            where: [
                '( receiver_id = ? AND sender_id = ? ) OR ( receiver_id = ? AND sender_id = ? )',
                partner,
                my_id,
                my_id,
                partner
            ],
            order: [
                ['id','DESC']
            ],
            offset: 0,
            limit: 10
        })
        .then(function(result) {
            //console.log(result.dataValues);
            // if(io.sockets.connected[sktid]){
            //     io.sockets.connected[sktid].emit('lastMessagesLoaded',JSON.stringify({result:result,partner:partner}));
            // }

            var roomId = 'room_'+my_id;
            console.log('Room is '+roomId);

            console.log('Getting clients sockets');
			var clients = io.sockets.adapter.rooms[roomId].sockets;

			console.log('Foreach client socket...');
			for(clientSocket in clients ){
				var socketClient = io.sockets.connected[clientSocket];
				console.log('Emitting messages');
				socketClient.emit('messages',
					{
						uid: partner,
						messages: result
					}
				);
			}

        });




		// client.get('laravel:' + data.key, function(err, reply){
		// 	var session = {};

		// 	session = unserialize(reply);
		// 	session = unserialize(session);

		// 	//session.user_id

		// 	var data = JSON.parse(data);
	 //        var partner = parseInt(data.partner);
	 //        var my_id = data.my_id;

	 //        var options = {
	 //            onlineUsers:onlineUsers,
	 //            id_dest:my_id
	 //        }

	 //        var sktid = getSocketId(options);

	 //        Chat.findAll({
	 //            where: [
	 //                '( receiver_id = ? AND sender_id = ? ) OR ( receiver_id = ? AND sender_id = ? )',
	 //                data.partner,
	 //                my_id,
	 //                my_id,
	 //                data.partner
	 //            ],
	 //            order: [
	 //                ['id','DESC']
	 //            ],
	 //            offset: 0,
	 //            limit: 10
	 //        })
	 //        .then(function(result) {
	 //            //console.log(result.dataValues);
	 //            if(io.sockets.connected[sktid]){
	 //                io.sockets.connected[sktid].emit('lastMessagesLoaded',JSON.stringify({result:result,partner:partner}));
	 //            }
	 //        });
		// });
    });

	socket.on('disconnect', function() {
    	redisClient.quit();
  	});
});