module.exports = {

	init:function(){
		var th = this;
		$(function(){
			if($( "#slider-job-distance" ).length){
				var slider = $( "#slider-job-distance" ).slider({
			      	min: 1,
			      	max: 30,
			      	formatter: function(val){
			      		return val+'km';
			      	}
			    }).on('slide', function( event, ui ) {
			    	$('#show-distance').text(th.distanceText(event));
			    });

			    $('#show-distance').text(th.distanceText(null,slider.slider('getValue')));
			}
		});

		//$('.city-typeahead').typeahead({

		//});

		// $('.city-typeaheads').typeahead({
		//   hint: true,
		//   highlight: true,
		//   minLength: 1
		// },
		// {
		//   name: 'states',
		//   source: substringMatcher(states)
		// });


		// $(".select-state-before,.select-city-after").selectpicker({
		// 	liveSearch:true
		// }); 

	},
	distanceText : function(event, val){
		return  (event != null && event.value > 20) || val > 20 ?'Qualquer distância':(event?event.value:val)+'km'; 
	}
}