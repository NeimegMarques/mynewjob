@extends('layouts.app',['page'=>'portfolio-view',
	'props'=>[
		'portfolioid'=>$portfolio->id,
        ':main_image_item'=>@$portfolio->items[0],
        ':active' => $portfolio->active ? 'true':'false',
        'editing' => @$editing,
        ':portfolioitems' => $portfolio->items
	]
])

@section('head_tags')
    <?php 
    $title = 'Portfolio: '.$portfolio->title.' de '.@$portfolio->user->name.' '.@$portfolio->user->surname;
    $description = strip_tags($portfolio->description);
    $keywords = 'Portfolio,';
    if(count($portfolio->tags))
    {
        foreach($portfolio->tags as $tag)
        {
            $keywords .= $tag->name.', ';
        }
    }
    $url = \Request::fullUrl();
    $type = "website";
    $image = @$portfolio->items[0]->thumb;
    ?>
    @include("includes.head_tags")               
@stop

@section('content')
<section class="bg-primary top-background-bar"></section>
<div class="portolio" v-cloak>
	<div class="container">
        <div class="frame">
            <div class="row">
                <div class="container-fluid">
                    <div class="author-info clearfix">
                        <div class="img-wrapper">
                            <img src="{{@$portfolio->user->thumb}}" alt="{{@$portfolio->user->slug}}" class="img-responsive">
                        </div>
                        <div class="name">
                            <a href="{{url('/u/'.$portfolio->user->slug)}}">
                                {{@$portfolio->user->name.' '.@$portfolio->user->surname}}
                            </a>
                        </div>
                    </div>
                    @if($portfolio->user->portfolios->count() > 1)
                    <div class="btn-group pull-right">
                        <a href="{{url('/portfolio/'.@$portfolio->prev.(@$editing?'/edit':''))}}" class="btn btn-primary">
                            <i class="fa fa-chevron-left"></i>
                        </a>
                        <a href="{{url('/portfolio/'.@$portfolio->next.(@$editing?'/edit':''))}}" class="btn btn-primary">
                            <i class="fa fa-chevron-right"></i>
                        </a>
                    </div>
                    @endif
                    @if(@$portfolio->user_id == @Auth::user()->id)
                    <a style="margin-right: 10px;" href="{{url('/portfolio/'.@$portfolio->slug.'/editar')}}" class="btn btn-success pull-right">
                        <i class="fa fa-pencil"></i> Editar
                    </a>
                    @endif
                </div>
            </div>
            <hr>
            @if(@$editing)
            {{Form::open(['url'=>url('/user/resume/portfolio/'.$portfolio->id)])}}
            <input name="_method" type="hidden" value="PUT">
            @endif
            <div class="row">
                <div class="col-sm-3">
                    <ul class="thumbs">
                        <li v-for="item in portfolioitems" v-bind:class="{'selected': item.id == main_image_item.id}">
                            <button v-on:click="removePortfolioItem($index)" type="button" class="btn-remove-portfolioitem" v-if="editing">
                                <i class="fa fa-remove"></i>
                            </button>
                            <input type="hidden" name="items[]" v-if="item.id" value="@{{item.id?item.id:''}}">
                            <input type="hidden" name="images[]" v-if="!item.id" value="@{{item.imagePath}}">
                            <a href="javascript:void(0)" v-on:click.prevent="setMainImage(item)">
                                <img src="@{{item.thumb}}" alt="" class="img-responsive">
                            </a>
                        </li>
                        <li v-if="editing">
                            <label for="input-new-portfolioitem">
                                <input v-on:change="onAddPortfolioImage($event)" type="file" name="" id="input-new-portfolioitem">
                                <span>
                                    <i class="fa fa-plus"></i>
                                </span>
                            </label>
                        </li>
                    </ul>
                </div>
                <div class="col-sm-14">
                    <div class="main-img-wrapper">
                        <img v-show="!loading_image" src="@{{main_image_item.imagePath}}" alt="" class="main-image img-responsive">
                        <img v-if="loading_image" src="{{asset('img/loading.gif')}}" alt="" class="loader img-responsive">
                    </div>
                </div>
                <div class="col-sm-7">
                    <h4 v-if="!editing" class="portfolio-title title">
                        {{$portfolio->title}}
                    </h4>
                    <div v-if="editing" >
                        <label for="" class="control-label">Tìtulo</label>
                        <input name="portfolio_title" style="font-size: 20px;" type="text" class="form-control" value="{{$portfolio->title}}">
                    </div>
                    
                    <div class="tags-container clearfix">
                        @if(@$editing)
                            <br>
                            <div>
                                <label for="" class="control-label">
                                    Tecnologias usadas no projeto
                                </label>
                                {!!Form::text('portfolio_tags', '',['class'=>'form-control  portfolio-tags-input','placeholder'=>'HTML, CSS, Photoshop...'])!!}
                            </div>
                        @else
                            @foreach($portfolio->tags as $tag)
                                <div class="tag tag-sm label label-info">{{$tag->name}}</div>
                            @endforeach
                        @endif
                    </div>
                    <br>

                    <div v-if="!editing" class="description">
                        <p>{{$portfolio->description}}</p>
                    </div>
                    <div v-if="editing" >
                        <label for="" class="control-label">Descrição</label>
                        <textarea v-if="editing" name="portfolio_description" rows="10" class="form-control">{{$portfolio->description}}</textarea>
                        <hr>
                        <div class="">
                            <button class="btn btn-success">Salvar</button>
                            <a href="{{url('/portfolio/'.$portfolio->slug)}}" class="btn btn-default">Cancelar</a>
                            ou
                            <a v-on:click.prevent="removePortfolio({{$portfolio->id}})" class="btn btn-link"><span class="small">Remover</span></a>
                        </div>
                    </div>
                </div>
            </div>
            @if(@$editing)
                {{Form::close()}}
            @endif
        </div>
    </div>
</div>
@endsection

@push('script-bottom')
<script>
    $(function () {
        @if($portfolio)
            @foreach($portfolio->tags as $tag) 
            $('.portfolio-tags-input').tagsinput('add',
                {'str_id':'{{$tag->id}}','name':'{!!$tag->name!!}'}
            );
            @endforeach
        @endif
    });
</script>
@endpush
