<div class="resume-item">
    @include('painel.users.includes.resume-item-options')
    <div class="resume-item-content">
        <h5 class="title">
            {{$job->title}} 
        </h5>
        <p class="start-end">
            {{!$job->end?'Desde ':''}}
            {{date('M Y',strtotime($job->start))}} {{($job->end != NULL) ? ' - '.date('M Y',strtotime($job->end)) : ''}}
        </p>
        <p class="company-name">
            {{@$job->company_name}}
            @if(@$job->city)
            <span class="small text-muted">
                <i class="fa fa-map-marker text-info"></i> {{ @$job->city?@$job->city->name.' - '.@$job->city->state->code:'' }}
            </span>
            @endif
        </p>
        <div class="description">
            <p>{{$job->description}}</p>
        </div>
    </div>
    @if(@\Auth::user()->is_me)
        <div class="resume-item-form-content hidden">
            @include('painel.users.includes.job.form-job')
        </div>
    @endif
</div>