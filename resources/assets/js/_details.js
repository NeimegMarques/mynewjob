module.exports = {

	init:function(){
		var th = this;

		$(function(){


			//Página de Estado e Cidade
			$('#select-city-after').selectpicker({
			  style: 'btn-default',
			  liveSearch:true,
			})

			$('#select-state-before').selectpicker({
			  style: 'btn-default',
			  liveSearch:true,
			}).on('changed.bs.select', function(){
				$('#info-location-btn-container').addClass('hidden').find('button').attr('disabled',true);

				$('.info-loader').removeClass('hidden');

				$.get({
					url:site_url+'/get/cities-by-state',
					data:{
						state_id:$(this).val()
					},
					dataType:'json',
					success:function(res){
						$('#select-city-after-container').removeClass('hidden');
						$('#select-city-after')
							.html('')
							.append('<option selected disabled>Selecione a cidade</option>');
						$.each(res, function(i, city){
							$('#select-city-after').append($('<option value="'+city.id+'" />').text(city.name))
						});

						$('#select-city-after').selectpicker("refresh");
					},
					error: function(e){
						console.log(e);
					},
					complete: function(){
						$('.info-loader').addClass('hidden');
					}
				});
			});

			$('#select-city-after').on('changed.bs.select', function(){
				$('#info-location-btn-container').removeClass('hidden').find('button').removeAttr('disabled');
			});

			$(document).on('click keypress','.btn-submit-info-city', function(e){
				$('.info-select-city-error').remove();
				var city = $('#select-city-after').val();
				if(!city){
					e.preventDefault();
					$(this).before('<div class="info-select-city-error alert alert-danger">Por favor, selecione o estado e a cidade</div>')
				}
			});

		});
	},
	
}