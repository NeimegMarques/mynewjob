<?php

namespace App\Http\Controllers\Companies;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\State;

use App\Models\Company;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;

use Carbon\Carbon;

class CompaniesController extends Controller
{
    public function show($slug){
	    $company = \App\Models\Company::where('slug',$slug)->first();
        
        view()->share('company', $company);

	    return view('painel.companies.show')
	    	->withCompany($company);
    }

    public function getByQuery($query){
        $users = Company::where('name','LIKE','%'.$query.'%')
                        ->limit(6)
                        ->get();
        if(\Request::json()){
            return $users->toJson();
        }   
    }

    // public function edit($slug){
    //     $user = \App\User::where('slug',$slug)->first();
    //     return view('painel.users.edit')
    //         ->withUser($user);
    // }

    // public function setZipCode(Request $request){
    //     \Auth::user()->zip_code = $request->zip_code;
    //     \Auth::user()->longitude = $request->lng;
    //     \Auth::user()->latitude = $request->lat;
    //     \Auth::user()->save();
    //     return redirect('painel');
    // }

    // public function setCity(Request $request){
    //     \Auth::user()->city_id = $request->city;
    //     \Auth::user()->save();
    //     return redirect('painel');
    // }

    // public function storeInfo(Request $request){
    //     switch($request->t){
    //         case "basic": return $this->setBasicInfo($request);
    //         break;
    //         case "pass": return $this->setPassInfo($request);
    //         break;
    //         case "social": return $this->setSocialInfo($request);
    //         break;
    //     }
    // }

    // public function setBasicInfo(Request $request){
    //     $user = \Auth::user();
    //     $zoom = $request->zoom;
    //     $offset_x = ceil($request->x*-1);
    //     $offset_y = ceil($request->y*-1);
    //     $rotation = $request->rotation;

    //     $birth = explode('/',$request->birth);

    //     $birth = Carbon::create($birth[2], $birth[1], $birth[0],0)->toDateString();
        
    //     if($request->hasFile('user_image'))
    //     {
    //         $image = $request->file('user_image');
            
    //         if($image->isValid())
    //         {
    //             $filename = date('dmY_His').'.'.$image->getClientOriginalExtension();
    //             $image = \Image::make($image);
                
    //             $image_w = $image->width();
    //             $image_h = $image->height();

    //             $n_w = $zoom*$image_w;
    //             $n_h = $zoom*$image_h;

    //             $image->rotate($rotation|0);
    //             $image->resize($n_w,$n_h);
    //             $image->crop(300,300, $offset_x, $offset_y);

    //             @\File::makeDirectory(storage_path('app/public/uploads'));
    //             @\File::makeDirectory(storage_path('app/public/uploads/users'));
    //             @\File::makeDirectory(storage_path('app/public/uploads/users/'.\Auth::user()->id));
    //             @\File::makeDirectory(storage_path('app/public/uploads/users/'.\Auth::user()->id).'/img');
    //             @\File::makeDirectory(storage_path('app/public/uploads/users/'.\Auth::user()->id.'/img/profile'));

    //             $image->save(storage_path('app/public/uploads/users/'.\Auth::user()->id.'/img/profile/'.$filename));
    //             $thumb = $image->resize(50,50);
    //             $image->save(storage_path('app/public/uploads/users/'.\Auth::user()->id.'/img/profile/thumb_'.$filename));

    //             $user->image = $filename;
    //         }
    //     }

    //     $user->name = $request->name;
    //     $user->surname = $request->surname;
    //     $user->birth = $birth;
    //     if($user->save()){
    //         return back()->withMessage('Edição concluída.');
    //     }
    // }

    // public function setPassInfo(Request $request){

    //     $old_pass               = $request->current_pass;
    //     $password               = $request->password;
    //     $password_confirmation  = $request->password_confirmation;

    //     $user = \Auth::user();

    //     if(\Hash::check($old_pass, \Auth::user()->password))
    //     {
    //         if(strlen($password) >= 6)
    //         {
    //             if($password == $password_confirmation){
    //                 $user->password = \Hash::make($password);

    //                 if($user->save())
    //                 {
    //                     return redirect(URL::previous() . "#pass")->withMessage('Senha alterada com sucesso');
    //                 }
    //                 else
    //                 {
    //                     return redirect(URL::previous() . "#pass")->withError('Houve um erro ao tentar salvar a nova senha');
    //                 }
    //             }
    //             else
    //             {    
    //                 return redirect(URL::previous() . "#pass")->withError('A nova senha não é igual à confirmação da nova senha');
    //             }
    //         }
    //         else
    //         {
    //             return redirect(URL::previous() . "#pass")->withError('A senha precisa ter no mínimo 6 caracteres');
    //         }
    //     }
    //     else
    //     {
    //         return redirect(URL::previous() . "#pass")->withError('A senha atual informada não conincide. Tente novamente');
    //     }
    // }

    // public function setSocialInfo(Request $request){

    //     $social = \Auth::user()->social?:new \App\Models\SocialLink;;
        
    //     $links = $request->link;

    //     $a = [];
    //     foreach ($links as $k => $lk) {
    //         $social->$k = $lk;
    //     }

    //     if(\Auth::user()->social()->save($social)){
    //         return redirect(URL::previous() . "#social")->withMessage('Os links foram atualizados com sucesso');
    //     }

    //     return redirect(URL::previous() . "#social")->withError('Houve um erro ao tentar atualizar os links');
    // }


    // public function setInfo(Request $request){
        
    //     $user = \Auth::user();

    //     if($request->name == 'name')
    //     {
    //         $user->name = $request->value;
    //         if($user->save()){
    //             return response()->json(['message'=>'O nome foi alterado com sucesso']);
    //         }
    //     }
    //     if($request->name == 'surname')
    //     {
    //         $user->surname = $request->value;
    //         if($user->save()){
    //             return response()->json(['message'=>'O sobrenome foi alterado com sucesso']);
    //         }
    //     }
    //     elseif($request->name == 'about')
    //     {
    //         $user->about = $request->value;
    //         if($user->save()){
    //             return response()->json(['message'=>'A descrição foi alterada com sucesso']);
    //         }   
    //     }
    //     elseif($request->has('city_id'))
    //     {
    //         $user->city_id = $request->city_id;
    //         if($user->save()){
    //             return response()->json(['message'=>'A cidade foi alterada com sucesso']);
    //         }   
    //     }
    //     elseif($request->name == 'video')
    //     {
    //         $url = $request->value;
    //         if($url){

    //             if(strpos($url,'www.youtube.com/embed/'))
    //             {
    //                 $video = $url;
    //             }
    //             elseif(strpos($url,'//youtu.be/'))
    //             {
    //                 $parts = explode('.be/',$request->value);
    //                 $video_id = explode('&',$parts[1])[0];

    //                 $video = 'https://www.youtube.com/embed/'.$video_id;
    //             }
    //             elseif(strpos($url,'v='))
    //             {
    //                 $parts1 = explode('v=',$request->value);
    //                 $video_id = explode('&',$parts1[1])[0];
    //                 $video = 'https://www.youtube.com/embed/'.$video_id;
    //             }

    //             $user->video = $video;

    //         }
    //         else
    //         {
    //             $video = '';
    //             $user->video = '';
    //         }

    //         if($user->save()){
    //             $view = $video ? view('painel.users.includes.user-video')->withCandidate(\Auth::user())->withVideo($video)->render() : '';
    //             return response()->json(['view'=>$view,'message'=>'A URL do vídeo foi alterada com sucesso']);
    //         }   
    //     }
    //     elseif($request->name == 'phones')
    //     {
    //         $phones = explode(PHP_EOL, $request->value);

    //         \DB::beginTransaction();
    //         $user->phones()->delete();

    //         foreach ($phones as $ph) {
    //             $phone = new \App\Models\Phone;
    //             $phone->phone = $ph;
    //             if(!$user->phones()->save($phone))
    //             {
    //                 \DB::rollBack();
    //                 return response()->json(['error'=>'Houve um erro ao tentar inserir o telefone: '.$ph]);
    //             }
    //         }

    //         \DB::commit();
    //         return response()->json(['message'=>'Os telefones foram incluídos com sucesso']);

    //     }
    //     elseif($request->name == 'emails')
    //     {
    //         $emails = explode(PHP_EOL, $request->value);

    //         \DB::beginTransaction();
    //         $user->emails()->delete();

    //         foreach ($emails as $mail) {
    //             $email = new \App\Models\Email;
    //             $email->email = $mail;
    //             if(!$user->emails()->save($email))
    //             {
    //                 \DB::rollBack();
    //                 return response()->json(['error'=>'Houve um erro ao tentar inserir o email: '.$mail]);
    //             }
    //         }

    //         \DB::commit();
    //         return response()->json(['message'=>'Os emails foram incluídos com sucesso']);

    //     }
    //     elseif($request->has('skills')){
    //         $skills = $request->skills;

    //         \DB::beginTransaction();

    //         foreach ($skills as $key => $sk) {
    //             if(is_numeric($sk['str_id']))
    //             {
    //                 $tags_ids[] = $sk['str_id'];
    //             }
    //             elseif($sk['name'] != '')
    //             {
    //                 $tag = new \App\Models\Tag;
    //                 $tag->name = $sk['name'];
    //                 $tag->tag = str_slug($sk['name']);
    //                 $tag->save();
    //                 $tags_ids[] = $tag['id'];
    //             }
    //         }

    //         if( !$user->tags()->sync($tags_ids))
    //         {
    //             \DB::rollBack();
    //             return response()->json(['error'=>'Houver um erro ao tentar salvar as tags. Tente novamente.']);
    //         };

    //         \DB::commit();
    //         return response()->json(['message'=>'As tags foram salvas com sucesso','tags'=>$user->tags]);
    //     }
    //     else
    //     {
    //         return response()->json(['message'=>'Opss...  Parece que o programador estava dormindo pouco e deixou passar algum erro :(']);
    //     }
    // }

    // public function storeResumeItem(Request $request,$type,$id=NULL){

    //     $user = \Auth::user();

    //     if($type == "job")
    //     {
            
    //         $start = $request->get('job_year_from').'-'.$request->get('job_month_from').'-01';
            
    //         $end = $request->get('until_now') == 'on' ? NULL : $request->get('job_year_to').'-'.$request->get('job_month_to').'-01';

    //         $job = $id ? $user->jobs()->find($id): new \App\Models\Job;
            

    //         $job->title = $request->get('job_name');
    //         $job->description = $request->get('job_description');
    //         $job->start = $start;
    //         $job->end = $end;
    //         $job->company_name = $request->get('company_name');
    //         $job->city_id = $request->get('city_id');
            
    //         if(!$id) $job->user_id = \Auth::user()->id;
            
    //         if($job->save())
    //         {
    //             $view = view('painel.users.includes.job.resume-job')->withJob($job)->render();
    //             return response()->json(['view'=>$view,'job'=>$job,'editing'=>$id?true:false]);
    //         }
    //         else
    //         {

    //         }
    //     }
    //     elseif($type == 'education')
    //     {
    //         $start = $request->get('education_year_from').'-'.$request->get('education_month_from').'-01';
            
    //         $end = $request->get('until_now') == 'on' ? NULL : $request->get('education_year_to').'-'.$request->get('education_month_to').'-01';

    //         $education = $id ? $user->educations()->find($id): new \App\Models\Education;
            
    //         $education->title = $request->get('education_name');
    //         $education->description = $request->get('education_description');
    //         $education->start = $start;
    //         $education->end = $end;
    //         $education->entity_name = $request->get('entity_name');
    //         $education->city_id = $request->get('city_id');
            
    //         if(!$id) $education->user_id = \Auth::user()->id;
            
    //         if($education->save())
    //         {
    //             $view = view('painel.users.includes.education.resume-education')->withEducation($education)->render();
    //             return response()->json(['view'=>$view,'education'=>$education,'editing'=>$id?true:false]);
    //         }
    //         else
    //         {

    //         }
    //     }
    //     elseif($type == 'course')
    //     {
    //         $start = $request->get('course_year_from').'-'.$request->get('course_month_from').'-01';
            
    //         $end = $request->get('until_now') == 'on' ? NULL : $request->get('course_year_to').'-'.$request->get('course_month_to').'-01';

    //         $course = $id ? $user->courses()->find($id): new \App\Models\Course;
            
    //         $course->title = $request->get('course_name');
    //         $course->description = $request->get('course_description');
    //         $course->start = $start;
    //         $course->end = $end;
    //         $course->entity_name = $request->get('entity_name');
    //         $course->city_id = $request->get('city_id');
            
    //         if(!$id) $course->user_id = \Auth::user()->id;
            
    //         if($course->save())
    //         {
    //             $view = view('painel.users.includes.course.resume-course')->withCourse($course)->render();
    //             return response()->json(['view'=>$view,'course'=>$course,'editing'=>$id?true:false]);
    //         }
    //         else
    //         {

    //         }
    //     }
    //     elseif($type == 'certificate')
    //     {

    //         $certificate = $id ? $user->certificates()->find($id): new \App\Models\Certificate;
            
    //         $certificate->title = $request->get('certificate_name');
    //         $certificate->description = $request->get('certificate_description');
    //         $certificate->entity_name = $request->get('entity_name');
            
    //         if(!$id) $certificate->user_id = \Auth::user()->id;
            
    //         if($certificate->save())
    //         {
    //             $view = view('painel.users.includes.certificate.resume-certificate')->withCertificate($certificate)->render();
    //             return response()->json(['view'=>$view,'certificate'=>$certificate,'editing'=>$id?true:false]);
    //         }
    //         else
    //         {

    //         }
    //     }
    //     elseif($type == 'language')
    //     {

    //         $language = $id ? $user->languages()->find($id): new \App\Models\Language;
            
    //         $language->title = $request->get('language_name');
    //         $language->level = $request->get('language_level');
            
    //         if(!$id) $language->user_id = \Auth::user()->id;
            
    //         if($language->save())
    //         {
    //             $view = view('painel.users.includes.language.resume-language')->withLanguage($language)->render();
    //             return response()->json(['view'=>$view,'language'=>$language,'editing'=>$id?true:false]);
    //         }
    //         else
    //         {

    //         }
    //     }
    // }

    // public function deleteResumeItem(Request $request, $type ,$id)
    // {
    //     $user = \Auth::user();

    //     if(in_array($type,['certificate','course','education','job','language']))
    //     {
    //         $relation_name = $type.'s';
    //         $$type = $user->{$relation_name}()->find($id);

    //         if($$type && $$type->delete())
    //         {
    //             return response()->json(['message'=>'O item foi excluído com sucesso']);
    //         }
    //         else
    //         {
    //             return response()->json(['error'=>'Houve um erro ao tentar excluir o item'],'400');
    //         }
    //     }
    // }
}
