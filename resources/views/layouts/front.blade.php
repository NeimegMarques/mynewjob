<!DOCTYPE html>
<html lang="en">
<head>
    <head>
    @section('head_tags')
        <?php 
        $title = "MyNewJob - Novas vagas de emprego todos os dias. Garanta a sua!";
        $description = "MyNewJob é um site de vagas de emprego onde você pode facilmente criar seu currículo e aparecer para as empresas que estão procurando profisionais como você.";
        $keywords = "Empregos, Currículo, Vagas, Empresas, Recrutamento, RH, Recolocação";
        $url = \Request::fullUrl();
        $type = "website";
        $image = asset("/img/logo-icon.png");
        ?>
        <title>{{$title}}</title>
        <meta name="description" content="{{$description}}"/>
        <meta name="keywords" content="{{$keywords}}"/>
        <meta name="url" content="{{$url}}"/>
        <meta property="fb:app_id" content="1555887301393105"/>
        <meta property="og:url" content="{{$url}}"/>
        <meta property="og:type" content="{{$type}}"/>
        <meta property="og:title" content="{{$title}}"/>
        <meta property="og:description" content="{{$description}}"/>
        <meta property="og:image" content="{{$image}}"/>
        <meta property="og:site_name" content="{{$title}}"/>
        <meta property="og:locale" content="PT_br"/> 

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'My New Job') }}</title>


        <!-- Fonts -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>

        <!-- Styles -->
        <link href="{{ elixir('css/styles.css') }}" rel="stylesheet">

        <script>
            window.maps_api_key = "{{env('MAPS_API_KEY')}}";
            window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
            ]); ?>
        </script>
        @include("includes.favicon") 
        <meta name="google-site-verification" content="rYfamR5Ps3e89WCpJJo9M30vWVtDYe7DJReYKoL57Os" />
    @show
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-59248202-7', 'auto');
      ga('send', 'pageview');

    </script>
    @stack('script-head')

</head>
<body>
    <div id="wrapper"
        data-url="{{url('/')}}" 
        url="{{url('/')}}" 
        data-uid="{{@Auth::user()->id}}" 
        data-sid="{{ \Session::getId() }}"
    >
        <div class="topbar">
            <nav class="navbar navbar-default navbar-static-top">
                <div class="container">
                    <div class="navbar-header">
            
                        <!-- Collapsed Hamburger -->
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                            <span class="sr-only">Toggle Navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
            
                        <!-- Branding Image -->
                        <a class="navbar-brand" href="{{ url('/') }}">
                            my<span class="text-primary">new</span>job
                        </a>
                    </div>
            
                    <div class="collapse navbar-collapse" id="app-navbar-collapse">
                        <!-- Left Side Of Navbar -->
                        <ul class="nav navbar-nav">
                            {{-- <li><a href="{{ url('/sobre') }}">Sobre</a></li>
                            <li><a href="{{ url('/blog') }}">Blog</a></li>--}}
                            <li><a target="_blank" href="http://facebook.com/neimeg">Contato</a></li>
                        </ul>
            
                        <!-- Right Side Of Navbar -->
                        <ul class="nav navbar-nav navbar-right">
                            <!-- Authentication Links -->
                            @if (Auth::guest())
                                <li class="login-register-links"><a href="{{ url('/login') }}">Entrar</a></li>
                                <li class="login-register-links"><a href="{{ url('/register') }}">Registrar</a></li>
                            @else
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                        {{ Auth::user()->name }} <span class="caret"></span>
                                    </a>
            
                                    <ul class="dropdown-menu" role="menu">
                                        <li>
                                            <a href="{{ url('/v/new') }}">Publicar uma vaga</a>
                                        </li>
                                        <li>
                                            <a href="{{ url('/painel') }}">Procurar vagas</a>
                                        </li>
                                        <li>
                                            <div class="container-fluid">
                                                {{Form::open(['url'=>url('logout')])}}
                                                    <button class="btn-link btn-block text-left"><i class="fa fa-btn fa-sign-out"></i> Sair</button>
                                                {{Form::close()}}
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a href="{{url('painel')}}">
                                        Painel
                                    </a>
                                </li>
                            @endif
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
        <main>
            <component is="{{@$page?:'front'}}" inline-template
                @if(count(@$props))
                    @foreach($props as $k => $prop)
                        {{$k}}="{{$prop}}"
                    @endforeach
                @endif
                :url="url"
            >
            @yield('content')
            </component>
        </main>

        <footer>
            <div class="container">
                <div class="row xs-align-center">
                    <div class="col-sm-12">
                        &copy;2016 MyNewJob. Todos os direitos reservados
                    </div>
                    <div class="col-sm-12">
                        <ul class="nav nav-pills pull-right">
                            {{-- <li><a href="{{url('sobre')}}">sobre nós</a></li>--}}
                            <li><a target="_blank" href="http://facebook.com/neimeg">contato</a></li>
                            <li><a href="{{url('login')}}">entrar</a></li>
                            <li><a href="{{url('register')}}">registrar</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>

        <!-- JavaScripts -->
        <script src="https://cdn.socket.io/socket.io-1.3.4.js"></script>
        <script src="{{ elixir('js/front.js') }}"></script>
        @if ( Config::get('app.debug') )
          <script type="text/javascript">
            document.write('<script src="//localhost:35729/livereload.js?snipver=1" type="text/javascript"><\/script>')
          </script> 
        @endif
        @stack('script-bottom')
    </div>
</body>
</html>
