<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="robots" content="follow, index">

 <!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<title id="fake-title">{{ config('app.name', 'My New Job') }}</title>
<title>@{{ totalNotifications > 0 ? '('+totalNotifications+') ' : ''}}{{ config('app.name', 'My New Job') }}</title>

<!-- Styles -->
<link href="{{ elixir('css/styles.css') }}" rel="stylesheet">
<script>
    window.Laravel = <?php echo json_encode([
        'csrfToken' => csrf_token(),
        'url' => url('/'),
    ]); ?>
</script>
<script src="https://use.fontawesome.com/2c0ec885c8.js"></script>
@include("includes.favicon") 