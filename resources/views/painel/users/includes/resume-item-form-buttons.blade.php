<div class="form-group">
	<button type="button" v-on:click="saveResumeItem($event)" tabindex="1" class="btn btn-sm btn-success"><i class="fa fa-save"></i> Salvar</button>
	<button type="reset" v-on:click="cancelEditResumeItem($event)" class="btn btn-sm  btn-default">Cancelar</button>
	@if(@$item)
		&nbsp;&nbsp;&nbsp;
		<span class="small">ou</span> 
		<button type="button" class="btn btn-link btn-remove-resume-item"><span class="small"><i class="fa fa-trash"></i>  remover</span></button>
	@endif
</div>