<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class HasAllInfosMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if(@Auth::check()){
            if( @Auth::user()->candidate){
                if (@Auth::user()->zip_code == '') {
                    return redirect('info/zip_code');
                }
                // elseif (@Auth::user()->city_id == '') {
                //     return redirect('info/zip_code');
                // }
            }

            if(@Auth::user()->recruiter){

            }
        }

        return $next($request);
    }
}
