<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AuthTest extends TestCase
{
    use DatabaseTransactions;
    
    public function teste_user_can_register_but_must_confirm_email()
    {
        // NÃO FUNCIONA MAS MANTIVE SÓ PRA ME BASEAR
        $this->visit('register')
             ->type('Teste 1', 'name')
             ->type('teste1@teste.com', 'email')
             ->type('password', 'password')
             ->press('Prosseguir');
        $this->see('Por favor, confirme seu email.')
            ->seeInDatabase('users',['name' => 'Teste 1','verified'=>0]);
    }
}
