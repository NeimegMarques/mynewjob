module.exports = {

	init:function(){
	    var th = this;

	    $(function () {
	    	// FUI OBRIGADO A MANTER ISSO AQUI PORQUE SE EU TENTAR COLOCAR NO COMPONENTE DO VUE
	    	// APARECE SÓ O ID DA TAG NO INPUT NA HORA DE EDITAR
	    	$('.skill-tags-input').tagsinput({
	            itemText: 'name',
	            itemValue: 'str_id',
	            typeaheadjs: {
	                name: 'tags',
	                displayKey: 'name',
	                //valueKey: 'nameWithCode',
	                source: tags,
	                templates: {
	                    empty: [
	                      '<div class="empty-message"></div>'
	                    ].join('\n'),
	                    suggestion: function(res){
	                        return $('<div>'+res.name+'</div>');
	                    }
	                }
	            }
	        });
	    	
	    	$(document)
	        .on('click','.until-now-checkbox', function(e){
	        	if(clickOrPress(e)){
	        		var checked = $(this).is(':checked');
	        		var tto = $(this).closest('.time-to');

	        		if(checked){
	        			tto.find('.time-to-container').addClass('hidden');
	        			tto.find('.until-now-text').removeClass('hidden');
	        		}else{
	        			tto.find('.time-to-container').removeClass('hidden');
	        			tto.find('.until-now-text').addClass('hidden');
	        		}
	        	}
	        })
	        .on('click','.btn-add-resume-item',function(e){
	        	if(clickOrPress(e)){
	        		var wrapper = $(this).closest('.resume-item');

	        		$(this).addClass('hidden');
	        		wrapper.find('.resume-item-form-content').removeClass('hidden');
	        	}
	        })
	        .on('click','.btn-remove-resume-item', function(e){
	        	if(clickOrPress(e)){
		        	var btn = $(this);
	        		var form = $(e.target).closest('form');

	        		swal({
					  	title: "Tem certeza?",
					  	text: "Se você apagar não será possível recuperar essas informações!",
					  	type: "warning",
					  	showCancelButton: true,
					  	confirmButtonClass: "btn-danger",
					  	confirmButtonText: "Sim, remover!",
					  	cancelButtonText: "Não, não quero remover!",
					  	closeOnConfirm: false,
					  	closeOnCancel: true
					},
					function(isConfirm) {
					  if (isConfirm) {
					    // A função está em main.js
		        		var data = form.serializeObject();
		        		data['_method'] = 'DELETE';

		        		$.post({
		        			url:form.attr('action'),
		        			data:data,
		        			dataType:'json',
		        			success:function(res){
		        				btn.closest('.resume-item').remove();
		        				swal("Removido!", res.message, "success");
		        			},
		        			error: function(e){
		        				swal("Ooops!", e.responseJSON.error, "error");
		        			}
		        		});
					  }
					});
	        	};
	        });
	    });
  	},
  	updateTags:function(tags){
  		var stags = $('.skills-tags-container');
  		stags.html('');

  		$.each(tags, function(i,tag){
  			stags.append('<span class="tag label label-info">'+tag.name+'</span>');
  		});
  	}
}