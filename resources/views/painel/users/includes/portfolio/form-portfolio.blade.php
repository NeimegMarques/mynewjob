
{{Form::open(['url'=>url('/user/resume/portfolio/'.@$portfolio->id)])}}
@if(@$portfolio)
	{{Form::hidden('_method','PUT')}}
@endif
<div class="form-group">
	<label for="" class="control-label">
		Título
	</label>
	{{Form::text('portfolio_title',@$portfolio->title,['class'=>'form-control input-sm','tabindex'=>'1'])}}
</div>
<div class="form-group">
	<label for="" class="control-label">
		Tecnologias usadas no projeto
	</label>
	{!!Form::text('portfolio_tags', '',['class'=>'form-control  portfolio-tags-input','placeholder'=>'HTML, CSS, Photoshop...'])!!}
</div>
<div class="form-group">
	<label for="" class="control-label">
		Descrição
	</label>
	{{Form::textarea('portfolio_description',@$portfolio->description,['rows'=>'4','class'=>'form-control input-sm','tabindex'=>'1'])}}
</div>
<div class="form-group">
	<label for="" class="control-label">
		Imagens 
	</label>
	<br>
	<br>
	<div class="row">
		<div v-for="image in portfolioImages" class="col-sm-8">
			<input type="hidden" name="images[@{{$index}}]" value="@{{image}}">
		    <div class="resume-item portfolio">
		    	<div class="resume-item-edit">
				    <button v-on:click="removePortfolioImage($event, $index)" style="background-color: #f66; border-color: #f66;" class="btn btn-edit-resume-item btn-default" type="button">
				        <i class="fa fa-remove"></i>
				    </button>
				</div>
		        <div class="resume-item-content">
		            <div class="img-wrapper" style="background: url(@{{image}}); background-size: cover; background-position: center;">
		            </div>
		        </div>
		    </div>
		</div>
		<div class="col-sm-8">
		    <div class="resume-item portfolio edit">
		        <div class="resume-item-content">
		            <div class="img-wrapper" style="background: url({{@$portfolio->items[0]->thumb}}); background-size: cover; background-position: center;">
		                {{-- <img src="" alt="" class="img-responsive"> --}}
		            </div>
		            <div class="portfolio-img-input-wrapper">
		                <label for="portfolio-img-input">
		                	<input type="file" v-on:change="onPortfolioFileChange" id="portfolio-img-input">
		                </label>
		                <i class="fa fa-plus"></i>
		            </div>
		        </div>
		    </div>
		</div>
	</div>
</div>
<hr>
@include('painel.users.includes.resume-item-form-buttons',['item'=>@$portfolio])
{{Form::close()}}