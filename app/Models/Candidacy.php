<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Candidacy extends Model
{
    public function vacancy()
    {
    	return $this->belongsTo('App\Models\Vacancy','vacancy_id');
    }

    public function candidate()
    {
    	return $this->belongsTo('App\User','user_id');
    }
}
