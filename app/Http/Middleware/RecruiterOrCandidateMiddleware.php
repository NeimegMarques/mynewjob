<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RecruiterOrCandidateMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if(@Auth::check() && @!Auth::user()->candidate && @!Auth::user()->recruiter){
            return redirect('info/tipo');
        }
        return $next($request);
    }
}
