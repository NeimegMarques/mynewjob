require('./bootstrap');

require('jquery-mask-plugin');
require('jquery.animate-number');
require('bootstrap-datepicker');

require('../../../bower_components/x-editable/dist/bootstrap3-editable/js/bootstrap-editable.min.js');


// >>>> AO INVÉS DE CARREGAR ESSE...
//require('../../../bower_components/x-editable/dist/inputs-ext/typeaheadjs/typeaheadjs.js');
// >>>> ESTOU CARREGANDO ESSE, que é uma versão com poucas modificações, PORQUE EU FIZ SER COMPATìVEL COM Typeahead 0.11.x
require('./_typeaheadjs-adapter.js');


require('../../../node_modules/jquery-slimscroll/jquery.slimscroll.min.js');


$('form').on("keypress", ":input:not(textarea):not([type=submit])", function(event) {
    if(event.keyCode === 13){
      event.preventDefault();
      return false;
    } 
});
window.clickOrPress = function(e){
  if(e.keyCode === 13 || e.type === 'click'){
    return true;
  }
  return false;
}

import Front from './components/system/front/Front.vue';
import AccountType from './components/system/registering/AccountType.vue';
import Address from './components/system/registering/Address.vue';

// Vue.http.headers.common['Access-Control-Allow-Origin'] = '*';
// Vue.http.headers.common['Access-Control-Request-Method'] = '*';
Vue.http.options.credentials = true;

new Vue({
  el:'#wrapper',
  props:{
    dataUrl:'',
    dataUid:'',
    dataSid:'',
    url:''
  },
  ready(){
    var th = this;
      th.adjustMainHeight();

    // ATIVA A TAB REFERENTE AO HASH DA URL
    var hash = window.location.hash;
    hash && $('ul.nav a[href="' + hash + '"]').tab('show');

    $(window).scroll(function(e){
      var top = $(window).scrollTop();
      th.adjustMainHeight();

      if($(window).width() > 768){
          if(top>0){
              $('header,body').addClass('page-scrolled');
          }else{
              $('header,body').removeClass('page-scrolled');
          }
      }else{
          $('header,body').removeClass('page-scrolled');
      }

      // $('.banner').css({
      //     'background-position':'center '+(top*0.6)+'px'
      // });

      if($('.counters').length){
          var ot = $('.counters').offset().top;
          if(top+$(window).height() > ot+50){
            $('.counter-number:not(.is-counted)').each(function(i,item){
                var number = $(item).data('number');
                $(item).animateNumber({
                  number: number,
                    easing: 'easeInQuad', // require jquery.easing 
                
                  // optional custom step function 
                    // using here to keep '%' sign after number 
                    numberStep: function(now, tween) {
                    var floored_number = Math.floor(now),
                        target = $(tween.elem).find('span');
                    target.text(floored_number);
                  }
                },
                1300,
                function() {
                  item.addClass('is-counted');
                });
              });
          }
      }
    });


    // REGISTER PAGE
    var timer = 0;
    $(function(){
      $(document)
      .on('click keypress','.btn-next', function(e){
        e.preventDefault();
        var form = $(this).closest('form');
        var id = $(this).closest('.address-result').attr('id');

        $('.address-result').not('#'+id).remove().promise().done(function(){
          form.submit();
        });

      });
    })
  },
  methods:{
    adjustMainHeight: function(){
      $('main').css('min-height',window.innerHeight-$('.topbar').innerHeight()-$('footer').innerHeight());
    }
  },
  components: {
    Front,
    Address,
    AccountType
  }
});
