<li class="plan plan1 list-group-item">
    <div class="row">
        <div class="col-sm-7 col-xs-12">
            <label for="plan1" class="plan-check-wrapper">
                <input @if(Input::get('p')=='gratis') checked @endif id="plan1" type="radio" name="select_plan" value="gratis">
                Plano Grátis
            </label>
        </div>
        <div class="col-sm-9 col-xs-12">
            <h1 class="plan-price">R$ 0,00<span>/mês</span></h1>
        </div>
        <div class="col-sm-8 hidden-xs">
            Para quem deseja ver as melhores vagas e oportunidades disponíveis
        </div>
    </div>
</li>
<li class="plan plan2 list-group-item">
    <div class="row">
        <div class="col-sm-7 col-xs-12">
            <label for="plan2" class="plan-check-wrapper">
                <input @if(Input::get('p')=='pro' || !Input::has('p')) checked @endif  id="plan2" type="radio" name="select_plan" value="pro">
                Plano Pró
            </label>
        </div>
        <div class="col-sm-9 col-xs-12">
            <h1 class="plan-price">R$ {{number_format(env('PRO_PRICE')/100,2,',','.')}}<span>/mês</span></h1>
        </div>
        <div class="col-sm-8 hidden-xs">
            O mais recomendado. Você tem acesso às melhores vagas, mapa, chat, e muito mais.
        </div>
    </div>
</li>
<li class="plan plan3 list-group-item">
    <div class="row">
        <div class="col-sm-7 col-xs-12">
            <label for="plan3" class="plan-check-wrapper">
                <input @if(Input::get('p')=='vip') checked @endif  id="plan3" type="radio" name="select_plan" value="vip">
                Plano Vip
            </label>
        </div>
        <div class="col-sm-9 col-xs-12">
            <h1 class="plan-price">R$ {{number_format(env('VIP_PRICE')/100,2,',','.')}}<span>/mês</span></h1>
        </div>
        <div class="col-sm-8 hidden-xs">
            Pra você que sonha alto e quer chegar lá! Acesso ilimitado às vagas e recursos do site.
        </div>
    </div>
</li>