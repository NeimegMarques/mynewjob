<div class="vacancy frame" vacancyId="{{@$vacancy->id}}">
    <div class="vacancy-head">
        <div class="vacancy-options">
            <button class="btn btn-default dropdown-toggle" type="button" id="vacancy-{{@$vacancy->id}}-option-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                <i class="fa fa-chevron-down"></i>
            </button>
            <ul class="dropdown-menu pull-right" aria-labelledby="vacancy-{{@$vacancy->id}}-option-dropdown">
                @if($vacancy->is_mine)
                <li><a href="{{route('editar-vaga',$vacancy->id)}}">Editar</a></li>
                {{-- <li><a href="">Excluir</a></li> --}}
                <li>
                    <a v-if="!toggling" href="#" v-on:click.prevent="toggleActive()">
                        <span v-if="active">Encerrar inscrições</span>
                        <span v-else>Abrir inscrições</span>
                    </a>
                    <a href="" v-if="toggling">Processando...</a>
                </li>
                @else
                <li><a href="#">Sugerir correção</a></li>
                    @if( !@$vacancy->redirect )
                    <li v-if="applied"><a v-on:click="giveUpTheJob()" href="#">Desistir da vaga</a></li>
                    @endif
                @endif
            </ul> 
        </div>

        <div class="col-sm-6">
            <div class="vacancy-company-img-wrapper">
                @if(@Auth::check())
                <a href="#">
                    @if($vacancy->company->logo)
                    <img class="img-responsive" src="{{$vacancy->company->logo_path}}" alt="...">
                    @else
                    <img src="{{asset('/img/logo-icon.png')}}" alt="" class="img-responsive">
                    @endif
                </a>
                @else
                <img src="{{asset('/img/logo-icon.png')}}" alt="" class="img-responsive">
                @endif
            </div>
        </div>
        <div class="col-sm-18">
            <h3 class="vacancy-title">
                {{$vacancy->name}}
            </h3>
            <h5 class="vacancy-company">
                @if(@Auth::check())
                <a href="{{$vacancy->company->url}}">{{$vacancy->company->name}}</a>
                @else
                    <b class="text-primary">Confidencial</b>
                @endif
            </h5>
            @if($vacancy->city_id)
                <p class="vacancy-city">
                    <i class="fa fa-map-marker text-info"></i>
                    {{@$vacancy->city->name.' - '.@$vacancy->city->state->code}}
                </p>
            @endif
            @if(@Auth::check())
                @if( $vacancy->company->is_mine )
                    <div class="vacancy-buttons">
                        <a class="btn" :class="{'btn-warning':active && !toggling,'btn-success':!active && !toggling,'btn-default':toggling}" href="#" v-on:click.prevent="toggleActive()">
                            <span v-if="active && !toggling"><i class="fa fa-lock"></i> Encerrar inscrições</span>
                            <span v-if="!active && !toggling"><i class="fa fa-unlock"></i> Abrir inscrições</span>
                            <span v-if="toggling"><i class="fa fa-refresh"></i> Processando...</span>
                        </a>
                    </div>
                @else
                    @if($vacancy->active)
                        <div v-if="!applied" v-cloak>
                            <div class="vacancy-buttons">
                                <button v-if="!redirect" type="button" class="btn btn-success btn-candidatar-me btn-lg" data-toggle="modal" data-target="#modalCandidatarMe" v-on:click="candidacy.resultError=''">Candidatar-me</button>
                                <div v-if="redirect">
                                    <a href="{{$vacancy->link}}" type="button" class="btn btn-success btn-candidatar-me btn-lg" v-on:click="candidacy.resultError=''" target="_blank">Candidatar-me</a>
                                    <div class="small text-muted">
                                        {{ parse_url($vacancy->link, PHP_URL_HOST) }}
                                    </div>
                                </div>
                                {{-- <button class="btn btn-default btn-lg">Salvar</button> --}}
                            </div>
                            
                            <div class="modal fade" id="modalCandidatarMe" tabindex="-1" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title">Candidatar-me</h4>
                                        </div>
                                        @if(@Auth::user()->isAbleTo('apply'))
                                            {{Form::open(['url'=>url('v/candidacy')])}}
                                                {{Form::hidden('vacancy_id',$vacancy->id)}}
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-sm-6">Vaga:</div>
                                                        <div class="col-sm-18">
                                                            <p><b>{{$vacancy->name}}</b></p>
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <div class="row">
                                                        <div class="col-sm-6">Pretensão salarial:@if($vacancy->required_salary_pretension) <span class="text-danger">*</span> @endif</div>
                                                        <div class="col-sm-18">
                                                            <p>
                                                                <input @if($vacancy->required_salary_pretension) required @endif type="text" name="candidacy_salary" placeholder="0,00" class="salary-input form-control" v-model="candidacy.salary">
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                        </div>
                                                        <div class="col-sm-18">
                                                            <p>
                                                                Você pode incluir uma mensagem na sua candidatura.
                                                            </p>
                                                            <p>{{Form::textarea('candidacy_message','',['class'=>'form-control','rows'=>4,'autofocus'=>'true','v-model="candidacy.message"'])}}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <div v-if="candidacy.resultError != ''" class="alert alert-danger alert-error-candidacy text-left">
                                                        @{{candidacy.resultError}}
                                                        <button type="button" class="close" v-on:click="candidacy.resultError=''" aria-label="Fechar"><span aria-hidden="true">&times;</span></button>
                                                    </div>
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                                    <button v-bind:disabled="applying" v-on:click.prevent="candidatarMe" type="button" class="btn btn-primary btn-proceed-candidacy">
                                                        <span v-if="!applying">Prosseguir</span>
                                                        <span v-if="applying">Enviando curículo...</span>
                                                    </button>
                                                </div>
                                            {{Form::close()}}
                                        @else
                                            <div class="modal-body">
                                                <br>
                                                <p>Antes de candidatar-se você precisa preencher algumas informações que estão faltando em seu currículo: 
                                                <ul style="padding-left:20px; list-style: none;">
                                                    <li>
                                                        @if(@Auth::user()->phones->count() || @Auth::user()->emails->count())
                                                            <i class="glyphicon glyphicon-ok text-success" style="color:#1ab71d;"></i> 
                                                        @else
                                                            <i class="glyphicon glyphicon-remove text-muted" style="color:#ccc"></i>
                                                        @endif
                                                        Email e/ou Telefone de contato
                                                    </li>
                                                    <li>
                                                        @if(@Auth::user()->educations->count() || @Auth::user()->jobs->count() || @Auth::user()->courses->count())
                                                            <i class="glyphicon glyphicon-ok text-success" style="color:#1ab71d;"></i> 
                                                        @else
                                                            <i class="glyphicon glyphicon-remove text-muted" style="color:#ccc"></i>
                                                        @endif
                                                        Educação, Trabalhos e/ou Cursos
                                                    </li>
                                                    <li>
                                                        @if(@Auth::user()->skills->count())
                                                            <i class="glyphicon glyphicon-ok text-success" style="color:#1ab71d;"></i> 
                                                        @else
                                                            <i class="glyphicon glyphicon-remove text-muted" style="color:#ccc"></i>
                                                        @endif
                                                        Pelomenos 1 competência.
                                                    </li>
                                                </ul> 
                                                <p>Informações detalhadas a seu respeito são tão importantes quanto uma entrevista.</p>
                                                <p>No futuro será possível importar suas informações do LinkedIn, estamos trabalhando nisso.</p>
                                                <hr>
                                                <div class="alert alert-warning alert-square">
                                                    <h5>Você pode preencher suas informações através dá sua 
                                                        <a href="{{url('/u/'.@Auth::user()->slug)}}"><b>página pessoal</b></a>
                                                    </h5> 
                                                </div>
                                            </div>
                                        @endif
                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->
                        </div>
                        <div v-cloak v-if="applied">
                            <div class="alert alert-success"> Obrigado por candidatar-se a esta vaga. Boa sorte!</div>
                        </div>
                        <div v-cloak v-if="giveup.error">
                            <div class="alert alert-danger">
                                @{{giveup.error}}
                            </div>
                        </div>
                        <div v-cloak v-if="giveup.message">
                            <div class="alert alert-success">
                                @{{giveup.message}}
                            </div>
                        </div>
                    @else
                        <div class="alert alert-warning square">
                            Esta vaga não está mais ativa ou o prazo para candidaturas acabou.
                        </div>
                    @endif
                @endif
            @else
                <div class="alert alert-info">Faça <a href="{{url('/login')}}">login</a> para candidatar-se a esta vaga!</div>
            @endif
        </div>
    </div>
    <hr>
    <div class="vacancy-body clearfix">
        
        <div class="col-sm-6">
            <div class="job-salary">
                <p class="title"><strong>Salário</strong></p>
                <p class="small salary-value"><strong>{{$vacancy->salary_description}}</strong></p>
            </div>

            @if($vacancy->benefits->count())
            <div class="job-requirement">
                <p class="title"><strong>Benefícios</strong></p>
                <p>
                    @forelse($vacancy->benefits as $benefit)
                        <div class="label label-primary">{{$benefit->name}}</div>
                    @empty
                    @endforelse
                </p>
            </div>
            @endif
            
            <div class="job-requirement">
                <p class="title"><strong>Quantidade de vagas</strong></p>
                <p>
                    <span class="text-muted small">
                        {{$vacancy->quantity > 1 ? $vacancy->quantity.' vagas' : '1 vaga'}}
                    </span>
                </p>
            </div>
            
            <div class="job-requirement">
                <p class="title"><strong>Regime de trabalho</strong></p>
                <p>
                    <span class="text-muted small">
                        {{$vacancy->jobtype->name}}
                    </span>
                </p>
            </div>
            <div class="job-requirement">
                <p class="title"><strong>Experiência requerida</strong></p>
                <p>
                    @forelse($vacancy->experiences as $exp)
                        <div class="label label-primary">{{$exp->name}}</div>
                    @empty
                    @endforelse
                </p>
            </div>
            <div class="job-requirement">
                <p class="title"><strong>Setor</strong></p>
                <p>
                    @forelse(@$vacancy->sectors as $sector)
                        <span class="text-muted small">{{$sector->name}}</span><br />
                    @empty
                    @endforelse
                </p>
            </div>
            <div class="job-requirement">
                <p class="title"><strong>Ramo de atuação da empresa</strong></p>
                <p>
                    @forelse(@$vacancy->company->sectors as $sector)
                        <span class="text-muted small">{{$sector->name}}</span><br />
                    @empty
                        <span class="text-muted">Não informado</span>
                    @endforelse
                </p>
            </div>
        </div>
        <div class="col-sm-18">
            <div>
                <h5 class="title">Competências exigidas</h5>
                @if($vacancy->tags->count())
                <div class="vacancy-tags">
                    @forelse($vacancy->tags as $tag)
                    <div class="label label-primary">{{$tag->name}}</div>
                    @empty
                    @endforelse
                </div>
                @endif
            </div>
            <div>
                @if($vacancy->desiredskills->count())
                    <h5 class="title">Competências desejadas</h5>
                    <div class="vacancy-tags">
                        @forelse($vacancy->desiredskills as $skill)
                        <div class="label label-primary">{{$skill->name}}</div>
                        @empty
                        @endforelse
                    </div>
                @endif
            </div>
            <div class="vacancy-description">
                <h5 class="title">Descrição da vaga</h5>
                <div>
                    {!! nl2br($vacancy->description)!!}
                </div>
            </div>
            @if($vacancy->company->is_mine || @Auth::user()->id == 1)
            <div class="vacancy-candidacies" v-if="!redirect">
                <hr>
                <h4 class="title">
                    <strong>Candidatos</strong>
                    @if($vacancy->candidacies->count() && $vacancy->user_id == @Auth::user()->id)
                        <button class="btn btn-primary btn-xs pull-right" v-on:click="exportcv()">
                            <span class="fa fa-file-pdf-o"></span> Exportar CV dos candidatos
                        </button>
                    @endif
                </h4>
                @forelse($vacancy->candidacies as $candidacy)
                    @include('painel.vacancy.includes._candidacy',['candidacy'=>$candidacy])
                @empty
                    <p class="text-muted">Nenhum usuário candidatou-se ainda</p>
                @endforelse
            </div>
            @endif
        </div>
    </div>
</div>