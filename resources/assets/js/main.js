require('./bootstrap');

require('chosen-jquery-browserify');
require('tagmanager');
require('jquery-mask-plugin');
require('bootstrap-select');
require('bootstrap-datepicker');
require('cropper');
require('bootstrap-tagsinput');
require('bootstrap-slider');
require('bootstrap-notify');
require('slimscroll');
require('../../../node_modules/bootstrap-sweetalert/dist/sweetalert.min.js');

require('../../../node_modules/bootstrap-datepicker/js/locales/bootstrap-datepicker.pt-BR.js');
//require('../../../bower_components/eonasdan-bootstrap-datetimepicker/src/js/bootstrap-datetimepicker.js');
require('../../../bower_components/x-editable/dist/bootstrap3-editable/js/bootstrap-editable.min.js');


// >>>> AO INVÉS DE CARREGAR ESSE...
//require('../../../bower_components/x-editable/dist/inputs-ext/typeaheadjs/typeaheadjs.js');
// >>>> ESTOU CARREGANDO ESSE, que é uma versão com poucas modificações, PORQUE EU FIZ SER COMPATìVEL COM Typeahead 0.11.x
require('./_typeaheadjs-adapter.js');

//require('flexslider');
require('../../../node_modules/jquery-slimscroll/jquery.slimscroll.min.js');
//require('../plugins/customSelect/jquery.customSelect.min.js');

//var boot = require('./_boot');
var topbar = require('./_topbar');
var subscription = require('./_subscription');
var jobsearch = require('./_job-search');
var user = require('./_user');
var user_edit = require('./_user_edit');
var details = require('./_details');
//var vacancy = require('./_vacancy');
var socket_events = require('./_socket-events');
topbar.init();
subscription.init();
jobsearch.init();
user.init();
user_edit.init();
details.init();
//vacancy.init();

// OS EVENTOS DO SOCKET SÓ SERÃO INICIALIZADOS
// SE O USUÁRIO ESTIVER LOGADO
if($('body').data('uid')>0){
  socket_events.init();
}

$(function(){
	$('.city-typeahead').typeahead({
	  hint: true,
	  highlight: true,
	  minLength: 3
	},
	{
    limit:10,
	  name: 'cities',
	  source: cities,
	  display:'nameWithCode',
	  templates: {
	    empty: [
	      '<div class="empty-message">',
	        'Nenhum resultado encontrado',
	      '</div>'
	    ].join('\n'),
	    suggestion: function(res){
	    	return $('<div><strong>'+res.name+'</strong> – '+res.state.code+'</div>');
	    }
	  }
	});

  $('.portfolio-tags-input').tagsinput({
      itemText: 'name',
      itemValue: 'str_id',
      typeaheadjs: {
          name: 'tags',
          displayKey: 'name',
          //valueKey: 'nameWithCode',
          source: tags,
          templates: {
              empty: [
                '<div class="empty-message">Nenhum resultado encontrado</div>'
              ].join('\n'),
              suggestion: function(res){
                  return $('<div>'+res.name+'</div>');
              }
          }
      },
  });

  $('form').on("keypress", ":input:not(textarea):not([type=submit]):not([type=button])", function(event) {
      if(event.keyCode === 13){
        event.preventDefault();
        return false;
      } 
  });
});
window.clickOrPress = function(e){
  if(e.keyCode === 13 || e.type === 'click'){
    return true;
  }
  return false;
}

window.tags = new Bloodhound({
  	datumTokenizer: Bloodhound.tokenizers.whitespace,
  	queryTokenizer: Bloodhound.tokenizers.whitespace,
  	remote: {
    	url: site_url+'/get/tags-with-fake/%QUERY',
    	wildcard: '%QUERY'
  	}
});

window.benefits = new Bloodhound({
    datumTokenizer: Bloodhound.tokenizers.whitespace,
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: {
      url: site_url+'/get/benefits-with-fake/%QUERY',
      wildcard: '%QUERY'
    }
});

window.cities = new Bloodhound({
  datumTokenizer: Bloodhound.tokenizers.whitespace,
  queryTokenizer: Bloodhound.tokenizers.whitespace,
  remote: {
    url: site_url+'/get/cities-by-query/%QUERY',
    wildcard: '%QUERY'
  }
});

window.users = new Bloodhound({
  datumTokenizer: Bloodhound.tokenizers.whitespace,
  queryTokenizer: Bloodhound.tokenizers.whitespace,
  remote: {
    url: site_url+'/get/users-by-query/%QUERY',
    wildcard: '%QUERY'
  }
});

window.companies = new Bloodhound({
  datumTokenizer: Bloodhound.tokenizers.whitespace,
  queryTokenizer: Bloodhound.tokenizers.whitespace,
  remote: {
    url: site_url+'/get/companies-by-query/%QUERY',
    wildcard: '%QUERY'
  }
});

window.vacancies = new Bloodhound({
  datumTokenizer: Bloodhound.tokenizers.whitespace,
  queryTokenizer: Bloodhound.tokenizers.whitespace,
  remote: {
    url: site_url+'/get/vacancies-by-query/%QUERY',
    wildcard: '%QUERY'
  }
});

window.getStyle = function (el, styleProp) {
  var value, defaultView = (el.ownerDocument || document).defaultView;
  // W3C standard way:
  if (defaultView && defaultView.getComputedStyle) {
    // sanitize property name to css notation
    // (hypen separated words eg. font-Size)
    styleProp = styleProp.replace(/([A-Z])/g, "-$1").toLowerCase();
    return defaultView.getComputedStyle(el, null).getPropertyValue(styleProp);
  } else if (el.currentStyle) { // IE
    // sanitize property name to camelCase
    styleProp = styleProp.replace(/\-(\w)/g, function(str, letter) {
      return letter.toUpperCase();
    });
    value = el.currentStyle[styleProp];
    // convert other units to pixels on IE
    if (/^\d+(em|pt|%|ex)?$/i.test(value)) { 
      return (function(value) {
        var oldLeft = el.style.left, oldRsLeft = el.runtimeStyle.left;
        el.runtimeStyle.left = el.currentStyle.left;
        el.style.left = value || 0;
        value = el.style.pixelLeft + "px";
        el.style.left = oldLeft;
        el.runtimeStyle.left = oldRsLeft;
        return value;
      })(value);
    }
    return value;
  }
}
//VueJS

import Home from './components/system/home/Home.vue';
import ResumeView from './components/system/user/ResumeView.vue';
import UserView from './components/system/user/UserView.vue';
import VacanciesView from './components/system/vacancy/VacanciesView.vue';
import VacancyView from './components/system/vacancy/VacancyView.vue';
import NewVacancy from './components/system/vacancy/NewVacancy.vue';
import Notifications from './components/system/global/Notifications.vue';
import Chat from './components/system/global/Chat.vue';
import ProfissionaisView from './components/system/user/ProfissionaisView.vue';

import PortfolioView from './components/system/portfolio/PortfolioView.vue';
import Timeline from './components/system/timeline/Timeline.vue';

Vue.directive('img', function(url) {
  var img = new Image();
  img.src = url;

  img.onload = function() {
    this.el.src = url;
    $(this.el)
      .css('opacity', 0)
      .animate({ opacity: 1 }, 1000)
  }.bind(this);
  
});

new Vue({
  el:'html',
  props:{
    url:'',
    stringdate:'',
    socket_id:'',
    uid:0,
    notreadmessages:0,
    notnotifiednotifications:0,
    me:{}
  },
  data:{
    date:{},
    chats:{},
    chatsLoaded:false,
    showchat:false,
    totalNotifications:0,
    tab_focused:true,
    someFriendTyping:false,


    //módulo de sugestão
    suggestion_type:'suggestion',
    showSuggestionForm:false,
    suggestion_text:'',
    submiting_suggestion:false,
    suggestion_message:''
  },
  components: {
    ResumeView,
    UserView,
    VacancyView,
    VacanciesView,
    Home,
    Notifications,
    Chat,
    PortfolioView,
    Timeline,
    NewVacancy,
    ProfissionaisView
  },
  events:{
    'startNewChat': function(user){
      this.createNewChat(user);
    },
    'toggle-follow':'toggleFollow'
  },
  ready(){
    this.setTime();
    var th = this;

    socket.emit('joinroom', th.uid);

    $('#fake-title').remove();
    window.onfocus = function() {
      th.changeWindowFocusStatus(true);
    };
    window.onblur = function() {
      th.changeWindowFocusStatus(false);
    };

    //th.$refs.notifications.load_not_notified_notifications();
    th.loadLastChats();

    $('input',$('form')).keypress(function(e){
      if ( e.which == 13 ){
        $(this).next().focus();
        e.preventDefault();
        return false;
      }
    });

    $('.autosize-textarea').textareaAutoSize().promise().done(function(){
      $('.autosize-textarea').addClass('autosize-done');
    });

    // SOCKET
    socket.on('roomJoined', function(data){
      //socket.emit('get notifications', { key: session_id });
    });
  },
  watch:{
    'showchat': function(){
      //this.$set('notreadmessages',0);
    },
    notreadmessages:'updateTotalNotifications'
  },
  methods:{
    toggleFollow(uid){
      var th = this;
      th.$http.post(th.url+'/u/follow',{
        uid:uid
      }).then((res) => {
        var data = {
          resp: res.data,
          uid: uid
        };
        th.$broadcast('follow-toggled',data);
      }).finally((res) => {
      });
    },
    setTime(){
      var th = this;
      var date_items = th.stringdate.split(',');
      var y = date_items[0];
      var m = date_items[1]-1;
      var d = date_items[2];
      var h = date_items[3];
      var i = parseInt(date_items[4]) <= 9 ? date_items[4].substring(1,2) : date_items[4];
      var s = parseInt(date_items[5]) <= 9 ? date_items[5].substring(1,2) : date_items[5];
      var date = new Date(Date.UTC(y,m,d,h,i,s));
      setInterval(function() {
          date.setSeconds(date.getSeconds() + 1);

          th.$set('date',date);
      }, 1000);
    },

    // MÓDULO DE SUGESTÃO
    sendSuggestion(){
      var th = this;
      th.$set('submiting_suggestion',true);
      th.$http.post(th.url+'/suggestion', {
        suggestion_type: th.suggestion_type,
        suggestion_text: th.suggestion_text
      }).then((resp) => {
        th.$set('submiting_suggestion',false);
        th.$set('suggestion_message',
            '<b class="text-center">'+
            (th.suggestion_type == 'error' ? 'O erro foi reportado com sucesso.':'A sugestão foi enviada com sucesso.')+'<br />Muito obrigado pela sua ajuda!</b>');
        th.$set('suggestion_text','');
        th.$set('suggestion_type','suggestion');
        setTimeout(function(){
          th.$set('suggestion_message','');
        },5000)
      }, (err) => {
        th.$set('submiting_suggestion',false);
      })
    },
    // FIM MÓDULO DE SUGESTÃO
    
    changeWindowFocusStatus(focused){
      this.$set('tab_focused',focused);
    },
    updateTotalNotifications(){
      var total = 0;
      total += this.notreadmessages;

      this.$set('totalNotifications', total);

    },
    createNewChat(user){
      var th = this;
      var chat = th.chats['u_'+user.id];
      var chatExists = undefined != chat && chat.length ? true : false;

      if( !chatExists ){
          var newChat = {
            uid:user.id,
            name:user.name,
            image:user.image,
            notified:true,
            receiver_id:user.id,
            sender_id: parseInt(th.uid),
            surname: user.surname
          }
          this.$set('chats.u_'+user.id, newChat);
      }

      th.$nextTick(function(){
        if( !chatExists ){
          chat = th.$get('chats.u_'+user.id);
        }
        th.$broadcast('loadMessages', chat);
        th.$nextTick(function(){
          th.$set('showchat', true);
        });
      });
    },
    loadLastChats(){
      var th = this;
      if( !this.chatsLoaded ){
        this.$http.get(this.url+'/get/chats').then((resp) => {
          var body = $.parseJSON(resp.body);
          th.$set('notreadmessages', body.notreadmessages);
          for( var i = 0; i < body.chats.length; i++){
            th.$set('chats.u_'+body.chats[i].uid, body.chats[i]);
          }
          this.$set('chatsLoaded',true);
        },(error) => {
          console.log(error);
        })
      }
    },
    number_format(number, decimals, dec_point, thousands_sep) {
      // http://kevin.vanzonneveld.net
      // +   original by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
      // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
      // +     bugfix by: Michael White (http://getsprink.com)
      // +     bugfix by: Benjamin Lupton
      // +     bugfix by: Allan Jensen (http://www.winternet.no)
      // +    revised by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
      // +     bugfix by: Howard Yeend
      // +    revised by: Luke Smith (http://lucassmith.name)
      // +     bugfix by: Diogo Resende
      // +     bugfix by: Rival
      // +      input by: Kheang Hok Chin (http://www.distantia.ca/)
      // +   improved by: davook
      // +   improved by: Brett Zamir (http://brett-zamir.me)
      // +      input by: Jay Klehr
      // +   improved by: Brett Zamir (http://brett-zamir.me)
      // +      input by: Amir Habibi (http://www.residence-mixte.com/)
      // +     bugfix by: Brett Zamir (http://brett-zamir.me)
      // +   improved by: Theriault
      // +   improved by: Drew Noakes
      // *     example 1: number_format(1234.56);
      // *     returns 1: '1,235'
      // *     example 2: number_format(1234.56, 2, ',', ' ');
      // *     returns 2: '1 234,56'
      // *     example 3: number_format(1234.5678, 2, '.', '');
      // *     returns 3: '1234.57'
      // *     example 4: number_format(67, 2, ',', '.');
      // *     returns 4: '67,00'
      // *     example 5: number_format(1000);
      // *     returns 5: '1,000'
      // *     example 6: number_format(67.311, 2);
      // *     returns 6: '67.31'
      // *     example 7: number_format(1000.55, 1);
      // *     returns 7: '1,000.6'
      // *     example 8: number_format(67000, 5, ',', '.');
      // *     returns 8: '67.000,00000'
      // *     example 9: number_format(0.9, 0);
      // *     returns 9: '1'
      // *    example 10: number_format('1.20', 2);
      // *    returns 10: '1.20'
      // *    example 11: number_format('1.20', 4);
      // *    returns 11: '1.2000'
      // *    example 12: number_format('1.2000', 3);
      // *    returns 12: '1.200'
      var n = !isFinite(+number) ? 0 : +number, 
          prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
          sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
          dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
          toFixedFix = function (n, prec) {
              // Fix for IE parseFloat(0.55).toFixed(0) = 0;
              var k = Math.pow(10, prec);
              return Math.round(n * k) / k;
          },
          s = (prec ? toFixedFix(n, prec) : Math.round(n)).toString().split('.');
      if (s[0].length > 3) {
          s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
      }
      if ((s[1] || '').length < prec) {
          s[1] = s[1] || '';
          s[1] += new Array(prec - s[1].length + 1).join('0');
      }
      return s.join(dec);
    }
  }
});
