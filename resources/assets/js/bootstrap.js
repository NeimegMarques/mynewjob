
window._ = require('lodash');

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

window.$ = window.jQuery = require('jquery');
require('bootstrap-sass');

/**
 * Vue is a modern JavaScript library for building interactive web interfaces
 * using reactive data binding and reusable components. Vue's API is clean
 * and simple, leaving you to focus on building your next great project.
 */

window.Vue = require('vue');
require('vue-resource');

var VueTouch = require('vue-touch')
Vue.use(VueTouch);

require('textarea-autosize');

/**
 * We'll register a HTTP interceptor to attach the "CSRF" header to each of
 * the outgoing requests issued by this application. The CSRF middleware
 * included with Laravel will automatically verify the header's value.
 */

Vue.http.interceptors.push((request, next) => {
    request.headers['X-CSRF-TOKEN'] = Laravel.csrfToken;
    next();
});

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

// import Echo from "laravel-echo"

// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: 'your-pusher-key'
// });



window.site_url = Laravel.url;
window.browserify = true;
window.socket = io.connect(site_url+':3001', {
  'connect timeout': 500,
  'reconnect': false,
  'reconnection delay': 500,
  'reopen delay': 500,
  'max reconnection attempts': 5,
  'log level':0,
  'secure':true
});

var typeahead = require('typeahead.js-browserify');
var Bloodhound = require("typeahead.js-browserify").Bloodhound;
window.Bloodhound = Bloodhound;
typeahead.loadjQueryPlugin();

$.fn.serializeObject = function()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

window.real2Float = function(valor){
    if(valor === ""){
       valor =  0;
    }else{
       valor = valor.replace(".","");
       valor = valor.replace(",",".");
       valor = parseFloat(valor);
    }
    return valor;
}

window.float2Real = function(valor)
{
  var inteiro = null, decimal = null, c = null, j = null;
  var aux = new Array();
  valor = ""+valor;
  c = valor.indexOf(".",0);
  //encontrou o ponto na string
  if(c > 0){
     //separa as partes em inteiro e decimal
     inteiro = valor.substring(0,c);
     decimal = valor.substring(c+1,valor.length);
  }else{
     inteiro = valor;
  }
   
  //pega a parte inteiro de 3 em 3 partes
  for (j = inteiro.length, c = 0; j > 0; j-=3, c++){
     aux[c]=inteiro.substring(j-3,j);
  }
   
  //percorre a string acrescentando os pontos
  inteiro = "";
  for(c = aux.length-1; c >= 0; c--){
     inteiro += aux[c]+'.';
  }
  //retirando o ultimo ponto e finalizando a parte inteiro
   
  inteiro = inteiro.substring(0,inteiro.length-1);
   
  decimal = parseInt(decimal);
  if(isNaN(decimal)){
     decimal = "00";
  }else{
     decimal = ""+decimal;
     if(decimal.length === 1){
        decimal = "0"+decimal;
     }
  }
  valor = inteiro+","+decimal;
  return valor;
}








$(function(){
  $.ajaxPrefilter(function(options, originalOptions, xhr) { // this will run before each request
    var token = Laravel.csrfToken;
    if (token) {
        return xhr.setRequestHeader('X-CSRF-TOKEN', token); // adds directly to the XmlHttpRequest Object
    }
  });

  // $('#birthdate').datetimepicker({
  //   format: "DD/MM/YYYY",
  //   icons:{
  //       time: 'fa fa-time',
  //       date: 'fa fa-calendar',
  //       up: 'fa fa-chevron-up',
  //       down: 'fa fa-chevron-down',
  //       previous: 'fa fa-chevron-left',
  //       next: 'fa fa-chevron-right',
  //       today: 'fa fa-screenshot',
  //       clear: 'fa fa-trash',
  //       close: 'fa fa-remove'
  //   }
  // });
});



$(function(){
	//DRY => DON'T REPEAT YOURSELF
	$('.city-typeahead').on('focus', function(){
		var parent = $(this).closest('.form-group');
		if(!$(this).hasClass('tt-hint') && !$(this).hasClass('tt-input')){
		    $(this).typeahead({
			    hint: true,
			    highlight: true,
			    minLength: 2
		    },
		    {
		    	name: 'cities',
		    	source: cities,
		    	display:'nameWithCode',
		    	templates: {
		    		empty: [
		    			'<div class="empty-message">',
		    			'Pesquise pelo nome da cidade',
		    			'</div>'
		    		].join('\n'),
		    		suggestion: function(res){
		    			return $('<div><strong>'+res.name+'</strong> – '+res.state.code+'</div>');
		        	}
		      	}
		    })
		    .promise().done(function(){
		    	$(parent).find('.tt-input').focus();
		    });
		}
	})
	.on('typeahead:selected', function($e, datum){
		var form = $(this).closest('form');
		form.find('input[name=city_id]').remove();
		form.append($('<input name="city_id" type="hidden">').val(datum.id));
	});
});
