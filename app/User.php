<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Cashier\Billable;
use Carbon\Carbon;

class User extends Authenticatable
{
    use Notifiable;
    use Billable;

    public $appends = ['image_path','thumb','link','is_me'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','surname','slug', 'email', 'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','card_last_four','stripe_id','about'
    ];

    public static function boot()
    {
        parent::boot();
        static::creating(function($user){
            $user->email_token = str_random(30);
        });
    }

    public function getStrippedAboutAttribute(){
        $breaks = array("<br />","<br>","<br/>");  
        return str_ireplace($breaks, "\r\n", $this->about);
    }
    
    public function getInitialsAttribute(){
        $arr_name = explode(' ',preg_replace('/\s+/', ' ',iconv('UTF-8', 'ASCII//TRANSLIT', $this->name)));
        $len = count($arr_name);
        if ($len == 1){
            $arr_surname = explode(' ',preg_replace('/\s+/', ' ',iconv('UTF-8', 'ASCII//TRANSLIT', $this->surname)));
            $surname_arr_length = count(@$arr_surname);
            if($surname_arr_length == 1){
                $second_part = substr(@$arr_surname[0], 0, 1);
            } else if($surname_arr_length >= 2){
                $second_part = ctype_upper(substr(@$arr_surname[0], 0, 1)) ? substr(@$arr_surname[0],0,1) : substr(@$arr_surname[1],0,1);    
            }
        }else if($len >= 2){
            $second_part = ctype_upper(substr(@$arr_name[1], 0, 1)) ? substr(@$arr_name[1],0,1): substr(@$arr_name[2],0,1);
        }
        $initials = strtoupper(substr($arr_name[0], 0, 1).@$second_part);
        return $initials;
    }

    public function getImagePathAttribute(){
        return $this->image ? 
                asset('storage/uploads/users/'.$this->id.'/img/'.@$this->image):
                asset('storage/uploads/users/placeholders/'.$this->initials.'.jpg');
    }
    public function getThumbAttribute(){
        return $this->image ? 
                asset('storage/uploads/users/'.$this->id.'/img/thumb_'.$this->image):
                asset('storage/uploads/users/placeholders/'.$this->initials.'-thumb.jpg');
    }

    public function getLinkAttribute(){
        return url('u/'.$this->slug);
    }

    public function getIAmFollowingAttribute(){
        return @in_array($this->id, @\Auth::user()->follows()->pluck('id')->toArray());
    }

    public function getIsMeAttribute(){
        if((@\Auth::user()->id == $this->id) && @\Request::get('view_as_visitor') == true) {
            return false;
        }
        return @\Auth::user()->id == $this->id;
    }
    
    public function getBirthdateAttribute(){
        return $this->birth_day.'/'.$this->birth_month.'/'.$this->birth_year;
    }
    public function getBirthDayAttribute(){
        $d = Carbon::parse($this->birth);
        return ($d->day>=10?'':'0').$d->day;
    }
    public function getBirthMonthAttribute(){
        $d = Carbon::parse($this->birth);
        return ($d->month>=10?'':'0').$d->month;
    }
    public function getBirthYearAttribute(){
        $d = Carbon::parse($this->birth);
        return $d->year;
    }

    public function getOccupationAttribute(){
        $occupation = $this->jobs()->whereNull('end')->first();
        if($occupation)
        {
            $article = 'na empresa';
        }

        if(!@$occupation)
        {
            $occupation = $this->educations()->whereNull('end')->first();
            
            if(@$occupation)
            {
                $article = 'em';
            }
        }
        @$occupation->article = @$article;
        return @$occupation;
    }

    public function notifications(){
        return $this->hasMany('App\Models\Notification','receiver_id')->take(10)->orderBy('id','DESC');
    }

    public function candidacies(){
        return $this->hasMany('App\Models\Candidacy');
    }

    public function vacancies(){
        return $this->hasMany('App\Models\Vacancy');
    }

    public function social(){
        return $this->hasOne('App\Models\SocialLink','user_id');
    }

    public function phones(){
        return $this->morphMany('App\Models\Phone','phoneable');
    }

    public function likedposts(){
        return $this->morphedByMany('App\Post', 'likeable');
    }

    public function emails(){
        return $this->morphMany('App\Models\Email','emailable');
    }

    public function follows()
    {
        return $this->belongsToMany('App\User','follows','user_id','followed_id')
            ->whereNull('follows.deleted_at')
            ->withTimestamps();
    }

    public function followers(){
        return $this->belongsToMany('App\User','follows','followed_id','user_id')
            ->whereNull('follows.deleted_at') // Table `follows` has column `deleted_at`
            ->withTimestamps(); // Table `follows` has columns: `created_at`, `updated_at`
    }

    public function city()
    {
        return $this->belongsTo('App\Models\City');
    }

    public function jobs()
    {
        return $this->hasMany('App\Models\Job','user_id')->orderBy('start','DESC')->orderBy('end','DESC');
    }

    public function educations()
    {
        return $this->hasMany('App\Models\Education','user_id')->orderBy('start','DESC')->orderBy('end','DESC');
    }

    public function courses()
    {
        return $this->hasMany('App\Models\Course','user_id')->orderBy('start','DESC')->orderBy('end','DESC');
    }

    public function portfolios()
    {
        return $this->hasMany('App\Models\Portfolio')->orderBy('position');
    }

    public function certificates()
    {
        return $this->hasMany('App\Models\Certificate','user_id');
    }

    public function languages()
    {
        return $this->hasMany('App\Models\Language','user_id');
    }   

    public function skills()
    {
        $query = $this
            ->hasMany('App\Models\Skill')
            ->selectRaw('skills.id,skills.user_id,skills.tag_id, count(recommendations.id) as recoms')
            ->leftJoin('recommendations','skills.id','=','recommendations.skill_id')
            ->groupBy('skills.id')
            ->groupBy('skills.user_id')
            ->groupBy('skills.tag_id')
            ->orderBy('recoms','DESC');
        return $query;
    }

    public function companies()
    {
        return $this->morphToMany('App\Models\Company', 'companiable');
    }

    public function isAbleTo($action)
    {
        if($action == 'apply'){
            if(
                (
                    $this->skills->count()
                ) && (
                    $this->phones->count() || 
                    $this->emails->count()
                ) && (
                    $this->educations->count() ||
                    $this->courses->count() ||
                    $this->jobs->count()
                )
            ) {
                return true;
            }
            return false;
        }
    }

    public function scopeActive($query, $flag = 1)
    {
        return $query->where('confirmed', $flag);
    }
}
