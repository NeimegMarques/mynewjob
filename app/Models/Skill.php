<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Skill extends Model
{
    public $with = ['recommendations','recommendations.user'];
    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public function tag()
    {
    	return $this->belongsTo('App\Models\Tag');
    }

    public function recommendations()
    {
    	return $this->hasMany('App\Models\Recommendation');
    }
}
