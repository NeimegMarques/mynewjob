@extends('layouts.front')

@section('content')
<section class="callout">
    <div class="container text-upper text-center">
        <h3>novas vagas todos os dias, a próxima pode ser sua!</h3>
    </div>
</section>
<section>
    {{Form::open(['url'=>url('contratar/plano')])}}
    <div class="container">
        <div class="row">
            <br>
            <div class="col-sm-16 col-sm-offset-4">
                <ul class="list-group plans-list-selector">
                    @include('subscriptions.'.$view_name)
                </ul> <!-- end plans-list-selector -->
                <br>
                <p class="text-center">
                    <button class="btn btn-success btn-xlg">
                        Ok, vamos prosseguir <i class="fa fa-chevron-right"></i>
                    </button>
                </p>
            </div>
        </div>
    </div>
    {{Form::close()}}
</section>
@endsection
