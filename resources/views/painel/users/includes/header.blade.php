<section class="user-header">
    <div class="user-header-dark">
        <div class="container">
            <div class="row">
                <div class="container-fluid">
                    <div class="row">
                        <div class=container-fluid>
                            <div class="user-menu">
                                <nav class="navbar navbar-default">
                                    <!-- Brand and toggle get grouped for better mobile display -->
                                    <div class="navbar-header">
                                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#user-menu" aria-expanded="false">
                                            <span class="sr-only">Toggle navigation</span>
                                            <i class="fa fa-ellipsis-v"></i>
                                        </button>
                                    </div>

                                    <!-- Collect the nav links, forms, and other content for toggling -->
                                    <div class="collapse navbar-collapse" id="user-menu">
                                        <ul class="nav navbar-nav user-ul-menu">
                                            <li class="active">
                                                <a href="#inicio">Início <span class="sr-only">(current)</span></a>
                                            </li>
                                            <li><a href="#competencias">Competências</a></li>
                                            <li><a href="#educacao">Educação</a></li>
                                            <li><a href="#trabalho">Trabalho</a></li>
                                            <li><a href="#idiomas">Idiomas</a></li>
                                            <li><a href="#certificados">Certificados</a></li>
                                            <li><a href="#cursos">Cursos</a></li>
                                        </ul>
                                        <ul class="social-menu nav navbar-nav pull-right ">
                                            @if(@$user->social->facebook)
                                            <li>
                                                <a target="_blank" href="{{$user->social->facebook}}" class="link-facebook">
                                                    <i class="fa fa-facebook"></i>
                                                </a>
                                            </li>
                                            @endif
                                            @if(@$user->social->twitter)
                                            <li>
                                                <a target="_blank" href="{{$user->social->twitter}}" class="link-twitter">
                                                    <i class="fa fa-twitter"></i>
                                                </a>
                                            </li>
                                            @endif
                                            @if(@$user->social->google_plus)
                                            <li>
                                                <a target="_blank" href="{{$user->social->google_plus}}" class="link-google-plus">
                                                    <i class="fa fa-google-plus"></i>
                                                </a>
                                            </li>
                                            @endif
                                            @if(@$user->social->pinterest)
                                            <li>
                                                <a target="_blank" href="{{$user->social->pinterest}}" class="link-pinterest">
                                                    <i class="fa fa-pinterest"></i>
                                                </a>
                                            </li>
                                            @endif
                                            @if(@$user->social->linkedin)
                                            <li>
                                                <a target="_blank" href="{{$user->social->linkedin}}" class="link-linkedin">
                                                    <i class="fa fa-linkedin"></i>
                                                </a>
                                            </li>
                                            @endif
                                            @if(@$user->social->behance)
                                            <li>
                                                <a target="_blank" href="{{$user->social->behance}}" class="link-behance">
                                                    <i class="fa fa-behance"></i>
                                                </a>
                                            </li>
                                            @endif
                                            @if(@$user->social->instagram)
                                            <li>
                                                <a target="_blank" href="{{$user->social->instagram}}" class="link-instagram">
                                                    <i class="fa fa-instagram"></i>
                                                </a>
                                            </li>
                                            @endif
                                        </ul>
                                    </div><!-- /.navbar-collapse -->
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>