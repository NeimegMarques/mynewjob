@extends('layouts.front')

@section('content')
<section class="callout">
	<div class="container text-upper text-center">
			<h3>novas vagas todos os dias, a próxima pode ser sua!</h3>
	</div>
</section>
<section>    
	{{Form::open(['url'=>url('pagar'), 'class'=>'form-horizontal','id'=>'form-payment','role'=>'form'])}}
        <div class="container">
            <div class="row">
                <div class="col-sm-14 col-sm-offset-5">
                    
                </div>
            </div>
            <div class="row">
                @if (!Auth::user()->subscribed(Input::get('p')))
                <div class="col-sm-14 col-sm-offset-5">
                    <div class="panel panel-primary panel-payment">
                        <div class="panel-heading text-center text-upper">
                            {{Input::get('p')}}
                        </div>
                        <div class="panel-body text-center">
                            <h1 style="font-size: 60px;">R${{number_format(env(strtoupper((\Input::get('p')).'_PRICE'))/100,2,',','.')}}<span style="font-size: 14px; color:rgba(0,0,0,0.8)">/mês</span></h1>
                        </div>
                        <div class="panel-footer">
                            <button class="btn btn-xlg btn-success btn-block" id="pay-button"><i class="fa fa-credit-card"></i>&nbsp;&nbsp; Pagar  </button>
                        </div>
                    </div>
                </div>
                @else
                    <div class="col-sm-14 col-sm-offset-5">
                        <h3 class="text-center">Você já possui este plano.
                            @if(!Auth::user()->subscribed('vip'))
                            Deseja <a href="{{url('pagar?p=vip')}}">fazer um upgrade?</a>
                            @endif
                        </h3>
                        <br>
                    </div>
                @endif
            </div>
        </div>	
	</form>
    <div class="container">
        <div class="row">
            <div class="col-sm-14 col-sm-offset-5">
                <div class="alert alert-success">
                    <p class="">
                        Você pode cancelar a sua assinatura a qualquer momento e não perderá o que já foi pago, ou seja, sua assinatura estará cancelada a partir da próxima renovação e você poderá usar o site normalmente até lá a menos que encerre a sua conta.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@push('script-bottom')
	<script src="https://checkout.stripe.com/checkout.js"></script>

	<script>
		var handler = StripeCheckout.configure({
			key: '{{env('STRIPE_KEY')}}',
			image: 'http://icons.iconarchive.com/icons/sykonist/looney-tunes/256/Marvin-Martian-icon.png',
			locale: 'auto',
			token: function(token) {
                console.log(token);
				// Use the token to create the charge with a server-side script.
				// You can access the token ID with `token.id`

                $('#form-payment')
                    .before('<div class="container text-center"><img style="margin: 0 auto;" src="{{asset('/img/loading.gif')}}"><h3>Configurando sua conta. Aguarde...<br></h3></div>')
                    .append($('<input type="hidden" name="stripeToken" value="'+token.id+'">'))
                    .append($('<input type="hidden" name="stripeEmail" value="'+token.email+'">'))
                    .append($('<input type="hidden" name="plan" value="{{Input::get('p')}}">'))
                    .find('')
                    .promise()
                    .done(function(){
                        $('#pay-button').attr('disabled','disabled').addClass('disabled');
                        $('#form-payment').submit();
                    });
			}
		});

		$(document).on('click', '#pay-button', function(e) {
			// Open Checkout with further options
			handler.open({
				name: 'Demo Site',
				description: 'Assinatura mensal',
				amount: {{env(strtoupper(\Input::get('p').'_PRICE'))?:0}}
			});
			e.preventDefault();
		});

		// Close Checkout on page navigation
		$(window).on('popstate', function() {
			handler.close();
		});
	</script>
@endpush
