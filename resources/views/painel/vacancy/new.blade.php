@extends('layouts.app',['page'=>'new-vacancy', 'props' =>[
    ':vacancy_redirect' => @$vacancy->redirect ? 'true' :'false'
]])

@section('content')
<section class="bg-primary top-background-bar"></section>
<div>
    <div class="container">
        <div class="row">
            <div class="container-fluid">
                <div class="frame page-inner">
                    <div class="row">
                        <div class="container-fluid">
                            {!! Form::open(['url'=>url('v'.(@$vacancy?'/'.$vacancy->id:'')),'files'=>true])!!}
                            @if(@$vacancy)
                            {!!Form::hidden('_method','PUT')!!}
                            @endif
                            <div class="row">
                                <div class="container-fluid">
                                    <h2 class="system-title">
                                        <i class="system-title-icon glyphicon glyphicon-edit"></i> {{@$vacancy ? 'Editar' : 'Publicar uma ' }} vaga
                                    </h2>
                                </div>
                            </div>
                            <br><br>
                            <div class="row">
                                <div class="col-sm-20 col-sm-offset-2">
                                    <h4 class="system-title subtitle">
                                        <span class="text-info">Informações da Empresa</span>
                                    </h4>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="container-fluid">
                                                <label for="">Empresa</label>
                                                <select name="company_id" class="form-control">
                                                    @foreach($companies as $i => $comp)
                                                        <option
                                                            @if($i===0 && old("company_id") == '')
                                                              selected
                                                            @endif
                                                            @if(@old("company_id") === $i 
                                                                || @$vacancy->company_id === $i
                                                            ) selected
                                                            @endif 
                                                            @if($i === 0) disabled @endif value="{{$i}}">{{$comp}}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                @if($errors->first('company_id'))
                                                    <span class="text-danger small">{{$errors->first('company_id')}}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="company-detail-inputs @if(@old("company_id") != 'new') hidden @endif">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="container-fluid">
                                                    <label for="">Nome da empresa</label>
                                                    {!!Form::text('company_name',old('company_name') ?:'' ,['class'=>'form-control '])!!}
                                                    @if($errors->first('company_name'))
                                                        <span class="text-danger small">{{$errors->first('company_name')}}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    @forelse((array)old('company_sector') as $key => $cs)
                                                        <div class="row clone-wrapper" @if($key>0) style="margin-top:20px" @endif>
                                                            <div class="col-sm-18">
                                                                @if($key==0)<label for="">Ramo de atuação da empresa</label>@endif
                                                                <div class="select-wrapper">
                                                                    @if($sectors)
                                                                    <select name="company_sector[]" class="form-control  select_sectors">
                                                                        @foreach($sectors as $si => $sector)
                                                                        <option {{$si==$cs?'selected':''}} value="{{$si}}">{{$sector}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                @if($key==0)<label for="">&nbsp;</label>@endif
                                                                <button
                                                                    type="button"
                                                                    class="btn-{{$key==0?'add':'remove'}}-sector btn btn-block btn-default"
                                                                    data-toggle="tooltip"
                                                                    data-placement="top"
                                                                    title="Adicionar setor de atividade"
                                                                >
                                                                    <i class="fa fa-{{$key==0?'plus':'minus'}}"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    @empty
                                                        <div class="row clone-wrapper">
                                                            <div class="col-sm-18">
                                                                <label for="">Ramo de atuação</label>
                                                                <div class="select-wrapper">
                                                                    {!!Form::select('company_sector[]',$sectors,'',['class'=>'form-control  select_sectors'])!!}
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <label for="">&nbsp;</label>
                                                                <button type="button" class="btn-add-sector btn btn-block btn-default" data-toggle="tooltip" data-placement="top" title="Adicionar setor de atividade">
                                                                    <i class="fa fa-plus"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    @endforelse
                                                </div>
                                                <div class="col-sm-12">
                                                    <label for="">Logo</label>
                                                    {!!Form::file('company_logo',@old('company_logo'),['class'=>'form-control'])!!}
                                                    <div class="logo-wrapper" style="max-width:200px; max-height: 160px;">
                                                        <!-- <img src="https://cdn.colorlib.com/wp/wp-content/uploads/sites/2/2014/02/Olympic-logo.png" alt="" class="img-responsive"> -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Descrição</label>
                                            {!!Form::textarea('company_description',@old('company_description'),['class'=>'form-control ','rows'=>3])!!}
                                            @if($errors->first('company_description'))
                                                <span class="text-danger small">{{$errors->first('company_description')}}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br><br>
                            <div class="row">
                                <div class="col-sm-20 col-sm-offset-2">
                                    <h4 class="system-title subtitle text-info">
                                        <span class="text-info">Informações da vaga</span>
                                    </h4>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <label for="">Título</label>
                                                {!!Form::text('vacancy_name',@old('vacancy_name')?:@$vacancy->name,['class'=>'form-control '])!!}
                                                @if($errors->first('vacancy_name'))
                                                    <span class="text-danger small">{{$errors->first('vacancy_name')}}</span>
                                                @endif
                                            </div>
                                            <div class="col-sm-12">
                                                <label for="">Experiência requerida</label>
                                                {!!Form::select('vacancy_experience[]',$experiences, @old('vacancy_experience[]')?@old('vacancy_experience[]'):count(@$vacancy->experiences) > 0 ? @$vacancy->experiences->pluck('id')->toArray() : [] ,['class'=>'form-control selectpicker','multiple'=>true,'title'=>'Nada selecionado'])!!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <label for="">Regime de trabalho</label>
                                                {!!Form::select('vacancy_jobtype',$jobtypes, @old('vacancy_jobtype')?:@$vacancy->jobtype_id,['class'=>'form-control '])!!}
                                            </div>
                                            <div class="col-sm-12 clearfix">
                                                <label for="">Cidade</label>
                                                {!!Form::text('vacancy_city', @old('vacancy_city')?:@$vacancy->city->name ,['class'=>'form-control  city-typeahead'])!!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <label for="">Benefícios</label>
                                                {!!Form::text('vacancy_benefits', @old('vacancy_benefits'),['class'=>'form-control  vacancy-benefits-input','placeholder'=>'Vale transporte, Vale refeição, Vale alimentação...'])!!}
                                                @if($errors->first('vacancy_benefits'))
                                                    <span class="text-danger small">{{$errors->first('vacancy_benefits')}}</span>
                                                @endif
                                            </div>
                                            <div class="col-sm-12">
                                                <label for="">Quantidade de vagas</label>
                                                {!!Form::text('vacancy_quantity', @old('vacancy_quantity')?:(@$vacancy->quantity?:1),['class'=>'form-control','min'=>'0'])!!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <label for="">Conhecimentos requeridos</label>
                                                {!!Form::text('vacancy_tags', @old('vacancy_tags'),['class'=>'form-control  vacancy-tags-input','placeholder'=>'Word, Excel, Photoshop...'])!!}
                                                @if($errors->first('vacancy_tags'))
                                                    <span class="text-danger small">{{$errors->first('vacancy_tags')}}</span>
                                                @endif
                                            </div>
                                            <div class="col-sm-12">
                                                <label for="">Conhecimentos desejáveis (diferenciais)</label>
                                                {!!Form::text('vacancy_desired_skills', @old('vacancy_desired_skills'),['class'=>'form-control  vacancy-desired-skills-input','placeholder'=>'Word, Excel, Photoshop...'])!!}
                                                @if($errors->first('vacancy_desired_skills'))
                                                    <span class="text-danger small">{{$errors->first('vacancy_desired_skills')}}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <?php
                                                    if(old('company_sector'))
                                                    {
                                                        $vacancy_sectors = old('vacancy_sector');
                                                    }
                                                    elseif($vacancy)
                                                    {
                                                        $vacancy_sectors = @$vacancy->sectors->pluck('id')->toArray();
                                                    }
                                                ?>
                                                @forelse((array)@$vacancy_sectors as $key => $cs)
                                                    <div class="{{$cs}} row clone-wrapper" @if($key>0) style="margin-top:20px" @endif>
                                                        <div class="col-sm-18">
                                                            @if($key==0)<label for="">Área de atuação da vaga</label>@endif
                                                            <div class="select-wrapper">
                                                                @if($sectors)
                                                                <select name="vacancy_sector[]" class="form-control  select_sectors">
                                                                    @foreach($sectors as $si => $sector)
                                                                    <option {{$si==$cs?'selected':''}} value="{{$si}}">{{$sector}}</option>
                                                                    @endforeach
                                                                </select>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            @if($key==0)<label for="">&nbsp;</label>@endif
                                                            <button
                                                                type="button"
                                                                class="btn-{{$key==0?'add':'remove'}}-sector btn btn-block btn-default"
                                                                data-toggle="tooltip"
                                                                data-placement="top"
                                                                title="Adicionar setor de atividade"
                                                            >
                                                                <i class="fa fa-{{$key==0?'plus':'minus'}}"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                @empty
                                                    <div class="row clone-wrapper">
                                                        <div class="col-sm-18">
                                                            <label for="">Área de atuação</label>
                                                            <div class="select-wrapper">
                                                                {!!Form::select('vacancy_sector[]',$sectors,'',['class'=>'form-control  select_sectors'])!!}
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <label for="">&nbsp;</label>
                                                            <button type="button" class="btn-add-sector btn btn-block btn-default" data-toggle="tooltip" data-placement="top" title="Adicionar setor de atividade">
                                                                <i class="fa fa-plus"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                @endforelse
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="form-group slider-vacancy-salary">
                                        <label for="" class="control-label">Salário/Pagamento previsto {{old('vacancy_salary')}}</label>
                                        <div class="row">
                                            <div class="container-fluid">
                                                <h3 style="margin-bottom:30px">
                                                    <b v-show="salary_to > 0">R$ @{{salary_from_formatted}} - R$ @{{salary_to_formatted}}</b>
                                                    <b v-show="salary_to <= 0">A combinar</b>
                                                </h3>
                                                <input type="hidden" name="vacancy_salary_from" value="@{{salary_from}}">
                                                <input type="hidden" name="vacancy_salary_to" value="@{{salary_to}}">
                                                <vue-slider v-on:callback="updateSalary()" v-ref:slider :interval="50" :height="8" :dot-size="26" :value="[{{old('vacancy_salary_from') > 0 ? old('vacancy_salary_from') : 'salary_from'}} , {{old('vacancy_salary_to') > 0 ? old('vacancy_salary_to') : 'salary_to'}}]" :min="0" :max="10001" :show="true" tooltip="hover"></vue-slider>
                                            </div>
                                        </div>
                                    </div>
                                    {{-- <div class="form-group slider-vacancy-salary">
                                        <label for="" class="control-label">Salário/Pagamento previsto {{old('vacancy_salary')}}</label>
                                        <div class="row">
                                            <div class="col-sm-18">
                                                <input class="form-control" data-slider-value="{{old('vacancy_salary')?:@$vacancy->salary}}" name="vacancy_salary" id="slider-vacancy-salary" />
                                            </div>
                                            <div class="col-sm-6">
                                                <a class="manually-insert-salary text-center text-info">Inserir manualmente</a>
                                                <div class="input-group slider-vacancy-manual-salary-wrapper hidden ">
                                                    <span class="input-group-addon" id="basic-addon1">R$</span>
                                                    <input type="text" class="form-control" placeholder="Pretensão salarial" value="{{old('vacancy_salary')?:@$vacancy->salary}}" aria-describedby="basic-addon1" id="slider-vacancy-manual-salary">
                                                </div>
                                            </div>
                                        </div>
                                    </div> --}}
                                    <div class="form-group">
                                        <div class="row">
                                            <br>
                                            <div class="col-sm-24">
                                                <label for="">Redirecionar candidatos para o link da vaga em outro site?</label>
                                                <div class="slideOnOff">
                                                    <input type="checkbox" id="slideLinkExterno" name="vacancy_redirect" v-model="vacancy_redirect"/>
                                                    <span class="off">Não</span>
                                                    <span class="on">Sim</span>
                                                    <label for="slideLinkExterno"></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group" v-if="!vacancy_redirect">
                                        <div class="row">
                                            <br>
                                            <div class="col-sm-24">
                                                <label for="">Exigir pretensão salarial dos candidatos?</label>
                                                <div class="slideOnOff">
                                                    <input type="checkbox" id="slidePretensaoSalarial" name="vacancy_required_salary"/>
                                                    <span class="off">Não</span>
                                                    <span class="on">Sim</span>
                                                    <label for="slidePretensaoSalarial"></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group" v-if="vacancy_redirect">
                                        <div class="row">
                                            <br>
                                            <div class="col-sm-12">
                                                <label for="">Link da vaga</label>
                                                {!!Form::text('vacancy_link',@old('vacancy_link')?:@$vacancy->link,['class'=>'form-control '])!!}
                                                @if($errors->first('vacancy_link'))
                                                    <span class="text-danger small">{{$errors->first('vacancy_link')}}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <br>
                                        <label for="">Descrição da vaga</label>
                                        <textarea rows="6" name="vacancy_description" class="form-control">{{ @old('vacancy_description')?:str_replace('<br />','',@$vacancy->description) }}</textarea>
                                        @if($errors->first('vacancy_description'))
                                            <span class="text-danger small">{{$errors->first('vacancy_description')}}</span>
                                        @endif
                                    </div>
                                    <hr>
                                    <div class="form-group">
                                        <button class="btn btn-success">Salvar{{$vacancy?' alterações':''}}</button>
                                    </div>
                                </div>
                            </div>
                            @if(old('city_id') || $vacancy)
                                {{Form::hidden('city_id',old('city_id')?:$vacancy->city_id)}}
                            @endif
                            {!! Form::close()!!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('script-bottom')
<script>
    $(function () {
        $(document).on('change','select[name=company_id]', function(){
            var val = $(this).val();


            if(val > 0){
                $('.company-detail-inputs').addClass('hidden');
            }else{
                $('.company-detail-inputs').removeClass('hidden');
            }
        })
        .on('click','.btn-add-sector',function(){
            var closest = $(this).closest('.clone-wrapper');
            var clone = closest.clone();
            var select = $('.select_sectors', clone.css('margin-top',20));

            clone.find('.select-wrapper').append(select).promise().done(function(){
                $('.bootstrap-select', clone).remove();

                clone.find('select').selectpicker({
                    style: 'btn-default',
                    liveSearch:true,
                });
                clone.find('label').remove();
                clone.find('.btn-add-sector').removeClass('btn-add-sector').addClass('btn-remove-sector').find('.fa-plus').removeClass('fa-plus').addClass('fa-minus');
                clone.insertAfter(closest);
            });
        })
        .on('click','.btn-remove-sector', function(){
            var closest = $(this).closest('.clone-wrapper');
            closest.remove();
        })
        // .on('click','.manually-insert-salary', function(){
        //     $(".slider-vacancy-manual-salary-wrapper").removeClass('hidden').find('input').focus().prop('placeholder','1.234,56');
        //     $(this).addClass('hidden');
        // })
        .on('keypress','[name=vacancy_quantity]', function(e){
            var charCode = (e.which) ? e.which : e.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
            {
                e.preventDefault();
                return false;
            }
        })
        .on('keyup','[name=vacancy_quantity]', function(e){
            var num = $(this).val().replace(/\D+/g, '');
            $(this).val(num);
        })
        .on('blur','[name=vacancy_quantity]', function(e){
            var num = $(this).val().replace(/\D+/g, '');
            if(num < 1) num = 1;
            $(this).val(num);
        });

        // var slider = $("#slider-vacancy-salary").slider({
        //     min: 0,
        //     max: 10010,
        //     //scale: 'logarithmic',
        //     step: 10,
        //     tooltip: 'always',
        //     tooltip_position:'bottom',
        //     formatter: function(val){
        //         if(val>10000){
        //             return 'Mais de R$ '+float2Real(10000);
        //         }else{
        //             return 'R$ '+float2Real(val);
        //         }
        //     }
        // }).on('slide', function( event) {
        //     $("#slider-vacancy-manual-salary").val(float2Real(event.value));
        // });

        // $("#slider-vacancy-manual-salary").on('keyup', function(){
        //     var val = $(this).val();

        //     slider.slider('setValue',real2Float(val));
        // });


		//$('#show-vacancy-salary').text(slider.slider('value'));

        $('.vacancy-tags-input,.vacancy-desired-skills-input').tagsinput({
            itemText: 'name',
            itemValue: 'str_id',
            typeaheadjs: {
                name: 'tags',
                displayKey: 'name',
                //valueKey: 'nameWithCode',
                source: tags,
                templates: {
                    empty: [
                      '<div class="empty-message"></div>'
                    ].join('\n'),
                    suggestion: function(res){
                        return $('<div>'+res.name+'</div>');
                    }
                }
            },
        });

        $('.vacancy-benefits-input').tagsinput({
            itemText: 'name',
            itemValue: 'str_id',
            typeaheadjs: {
                name: 'benefits',
                displayKey: 'name',
                //valueKey: 'nameWithCode',
                source: benefits,
                templates: {
                    empty: [
                      '<div class="empty-message"></div>'
                    ].join('\n'),
                    suggestion: function(res){
                        return $('<div>'+res.name+'</div>');
                    }
                }
            },
        });

        @if($vacancy)
            @foreach($vacancy->tags as $tag) 
            $('.vacancy-tags-input').tagsinput('add',
                {'str_id':'{{$tag->id}}','name':'{!!$tag->name!!}'}
            );
            @endforeach

            @foreach($vacancy->desiredskills as $tag) 
            $('.vacancy-desired-skills-input').tagsinput('add',
                {'str_id':'{{$tag->id}}','name':'{!!$tag->name!!}'}
            );
            @endforeach

            @foreach($vacancy->benefits as $benefit) 
            $('.vacancy-benefits-input').tagsinput('add',
                {'str_id':'{{$benefit->id}}','name':'{!!$benefit->name!!}'}
            );
            @endforeach
        @endif

        @if(old('vacancy_tags'))
            <?php $tags = explode(',', old('vacancy_tags'))?>
            @foreach($tags as $tag) 
                @if( !is_numeric($tag) && $tag != '')
                $('.vacancy-tags-input').tagsinput('add',
                    {'str_id':'{{$tag}}','name':'{!!$tag!!}'}
                );
                @endif
            @endforeach
        @endif

        @if(old('vacancy_desired'))
            <?php $tags = explode(',', old('vacancy_desired'))?>
            @foreach($tags as $tag) 
                @if( !is_numeric($tag) && $tag != '')
                $('.vacancy-desired-skills-input').tagsinput('add',
                    {'str_id':'{{$tag}}','name':'{!!$tag!!}'}
                );
                @endif
            @endforeach
        @endif

        @if(old('vacancy_benefits'))
            <?php $benefits = explode(',', old('vacancy_benefits'))?>
            @foreach($benefits as $benefit) 
                @if( !is_numeric($benefit) && $benefit != '')
                $('.vacancy-benefits-input').tagsinput('add',
                    {'str_id':'{{$benefit}}','name':'{!!$benefit!!}'}
                );
                @endif
            @endforeach
        @endif

        $('.vacancy-city-typeahead').on('focus', function(){
            var parent = $(this).closest('.form-group');
            if(!$(this).hasClass('tt-hint') && !$(this).hasClass('tt-input')){
                $(this).typeahead({
                    hint: true,
                    highlight: true,
                    minLength: 2
                },
                {
                    limit:10,
                    name: 'cities',
                    source: cities,
                    display:'nameWithCode',
                    templates: {
                    empty: [
                      '<div class="empty-message">',
                        'Pesquise pelo nome da cidade',
                      '</div>'
                    ].join('\n'),
                    suggestion: function(res){
                        return $('<div><strong>'+res.name+'</strong> – '+res.state.code+'</div>');
                    }
                  }
                })
                .promise().done(function(){
                    $(parent).find('.tt-input').focus();
                });
            }
        })
        .on('typeahead:selected', function($e, datum){
            var form = $(this).closest('form');
            form.find('input[name=city_id]').remove();
            form.append($('<input name="city_id" type="hidden">').val(datum.id));
        });

        $('select.select_sectors').each(function(i,item){
            $(item).selectpicker({
                style: 'btn-default',
                liveSearch:true,
            });
        });
    });
</script>
@endpush
