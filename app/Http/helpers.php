<?php
function user_path()
{
	return public_path('uploads/users/');
}

function float2Real($float){
	return 'R$ '.number_format($float, 2, ',', '.');
}

function months()
{
	for($i=1;$i<=12;$i++)
	{
		$months[$i] = Lang::get('dates.months.'.$i);
	}

	return $months;
}

function years($start=NULL, $end=NULL)
{
	$start = $start?:date('Y')-50;
	$end = $end?:date('Y');
	for($i=$end;$i>=$start;$i--)
	{
		$years[$i] = $i;
	}

	return $years;
}

function language_levels($level = NULL)
{
	$levels = [
		'beginner'=>'Inciante',
		'basic'=>'Básico',
		'intermediate'=>'Intermediário',
		'advanced'=>'Avançado',
		'fluent'=>'Fluente',
		'native'=>'Nativo'
	];

	return $level?$levels[$level]:$levels;
}

function languages($lang = NULL)
{
	$langs = [
		'pt_br'=>'Português',
		'en'=>'Inglês',
		'es'=>'Espanhol'
	];

	return $lang?$langs[$lang]:$langs;
}

function sanitizeContent($content){
	return nl2br(preg_replace("/[\r\n]+/", "\n", strip_tags($content)));
}

function adjustBrightness($hex, $steps) {
    // Steps should be between -255 and 255. Negative = darker, positive = lighter
    $steps = max(-255, min(255, $steps));

    // Normalize into a six character long hex string
    $hex = str_replace('#', '', $hex);
    if (strlen($hex) == 3) {
        $hex = str_repeat(substr($hex,0,1), 2).str_repeat(substr($hex,1,1), 2).str_repeat(substr($hex,2,1), 2);
    }

    // Split into three parts: R, G and B
    $color_parts = str_split($hex, 2);
    $return = '#';

    foreach ($color_parts as $color) {
        $color   = hexdec($color); // Convert to decimal
        $color   = max(0,min(255,$color + $steps)); // Adjust color
        $return .= str_pad(dechex($color), 2, '0', STR_PAD_LEFT); // Make two char hex code
    }

    return $return;
}

function create_links($text){
	// The Regular Expression filter
	$reg_exUrl = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";
	// Check if there is a url in the text
	if(preg_match($reg_exUrl, $text, $url)) {
	       // make the urls hyper links
	       return preg_replace($reg_exUrl, "<a href=\"{$url[0]}\">{$url[0]}</a> ", $text);
	} else {
	       // if no urls in the text just return the text
	       return $text;
	}
}