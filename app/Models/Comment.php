<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
	public $with = ['user'];

    public function commentable()
    {
    	return $this->morphTo();
    }

    public function comments()
    {
    	return $this->morphMany('App\Models\Comment','commentable');
    }

    public function user()
    {
    	return $this->belongsTo('App\User');
    }
}
