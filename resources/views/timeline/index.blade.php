@extends('layouts.app',['page'=>'timeline',
	'props'=>[]
])
@section('content')
<section class="timeline">
    <div class="container">
        <div class="row">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        @include('timeline.includes.sidebar')
                    </div>
                    <div class="col-sm-12">
                        <div class="posts">
                        	@if(@Auth::check())
							<component is="new-post" inline-template :url="url">
								<div class="post create clearfix" >
									{{-- <textarea name="" class="form-control" rows="2" placeholder="Escreva alguma coisa" ></textarea> --}}
									<div class="textarea-wrapper">
										<textarea v-model="newPost.text" placeholder="Digite aqui..." name="" rows="1" class="autosize-textarea form-control""></textarea>
									</div>
									<div class="post-create-buttons">
										<label for="post-img">
											<i class="fa fa-image"></i>
											<input v-on:change="onAddPostImage($event)" type="file" name="" id="post-img">
										</label>
										<button v-bind:disabled="storingPost" class="btn btn-primary pull-right" v-on:click="storeNewPost">
											<span v-if="!storingPost">Publicar</span>
											<span v-cloak v-if="storingPost">Publicando...</span>
										</button>
									</div>
									<div class="new-post-tags" v-if="newPost.tags.length">
										<span class="tag" v-for="tag in newPost.tags">@{{tag}}</span>
									</div>
									<div class="img-wrapper" v-if="newPost.image">
										<button class="btn btn-danger btn-remove-image" v-on:click="newPost.image = ''">
											<i class="fa fa-remove"></i>
										</button>
										<img class="img-responsive" src="@{{newPost.image}}" alt="">
									</div>
								</div>
							</component>
							@endif
                        	<div class="post" v-for="post in posts">
	                        	<component is="post" inline-template :post="post" :url="url">
	                        		<div class="post-header">
		                        		<div class="img-wrapper">
		                        			<a href="">
		                        				<img src="@{{post.user.thumb}}" alt="" class="img-responsive">	
		                        			</a>
		                        		</div>
		                        		<div class="post-info">
		                        			<span class="name">
		                        				<a href="">@{{post.user.name+' '+post.user.surname}}</a>
		                        			</span>
		                        		</div>
		                        	</div>
		                        	<div class="post-body">
		                        		<div class="post-content clearfix">
		                        			<p>
		                        				@{{{replaceHashtags(post.content)}}}
		                        			</p>
		                        		</div>
		                        		<div class="post-image" v-if="post.images[0]">
		                        			<img src="@{{url+'/storage/uploads/posts/'+post.id+'/'+post.images[0].image}}" alt="">
		                        		</div>
		                        	</div>
		                        	<div class="post-actions">
		                        		<ul class="nav nav pills">
		                        			<li>
		                        				<button v-bind:disabled="processing_like" v-bind:class="{'liked':post.liked}" class="btn btn-sm btn-primary" v-on:click.prevent="likePost()">
		                        					<i class="fa fa-thumbs-up"></i>
		                        					<span v-if="!post.liked">Curtir</span>
		                        					<span v-if="post.liked">Descurtir</span>
		                        				</button>
		                        			</li>
		                        			<li><button v-on:click.prevent="focusCommentPost($event)" class="btn btn-sm btn-primary">
		                        				<i class="glyphicon glyphicon-comment"></i>
		                        				Comentar
		                        			</button></li>
		                        		</ul>
		                        	</div>
		                        	<div class="post-likes" v-if="post.count_likes">
		                        		<span v-on:click.prevent="" class="small text-info">
                        					<b>
                        						<span v-if="post.liked">Você</span>
	                        					<span v-if="post.count_likes > 1 && post.liked"> +</span>
												<span v-if="post.liked && post.count_likes > 1"> @{{post.count_likes - 1}}</span>
												<span v-if="!post.liked &&  post.count_likes >= 1"> @{{post.count_likes}}</span>
												<span v-if="(!post.liked && post.count_likes >= 1) || post.liked && post.count_likes >= 2 ">pessoa<span v-if="(post.liked && post.count_likes > 2) || !post.liked && post.count_likes >= 2">s</span> </span>
												<span v-if="post.count_likes > 1">curtiram </span>
												<span v-if="post.count_likes == 1">curtiu</span>
	                        					isso
                        					</b>
                        				</span>
		                        	</div>
		                        	<div class="post-comments">
		                        		<div class="comment" v-for="comment in comments">
		                        			<component is="comment" inline-template :comment="comment" :url="url" :post_id="post.id">
		                        				<div class="img-wrapper">
			                        				<a href="@{{url+'/u/'+comment.user.slug}}">
			                        					<img src="@{{comment.user.thumb}}" alt="" class="img-responsive">
			                        				</a>
			                        			</div>
			                        			<div class="comment-body">
			                        				<div class="comment-content">
			                        					<span class="username">
			                        						<a href="@{{url+'/u/'+comment.user.slug}}">
			                        							@{{comment.user.name+' '+comment.user.surname}}
			                        						</a>
			                        					</span>
			                        					<span class="comment-text">
			                        						@{{{comment.comment}}}
			                        					</span>
			                        				</div>
			                        				<div class="comments">
			                        					<div class="comment" v-for="subcomment in comments">
						                        			<div class="img-wrapper">
						                        				<a href="@{{url+'/u/'+subcomment.user.slug}}">
						                        					<img src="@{{subcomment.user.thumb}}" alt="" class="img-responsive">
						                        				</a>
						                        			</div>
						                        			<div class="comment-body">
						                        				<div class="comment-content">
						                        					<span class="username">
						                        						<a href="@{{url+'/u/'+subcomment.user.slug}}">
						                        							@{{subcomment.user.name+' '+subcomment.user.surname}}
						                        						</a>
						                        					</span>
						                        					<span class="comment-text">
						                        						@{{{subcomment.comment}}}
						                        					</span>
						                        				</div>
						                        				{{-- comentario --}}
						                        			</div>
						                        		</div>
						                        		@if(@Auth::check())
						                        		<component is="new-comment" inline-template :comment="comment" :post_id="post_id" :url="url">
						                        			<div>
							                        			<button v-show="showedCommentForms.indexOf(post_id+'_'+comment.id) == -1" class="btn btn-xs btn-link link-comment" v-on:click.prevent="toggleCommentForm(post_id+'_'+comment.id)">Comentar</button>
							                        		</div>
							                        		<div class="comment" v-show="showedCommentForms.indexOf(post_id+'_'+comment.id) > -1">
							                        			<div class="img-wrapper">
							                        				<a href="">
							                        					<img src="{{@Auth::user()->thumb}}" alt="" class="img-responsive">
							                        				</a>
							                        			</div>
							                        			<div class="comment-body">
							                        				<div class="comment-content">
							                        					<div class="overlay-posting" v-if="commentsBeingPosted.indexOf(post_id+'_'+comment.id) > -1">
							                        						<img src="{{asset('/img/typing.svg')}}" alt="">
							                        					</div>
							                        					<textarea v-model="new_comment.text" v-on:keypress="keyPressed($event)" name="" rows="1" class="autosize-textarea form-control""></textarea>
							                        					<button v-show="showedCommentForms.indexOf(post_id+'_'+comment.id) > -1" class="btn btn-xs btn-link link-comment pull-right" v-on:click.prevent="toggleCommentForm(post_id+'_'+comment.id)">Cancelar</button>
							                        				</div>
							                        				{{-- comentario --}}
							                        			</div>
							                        		</div>
						                        		</component>
						                        		@endif
			                        				</div>
			                        			</div>
		                        			</component>	
		                        		</div> {{-- end comment loop --}}
		                        		@if(@Auth::check())
		                        		<component v-ref:new-comment is="new-comment" inline-template :post_id="post.id" :post="post" :url="url">
		                        			<div class="comment">
			                        			<div class="img-wrapper">
			                        				<a href="">
			                        					<img src="{{@Auth::user()->thumb}}" alt="" class="img-responsive">
			                        				</a>
			                        			</div>
			                        			<div class="comment-body">
			                        				<div class="comment-content">
			                        					<div class="overlay-posting" v-if="commentsBeingPosted.indexOf(post_id+'_') > -1">
			                        						<img src="{{asset('/img/typing.svg')}}" alt="">
			                        					</div>
						                        		<textarea v-el:textareainput v-model="new_comment.text" v-on:keypress="keyPressed($event)" name="" rows="1" class="autosize-textarea form-control""></textarea>
			                        				</div>
			                        			</div>
			                        		</div>
		                        		</component>
		                        		@endif
		                        	</div>
	                        	</component>
	                        </div>  {{-- end loop --}}
	                        <div class="frame loading-div" v-show="loadingPosts">
	                        	<img src="{{asset('/img/loading.gif')}}" alt="">
	                        </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection