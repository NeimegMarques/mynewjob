module.exports = {
	init: function(){
		var th = this;
		var notifications = require('./_notifications');
		var body = document.getElementsByTagName('body')[0];
		var session_id = $('body').data('sid');
		
		//socket.emit('joinroom', 'room_'+body.dataset.uid);

		// socket.on('roomJoined', function(data){
		// 	socket.emit('get notifications', { key: session_id });
		// });
		socket.on('notifications', function(data){
			data.notifications.forEach(function(notif){
				notif.thumb = notif.thumb != ''
					? site_url+'/uploads/users/'+notif.sender_id+'/img/profile/thumb_'+notif.thumb
					: site_url+'/img/user-thumb-placeholder.jpg';
				notifications.appendNotification(notif);
			});
			notifications.updateCounter({sum:data.new.new,type:'notification'});
		});

		socket.on('message', function (data) {
			console.log(data);
			// var html = ''+
			// 	'<div class="image-wrapper">'+
			// 		'<img src="'+data.thumb+'">'+
			// 	'</div><div class="content">'+data.message+'</div>';

		 //    var notify = $.notify({
		 //    	message:html,
		 //    	allow_dismiss: true,
		 //    	url:data.link,
		 //    	target:'_self'
		 //    },{
		 //    	type:'notify-default',
		 //    	offset:{
		 //    		y:70,
		 //    		x:20
		 //    	},
		 //    	delay:5000,
		 //    	animate: {
			// 		enter: 'animated fadeInLeft',
			// 		exit: 'animated fadeOutRight'
			// 	},
			// 	template:'<div class="alert-{0} notify-alert">{2}</div>'
		 //    });

		 //    notifications.appendNotification(data);
		 //    notifications.updateCounter({sum:1,type:'notification'});
		});

		$('#dropdownMenuNotifications').on('click', function(e){
			var parent = $(this).closest('.dropdown');

			$('.notif-counter',this).addClass('hidden').text(0);

			if(!parent.hasClass('open')){
				socket.emit('set notification notified',{key:session_id});
			}
		});

		$(document).on('click', '.notif-item.not-seen a', function(e){
			var notif = $(this).closest('.notif-item');
			var nid = notif.data('nid');
			notif.removeClass('not-seen');
			socket.emit('set notification seen',{key:session_id, nid:nid});
		});
	}
}