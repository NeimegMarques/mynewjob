<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Vacancy;
use App\Models\Jobtype;

class VacanciesController extends Controller
{
    public function create($vacancy=NULL)
    {
        $jobtypes = \App\Models\Jobtype::all()->pluck('name','id');
        $sectors = \App\Models\Sector::all()->pluck('name','id');
        $experiences = \App\Models\Experiencelevel::all()->pluck('name','id');
        
        $companies = \Auth::user()->companies()->pluck('name','companies.id')->toArray();
        $companies = ['Selecione uma empresa']+$companies+['new'=>'Criar nova empresa'];

        return view('painel.vacancy.new')
            ->withJobtypes($jobtypes)
            ->withCompanies($companies)
            ->withSectors($sectors)
            ->withExperiences($experiences)
            ->withVacancy($vacancy);
    }

    public function show(Request $request, $id)
    {
        $vacancy = Vacancy::find($id);
        
        $jobtypes = \App\Models\Jobtype::all();
        $states = \App\Models\State::all();

        return view('painel.vacancy.show')
            ->withJobtypes($jobtypes)
            ->withStates($states)
            ->withVacancy($vacancy);
    }

    public function edit(Request $request, $id)
    {
        $vacancy = Vacancy::find($id);

        return $this->create($vacancy);

    }

    public function toggle(Request $request, $id)
    {
        $vacancy = \Auth::user()->vacancies()->find($id);
        if($vacancy)
        {
            $vacancy->active = !$vacancy->active;
            if($vacancy->save()){
                return response()->json(['active'=>$vacancy->active]);
            }
            return response()->json(['error'=>'Houve um erro ao desativar a vaga']);
        }
        return response()->json(['error'=>'A vaga não foi encontrada no banco de dados']);
    }

    public function giveUp(Request $request)
    {
        $candidacy = \App\Models\Candidacy::where('user_id',\Auth::user()->id)->where('vacancy_id',$request->vacancyid)->first();
        if($candidacy->delete()){
            return response()->json([]);
        }
    }

    public function update(Request $request,$id)
    {
        return $this->store($request, $id);
    }

    public function store(Request $request, $vacancy_id = NULL)
    {

        $messages = [
            'company_id.required'   => 'Selecione ou crie uma empresa.',
            'company_name.required' => 'Se você está criando uma empresa então o nome é obrigatório.',
            'company_description.required' => 'Se você está criando uma empresa então a descrição é obrigatória.',
            'vacancy_name.required' => 'Por favor informe o titulo da vaga.',
            'vacancy_name.max'      => 'O título da vaga é muito longo.',
            'city_id'               => 'Por favor informe a cidade.',
            'vacancy_tags.required' => 'Por favor informe as tags referentes à vaga.',
            'vacancy_description.required' => 'Por favor, descreva a vaga oferecida.',
            'vacancy_description.min' => 'A descrição da vaga é muito curta, dê mais detalhes.',
        ];

    	\DB::beginTransaction();

    	//COMPANY INFOS
    	$comp_id = $request->company_id;
        $comp_name = sanitizeContent($request->company_name);
    	$comp_slug = str_slug($comp_name);
    	$comp_sectors = $request->company_sector; // It's an array
    	$comp_logo = $request->company_logo;
    	$comp_description = sanitizeContent($request->company_description);
    	
        //VACANCY INFOS
    	$vacancy = $vacancy_id ? Vacancy::find($vacancy_id) : new Vacancy;

        if($vacancy_id && !$vacancy->isMine)
        {
            \DB::rollback();
            return response()->view('errors.404');
        }

    	$vacancy->name 			= sanitizeContent($request->vacancy_name);
        $vacancy->jobtype_id    = $request->vacancy_jobtype;
        $vacancy->description   = sanitizeContent($request->vacancy_description);
        $vacancy->redirect      = $request->vacancy_redirect == 'on' ? true : false;
        $vacancy->link          = sanitizeContent($request->vacancy_link);
        $vacancy->user_id       = \Auth::user()->id;
        $vacancy->city_id       = is_numeric($request->city_id) ? $request->city_id:null;
        $vacancy->salary_from   = sanitizeContent($request->vacancy_salary_from);
        $vacancy->salary_to     = sanitizeContent( $request->vacancy_salary_to < $request->vacancy_salary_from ? $request->vacancy_salary_from : $request->vacancy_salary_to);
        $vacancy->required_salary_pretension= $request->vacancy_required_salary == 'on' ? 1 : 0;
        $vacancy->quantity      = sanitizeContent($request->vacancy_quantity);
        $vacancy->active        = true;
    	
        $form_experiences_ids = $request->vacancy_experience ?:[];

    	// IF THERE'S NO COMPANY SELECTED, CREATE A NEW ONE
    	if(is_numeric($comp_id) && $comp_id > 0)
    	{
            $company = \Auth::user()->companies()->find($comp_id);
    	}
    	else
    	{
    		$company = new \App\Models\Company;
            $company->name = $comp_name;
    		$company->slug = $comp_slug;
    		$company->description = $comp_description;
            $company->save();
    		\Auth::user()->companies()->save($company);

            if($request->hasFile('company_logo'))
            {
                $image = $request->file('company_logo');
                
                if($image->isValid())
                {
                    $filename = 'logo.'.$image->getClientOriginalExtension();
                    $image = \Image::make($image);
                    
                    $image_w = $image->width();
                    $image_h = $image->height();

                    @\File::makeDirectory(storage_path('app/public/uploads'));
                    @\File::makeDirectory(storage_path('app/public/uploads/companies'));
                    @\File::makeDirectory(storage_path('app/public/uploads/companies/'.$company->id));
                    @\File::makeDirectory(storage_path('app/public/uploads/companies/'.$company->id).'/img');

                    $image->save(storage_path('app/public/uploads/companies/'.$company->id.'/img/'.$filename));
                    $thumb = $image->widen(200);
                    $image->save(storage_path('app/public/uploads/companies/'.$company->id.'/img/thumb_'.$filename));

                    $company->logo = $filename;
                    $company->save();
                }
            }

    		$company->sectors()->sync($comp_sectors);
    	}

    	$company->vacancies()->save($vacancy);
    	$vacancy->sectors()->sync($request->vacancy_sector);
        $tags   =   explode(',',$request->vacancy_tags);
        $desired_skills   =   explode(',',$request->vacancy_desired_skills);
        $benefits   =   explode(',',$request->vacancy_benefits);
        $validator = \Validator::make($request->all(), [
            'company_id'   => 'required',
            'company_name' => 'sometimes',
            'company_description' => 'sometimes',
            'vacancy_name' => 'required|max:64',
            'vacancy_tags' => 'required',
            'vacancy_description' => 'required|min:64'
        ],$messages);
        $validator->sometimes(['company_name','company_description'], 'required', function($input) {
            // SE company_id == new ENTÃO company_name E company_description É OBRIGATÓRIO
            return $input->company_id == 'new';
        });

        $tags_ids = [];
        foreach ($tags as $key => $tag) {
            if(is_numeric($tag))
            {
                $tags_ids[] = $tag;
            }
            elseif($tag != '')
            {
                $dbtag = \App\Models\Tag::where('name',$tag)->first();
                if( !@$dbtag ){
                    $new_tag = new \App\Models\Tag;
                    $new_tag->name = $tag;
                    $new_tag->tag = str_slug($tag);
                    $new_tag->save();
                    $tags_ids[] = $new_tag['id'];
                }
            }
        }

        $desired_skills_ids = [];
        foreach ($desired_skills as $key => $skill) {
            if(is_numeric($skill))
            {
                $desired_skills_ids[] = $skill;
            }
            elseif($skill != '')
            {
                $new_tag = new \App\Models\Tag;
                $new_tag->name = $skill;
                $new_tag->tag = str_slug($skill);
                $new_tag->save();
                $desired_skills_ids[] = $new_tag['id'];
            }
        }

        $benefits_ids = [];
        foreach ($benefits as $key => $benefit) {
            if(is_numeric($benefit))
            {
                $benefits_ids[] = $benefit;
            }
            elseif($benefit != '')
            {
                $db_benefit = \App\Models\Benefit::where('name', $benefit)->first();
                if(!@$db_benefit){
                    $new_benefit = new \App\Models\Benefit;
                    $new_benefit->name = $benefit;
                    $new_benefit->save();
                    $benefits_ids[] = $new_benefit['id'];
                }
            }
        }

        @$vacancy->experiences()->sync(@$form_experiences_ids);
        $vacancy->tags()->sync(@$tags_ids);
        $vacancy->desiredskills()->sync(@$desired_skills_ids);
        $vacancy->benefits()->sync(@$benefits_ids);


        if ($validator->fails()) {
            \DB::rollback();
            return back()
                ->withErrors($validator)
                ->withInput();
        }

    	\DB::commit();

        if( @!\Auth::user()->recruiter ){
            \Auth::user()->recruiter = 1;
            \Auth::user()->save();
        }

    	return redirect('v/'.$vacancy->id);
    }

    public function newApply(Request $request)
    {
        \DB::beginTransaction();
        $vacancy_id = $request->vacancyid;
        $vacancy = Vacancy::find($vacancy_id);

        $candidacy = new \App\Models\Candidacy;
        $candidacy->user_id = \Auth::user()->id;
        $candidacy->vacancy_id = $vacancy->id;
        $candidacy->salary = $request->salary ? preg_replace('/[^0-9]+/', '', $request->salary):0;
        $candidacy->message = $request->message?:'';

        if($candidacy->save())
        {
            $admins = $vacancy->company->users->pluck('id')->toArray();
            $total = $vacancy->candidacies->count()-1;
            $message = '';

            if(!$total>0)
            {
                $message .= ' candidatou-se ';
            }
            elseif($total == 1)
            {
                $message .= ' e mais uma pessoa candidataram-se ';
            }
            else
            {
                $message .= ' e mais '.$total.' pessoas candidataram-se ';
            }

            $message .= 'à vaga <b>'.$vacancy->name.'</b>';

            $notifications = [];
            $date = date('Y-m-d H-i-s');
            $user_id = \Auth::user()->id;

            foreach ($admins as $admin) {
                // $data = [
                //     'message'=>$message,
                //     'user'=>$user_id,
                //     'room'=>'room_'.$admin,
                //     'link'=>route('mostrar-vaga',['id'=>$vacancy->id]),
                //     'thumb'=>\Auth::user()->thumb
                // ];
                // $redis->publish('message', json_encode($data));

                $notifications[] = [
                    'sender_id'=>$user_id,
                    'receiver_id'=>$admin,
                    'type'=>'notification',
                    'message'=>$message,
                    'link'=>route('mostrar-vaga',['id'=>$vacancy->id]),
                    'created_at'=>$date,
                    'updated_at'=>$date
                ];
            }
            \App\Models\Notification::insert(@$notifications);
            $view = view('painel.vacancy.includes._vacancy')->withVacancy($vacancy)->render();
            //if(env('APP_ENV') != 'local') {
                \Mail::to($vacancy->user->email)->send( new \App\Mail\NewApply(\Auth::user(),$vacancy));
            //}
            \DB::commit();

            if( @!\Auth::user()->candidate ){
                \Auth::user()->candidate = 1;
                \Auth::user()->save();
            }

            return response()->json(['view'=>$view,'message'=>'Obrigado por candidatar-se à esta vaga. Boa sorte!']);
        }
        else
        {
            \DB::rollback();
            return response()->json(['errorMessage'=>'Ooops! Não foi possível registrar sua candidatura à esta vaga no momento.'],500);
        }
    }
    public function exportcvs($vacancy_id, $slug = NULL)
    {
        $vacancy = \App\Models\Vacancy::where('id',$vacancy_id)->where('user_id',\Auth::user()->id)->first();
        
        $candidacies = $vacancy->candidacies();
        
        if($slug  != ''){
            $user = \App\User::where('slug', $slug)->first();
            $candidacies->where('user_id',$user->id);
        }

        $candidacies = $candidacies->get();

        if(@$user && !in_array($user->id, $candidacies->pluck('candidate.id')->toArray()))
        {
            return redirect('painel');
        }

        //return view('pdf.cv');
        $users = [];
        foreach ($candidacies as $key => $c) {
            $users[] = $c;
        }
        $pdf = \PDF::setPaper('A4')->loadView('pdf.cv', ['users'=> $users]);
        return $pdf->download(@$user?$user->name.' '.$user->surname.'.pdf' : 'candidatos.pdf');
        //dd($request->input());
    }
}
