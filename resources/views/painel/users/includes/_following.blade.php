<div class="frame" recompile="true"  v-show="navigation.current_page == 'following' && !loading_page">
    <div class="resume-group">
        <div class="row">
            <div class="container-fluid">
                <h4 class="resume-group-title">
                    <i class="icon-resume fa fa-arrow-up"></i>
                    Seguindo {{@$user->follows->count()}}
                </h4>
            </div>
        </div>
        <div class="row">
            <div class="container-fluid">
                @if(@$user->follows->count())
                    @forelse($user->follows as $i => $user)
                        @if($i == 0 || $i%2 == 0 )
                            @if($i > 0)
                                </div>
                            @endif
                            <div class="row row-eq-height">
                        @endif
                        <div class="col-sm-12 mb20">
                            @include('painel.users.includes._card')
                        </div>
                    @empty
                        <p class="well well-sm text-primary">Nenhum resultado encontrado</p>
                    @endforelse
                @endif
            </div>
        </div>
    </div>
</div>