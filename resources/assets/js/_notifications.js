module.exports = {
	appendNotification: function(data){
		//console.log(data);
		var html = ''+
		'<li class="clearfix notif-item '+(data.seen?'':'not-seen')+'" data-nid="'+data.id+'">'+
		    '<div class="notif-img-wrapper">'+
                '<img src="'+data.thumb+'" class="img-responsive">'+
            '</div>'+
            '<div class="notif-content">'+
                '<div class="notif-title">'+data.message+'</div>'+
            '</div>'+
            '<a href="'+data.link+'"></a>'+
        '</li>';
		$('#dropdown-menu-notifications').find('.notifs-container').prepend(html);
	},
	updateCounter: function(data){
		if(data.type == 'message')
		{
			var counter = $('.notif-counter',$('#dropdownMenuMessages'));
		}
		else if(data.type == 'notification')
		{
			var counter = $('.notif-counter',$('#dropdownMenuNotifications'));
		}

		var val = parseInt(counter.text());

		var nval = isNaN(val) ? data.sum : data.sum + val;

		counter.text(nval);
		if(nval > 0){
			counter.removeClass('hidden');
		}
	}
}