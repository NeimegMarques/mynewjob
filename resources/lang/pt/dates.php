<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'week'  =>  [
        'Domingo',
        'Segunda-feira',
        'Terça-feira',
        'Quarta-feira',
        'Quinta-feira',
        'Sexta-feira',
        'Sábado'
    ],

    'months' =>  [
        1       =>  "Janeiro",
        2       =>  "Fevereiro",
        3       =>  "Março",
        4       =>  "Abril",
        5       =>  "Maio",
        6       =>  "Junho",
        7       =>  "Julho",
        8       =>  "Agosto",
        9       =>  "Setembro",
        10      =>  "Outubro",
        11      =>  "Novembro",
        12      =>  "Dezembro"
    ]

];
