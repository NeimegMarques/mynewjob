const elixir = require('laravel-elixir');

require('laravel-elixir-vue');
//require('laravel-elixir-vue-2');
require('laravel-elixir-livereload');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(mix => {
    mix
    	.sass('app.scss')
		.styles([
			"../../bower_components/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css",
		    "public/css/app.css"
		],'public/css/styles.css' ,'public/css')
	   .webpack(['main.js'],'public/js/script.js')
	   .webpack(['front.js'],'public/js/front.js')
	   //.browserify(['main.js'],'public/js/script.js');
	   .livereload();
});
