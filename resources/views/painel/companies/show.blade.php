@extends('layouts.app',['page'=>'company-view',
	'props'=>[
		'companyid'=>$company->id,
		//'vacancies' => in_array(Auth::user()->id,$company->vacancies()->pluck('user_id')->toArray())
	]
])

@section('content')
<section class="bg-primary top-background-bar"></section>
<div>
	<div class="container">
        <div class="row">
            <div class="col-sm-20 col-sm-offset-2">
                <div class="">
                    <div class="company frame" companyId="{{@$company->id}}">
                        <div class="company-head">
                            <div class="col-sm-6">
                                <div class="company-img-wrapper">
                                    <a href="#">
                                        @if($company->logo)
                                        <img class="img-responsive" src="{{$company->logo_path}}" alt="...">
                                        @endif
                                    </a>
                                </div>
                            </div>
                            <div class="col-sm-18">
                                <h3 class="vacancy-title">
                                    {{$company->name}}
                                </h3>
                                <p class="vacancy-city">
                                    <i class="fa fa-map-marker"></i>
                                    {{@$company->city->name.' - '.@$company->city->state->code}}
                                </p>
                            </div>
                        </div>
                        <hr>
                        <div class="vacancy-body clearfix">
                            <div class="col-sm-6">
                                <div class="">
                                    <p><b>
                                        @if(count(@$company->sectors))
                                            @foreach(@$company->sectors as $sector)
                                                <span class="text-muted small">{{$sector->name}}</span><br />
                                            @endforeach
                                        @endif
                                    </b></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
            </div>
        </div>  
    </div>
</div>
@endsection
