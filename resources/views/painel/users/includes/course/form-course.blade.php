
{{Form::open(['url'=>url('/user/resume/course/'.@$course->id)])}}
@if(@$course)
	{{Form::hidden('_method','PUT')}}
@endif
<div class="form-group">
	<label for="" class="control-label">
		Curso
	</label>
	{{Form::text('course_name',@$course->title,['class'=>'form-control input-sm','tabindex'=>'1'])}}
</div>
<div class="form-group">
	<label for="" class="control-label">
		Entidade
	</label>
	{{Form::text('entity_name',@$course->entity_name,['class'=>'form-control input-sm','tabindex'=>'1'])}}
</div>
<div class="form-group">
	<label for="" class="control-label">
		Cidade
	</label>
	<p class="clearfix">
		{{Form::text('course_city',@$course->city ? @$course->city->name.' - '.@$course->city->state->code:'',['class'=>'form-control input-sm city-typeahead','tabindex'=>'1'])}}
	</p>
</div>
<div class="form-group">
	<label for="" class="control-label">
		Período
	</label>
	<div class="row">
		<div class="time-range">
			<div class="container-fluid">
				<div class="time-from">
					<div class="month-from">
						{{Form::select('course_month_from',months(),date('m',strtotime(@$course->start)),['class'=>'form-control input-sm','tabindex'=>'1'])}}
					</div>
					<div class="year-from">
						{{Form::select('course_year_from',years(),date('Y',strtotime(@$course->start)),['class'=>'form-control input-sm','tabindex'=>'1'])}}
					</div>
				</div>
				<div class="range-until">
					até
				</div>
				<div class="time-to">
					<div class="time-to-container @if(!@$course->end) hidden @endif">
						<div class="month-to">
							{{Form::select('course_month_to',months(),date('m',strtotime(@$course->end)),['class'=>'form-control input-sm','tabindex'=>'1'])}}
						</div>
						<div class="year-to">
							{{Form::select('course_year_to',years(),date('Y',strtotime(@$course->end)),['class'=>'form-control input-sm','tabindex'=>'1'])}}
						</div>
					</div>

					<div class="until-now-text @if(@$course->end) hidden @endif">agora</div>

					<label for="{{@$course?@$course->id.'-':''}}course-until-now-select" class="until-now-label">
						<input @if(!@$course->end) checked @endif tabindex="1" class="styled-checkbox until-now-checkbox" id="{{@$course?@$course->id.'-':''}}course-until-now-select" type="checkbox" name="until_now">
						Ainda estou cursando
					</label>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="form-group">
	<label for="" class="control-label">
		Descrição
	</label>
	{{Form::textarea('course_description',@$course->description,['rows'=>'4','class'=>'form-control input-sm','tabindex'=>'1'])}}
</div>
<hr>
@include('painel.users.includes.resume-item-form-buttons',['item'=>@$course])
@if(old('city_id') || @$course)
    {{Form::hidden('city_id',old('city_id')?:$course->city_id)}}
@endif
{{Form::close()}}