@extends('layouts.app',['page'=>'vacancy-view',
	'props'=>[
		'vacancyid'=>$vacancy->id,
        'applied' => in_array(@Auth::user()->id,$vacancy->candidacies()->pluck('user_id')->toArray()),
        ':redirect' => $vacancy->redirect ? 'true' : 'false',
		':active' => $vacancy->active ? 'true':'false',
        ':salary_pretension_required' => $vacancy->required_salary_pretension ? "true":"false"
	]
])

@section('head_tags')
    <?php 

    $title = 'Vaga '.$vacancy->name.' ';
    $count = $vacancy->experiences->count();
    foreach ($vacancy->experiences as $key => $exp) {
        $title .= $exp->name.($key+2 == $count ? ' ou ':', ');
    }
    $title = rtrim($title, ', ');
    $title .= ($vacancy->city? ' em '.@$vacancy->city->name : '').' - Salário:'.$vacancy->salary_description;
    $description = strip_tags($vacancy->description);
    $keywords = 'Emprego, Oportunidade, Vaga, ';
    if(count($vacancy->tags))
    {
        foreach($vacancy->tags as $tag)
        {
            $keywords .= $tag->name.', ';
        }
    }
    $url = \Request::fullUrl();
    $type = "website";
    $image = asset('/img/logo-icon.png');
    ?>
    @include("includes.head_tags")               
@stop

@section('content')
<section class="bg-primary top-background-bar"></section>
<div>
	<div class="container">
        <div class="row">
            <div class="col-sm-20 col-sm-offset-2">
                <div class="">
                    @include('painel.vacancy.includes._vacancy')
                </div> 
            </div>
        </div>  
    </div>
</div>
@endsection
