@extends('layouts.front')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-md-offset-6">
                <div class="row">
                    <div class="col-sm-12">
                        <button class="btn btn-xlg btn-block btn-facebook">
                            <i class="fa fa-facebook"></i>
                            Facebook
                        </button>
                    </div>
                    <div class="col-sm-12">
                        <button class="btn btn-xlg btn-block btn-google-plus">
                            <i class="fa fa-google-plus"></i>
                            Google
                        </button>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-md-offset-6">
                <hr>
                {{Form::open(['url'=>url('register'),'class'=>'form-horizontal', 'role'=>'form'])}}
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <div class="col-md-24">
                            <input type="text" class="form-control input-lg" placeholder="Nome" name="name" value="{{ old('name') }}">

                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <div class="col-md-24">
                            <input type="email" class="form-control input-lg" placeholder="Email" name="email" value="{{ old('email') }}">

                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <div class="col-md-24">
                            <input type="password" class="form-control input-lg" placeholder="Senha" name="password">

                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                        <div class="col-md-24">
                            <input type="password" class="form-control input-lg" placeholder="Confirme senha" name="password_confirmation">

                            @if ($errors->has('password_confirmation'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-24">
                            <button type="submit" class="btn btn-danger btn-lg pull-right">
                                Prosseguir 
                                <i class="fa fa-btn fa-chevron-right"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection
