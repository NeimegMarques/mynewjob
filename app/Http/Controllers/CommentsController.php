<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CommentsController extends Controller
{
    public function store(Request $request, $post_id)
    {
    	$post = \App\Models\Post::find($post_id);
    	$append_to = $post;
        
        if($request->comment_id > 0){
            $comment = $post->comments()->where('id',$request->comment_id)->first();
            $append_to = $comment;
        }

    	$new_comment = new \App\Models\Comment;
    	$new_comment->comment = sanitizeContent($request->comment['text']);
    	$new_comment->user_id = \Auth::user()->id;

    	$append_to->comments()->save($new_comment);

        if(\Auth::user()->id != $append_to->user_id){
            $notification = [
                'sender_id'=>\Auth::user()->id,
                'receiver_id'=>$append_to->user_id,
                'type'=>'notification',
                'message'=> $request->comment_id > 0 ? 'respondeu o seu comentário':'comentou a sua publicação',
                'link'=>url('/timeline'),
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s')
            ];
            \App\Models\Notification::insert(@$notification);
        }

    	return response()->json(['comment' => $new_comment->load('user')]);
    }
}
