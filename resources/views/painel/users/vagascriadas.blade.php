@extends('layouts.app',['page'=>'vagas-criadas-view',
    'props'=>[
        'user'=>$user
    ]
])

@section('content')
<section class="user-content @if(@$user->is_me) me @endif">
    <div class="container">
        <div class="row">
            <div class="col-sm-14 col-sm-offset-5">
                <div class="frame">
                    <h4 class="title">Vagas criadas por mim</h4>
                    @foreach($user->vacancies as $i => $vacancy)
                        @include('painel.vacancy.includes._vacancy_summary',['vacancy' => $vacancy])
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
@if(@$user->is_me)
</component>
@endif
@endsection