<?php

use Illuminate\Database\Seeder;

use \App\Models\Country;

class StatesTableSeeder extends Seeder {
	
	public function run() 
	{
		DB::table('states')->delete();

		$brasil = Country::where('code','BR')->first();

		$cities = array(
			array('id'=>1,'country_id'=>$brasil->id,'code'=>'AC','name'=>'ACRE'),
			array('id'=>2,'country_id'=>$brasil->id,'code'=>'AL','name'=>'ALAGOAS'),
			array('id'=>3,'country_id'=>$brasil->id,'code'=>'AP','name'=>'AMAPÁ'),
			array('id'=>4,'country_id'=>$brasil->id,'code'=>'AM','name'=>'AMAZONAS'),
			array('id'=>5,'country_id'=>$brasil->id,'code'=>'BA','name'=>'BAHIA'),
			array('id'=>6,'country_id'=>$brasil->id,'code'=>'CE','name'=>'CEARÁ'),
			array('id'=>7,'country_id'=>$brasil->id,'code'=>'DF','name'=>'DISTRITO FEDERAL'),
			array('id'=>8,'country_id'=>$brasil->id,'code'=>'ES','name'=>'ESPÍRITO SANTO'),
			array('id'=>9,'country_id'=>$brasil->id,'code'=>'RR','name'=>'RORAIMA'),
			array('id'=>10,'country_id'=>$brasil->id,'code'=>'GO','name'=>'GOIÁS'),
			array('id'=>11,'country_id'=>$brasil->id,'code'=>'MA','name'=>'MARANHÃO'),
			array('id'=>12,'country_id'=>$brasil->id,'code'=>'MT','name'=>'MATO GROSSO'),
			array('id'=>13,'country_id'=>$brasil->id,'code'=>'MS','name'=>'MATO GROSSO DO SUL'),
			array('id'=>14,'country_id'=>$brasil->id,'code'=>'MG','name'=>'MINAS GERAIS'),
			array('id'=>15,'country_id'=>$brasil->id,'code'=>'PA','name'=>'PARÁ'),
			array('id'=>16,'country_id'=>$brasil->id,'code'=>'PB','name'=>'PARAÍBA'),
			array('id'=>17,'country_id'=>$brasil->id,'code'=>'PR','name'=>'PARANÁ'),
			array('id'=>18,'country_id'=>$brasil->id,'code'=>'PE','name'=>'PERNAMBUCO'),
			array('id'=>19,'country_id'=>$brasil->id,'code'=>'PI','name'=>'PIAUÍ'),
			array('id'=>20,'country_id'=>$brasil->id,'code'=>'RJ','name'=>'RIO DE JANEIRO'),
			array('id'=>21,'country_id'=>$brasil->id,'code'=>'RN','name'=>'RIO GRANDE DO NORTE'),
			array('id'=>22,'country_id'=>$brasil->id,'code'=>'RS','name'=>'RIO GRANDE DO SUL'),
			array('id'=>23,'country_id'=>$brasil->id,'code'=>'RO','name'=>'RONDÔNIA'),
			array('id'=>24,'country_id'=>$brasil->id,'code'=>'TO','name'=>'TOCANTINS'),
			array('id'=>25,'country_id'=>$brasil->id,'code'=>'SC','name'=>'SANTA CATARINA'),
			array('id'=>26,'country_id'=>$brasil->id,'code'=>'SP','name'=>'SÃO PAULO'),
			array('id'=>27,'country_id'=>$brasil->id,'code'=>'SE','name'=>'SERGIPE'),
		);

		DB::table('states')->insert($cities);
	}
}
