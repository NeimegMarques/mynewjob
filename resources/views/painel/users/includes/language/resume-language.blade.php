<div class="resume-item">
    @include('painel.users.includes.resume-item-options')
    <div class="resume-item-content">
        <h5 class="title">{{languages($language->title)}} - <span class="text-info">{{language_levels($language->level)}}</span></h5>
    </div>
    @if(@\Auth::user()->is_me)
        <div class="resume-item-form-content hidden">
            @include('painel.users.includes.language.form-language')
        </div>
    @endif
</div>