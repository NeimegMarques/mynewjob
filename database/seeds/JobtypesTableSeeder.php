<?php

use Illuminate\Database\Seeder;

class JobtypesTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('jobtypes')->delete();

		$jobtypes = array(
			array('name'=>'Autônomo','slug'=>'autonomo'),
			array('name'=>'Efetivo(CLT)','slug'=>'efetivo'),
			array('name'=>'Estágio','slug'=>'estagio'),
			array('name'=>'Prestação de serviços(PJ)','slug'=>'prestacao-de-servicos'),
			array('name'=>'Freelance','slug'=>'freelance'),
			array('name'=>'Temporário','slug'=>'temporario'),
			array('name'=>'Outros','slug'=>'outros')
		);

		DB::table('jobtypes')->insert($jobtypes);
    }
}
