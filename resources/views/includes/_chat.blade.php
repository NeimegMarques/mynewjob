<chat inline-template :chats="chats" :showchat.sync="showchat" :url="url">
    <div class="chat" v-show="showchat" v-cloak>
        <div class="chat-overlay" v-on:click="showchat=false"></div>
        <div class="chat-inner">
            <div class="chats-list-wrapper">
                <div class="chat-search">
                    <input type="text" class="form-control" placeholder="Buscar">
                    <a class="closeChat visible-xs" href="#" v-on:click.prevent="showchat = false">
                        <i class="fa fa-close"></i>
                    </a>
                </div>
                <div class="chats-list-holder">
                    <div class="chat-list-container">
                        <ul class="chat-list">
                            <li class="chat-item" v-for="chat in chats" v-bind:class="{'active':currentWindow == 'u_'+chat.uid}">
                                <a href="#" 
                                v-on:click.prevent="loadMessages(chat)"></a>
                                <img src="@{{chat.imagePath}}" class="img-responsive">
                                <div class="chat-item-info">
                                    <p class="name">@{{chat.name+' '+chat.surname}}
                                        <span v-if="chat.unread > 0 && !chat.typing" class="pull-right badge">@{{chat.unread}}</span>
                                        <img v-if="chat.typing && currentWindow != 'u_'+chat.uid" class="pull-right" src="{{asset('img/typing.svg')}}" alt="Digitando..." class="img-responsive">
                                    </p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="chat-content" v-show="currentWindow != ''">
                <div class="text-center loadingMessagesOverlay" v-if="loadingMessages">
                    <h4 class="text-primary">Carregando...</h4>
                </div>
                <div v-for="(index, window) in chatWindows" v-show="currentWindow == index" class="@{{index}}">
                    <div class="user-info">
                        <div class="window-close visible-xs" v-on:click="currentWindow = ''">
                            <i class="fa fa-arrow-left"></i>
                        </div>
                        <img src="@{{window.imagePath}}" class="img-responsive">
                        <div class="user-details">
                            <span class="name">@{{window.name}}</span>
                        </div>
                        <div class="window-options"></div>
                    </div>
                    <div class="messages-list-wrapper">
                        <div class="messages-list clearfix">
                            <div class="messages-list-inner">
                                <div 
                                    v-for="(index, message) in window.data.messages"
                                    v-bind:class="{
                                                    'partner' : message.receiver_id == {{Auth::user()->id}} , 
                                                    'me' : message.sender_id == {{Auth::user()->id}},
                                                    'samesender': index > 0 && window.data.messages[index-1].sender_id == message.sender_id,
                                                    'nextissame': window.data.messages[index+1] != undefined && window.data.messages[index+1].sender_id == message.sender_id
                                                     }"
                                    class="message-item">
                                    {{-- <div class="message-info" v-if="index == 0 || window.data.messages[index-1].sender_id != message.sender_id">
                                        <span v-if="message.sender_id == {{Auth::user()->id}}" class="name">{{Auth::user()->name}}</span>
                                        <span v-else class="name">@{{window.name}}</span>
                                        <span class="time">10:12, Hoje </span>
                                    </div> --}}
                                    <div class="message-content">
                                        @{{{message.message}}}
                                    </div>
                                </div>
                                <div 
                                    v-if="window.typing"
                                    v-bind:class=""
                                    class="message-item partner typing">
                                    <div class="message-content">
                                        <img src="{{asset('img/typing.svg')}}" alt="Digitando..." class="img-responsive">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="compose-message-wrapper">
                        <div class="compose-message">
                            <div class="textarea-wrapper">
                                <textarea v-on:focus="setRead(index)" v-on:keyup="IAmTyping(window, index, $event)" rows="1" v-model="window.composeMessage" class="form-control" v-on:keyup.enter.prevent="sendMessage(index)"></textarea>
                                <button type="button" v-touch:tap="sendMessage(index)" class="btn-primary btn btn-send-message visible-xs">
                                    <i class="fa fa-send"></i>
                                </button>
                            </div>
                            {{-- <div class="compose-options">
                                <div class="row">
                                    <div class="col-xs-12 pull-right">
                                        <button class="btn btn-link btn-xs pull-right">ENVIAR</button>
                                    </div>
                                    <div class="col-xs-12">
                                        <button class="btn btn-xs btn-default">
                                            <i class="glyphicon glyphicon-picture"></i>
                                        </button>
                                        <button class="btn btn-xs btn-default">
                                            <i class="glyphicon glyphicon-paperclip"></i>
                                        </button>
                                    </div>
                                </div>
                            </div> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</chat>