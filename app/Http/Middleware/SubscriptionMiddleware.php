<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class SubscriptionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        // if(Auth::check()){
        //     if ((
        //             @!Auth::user()->free && 
        //             @!Auth::user()->subscribed('gratis') && 
        //             @!Auth::user()->subscribed('pro') && 
        //             @!Auth::user()->subscribed('vip')
        //         ))
        //     {
        //         return redirect('contratar');
        //     }
        // }

        return $next($request);
    }
}
