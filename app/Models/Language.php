<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    public function candidate()
    {
        return $this->belongsTo('App\Models\Candidate', 'user_id');
    }
}
