<?php $user = $candidacy->candidate;?>
<div class="user-card small clearfix">
    <div class="user-image">
        <a style="position: relative;" href="{{url('/u/'.$user->slug)}}">
            <img src="{{@$user->thumb}}" alt="" class="img-responsive">
        </a>
    </div>
    <div class="user-content">
        <div class="row">
            <div class="col-sm-18">
                 <div class="user-name">
                    <a style="position: relative;" href="{{url('/u/'.$user->slug)}}">{{$user->name.' '.$user->surname}}</a>
                 </div>
                @if($user->profession)
                    <div class="user-info">{{@$user->profession}}</div>
                @endif
                @if($user->city)
                    <div class="user-info"><i class="fa fa-map-marker text-info"></i> {{@$user->city->name}}</div>
                @endif
                <div>
                    <br>
                    {{strip_tags($candidacy->message)}}
                </div>
            </div>
            <div class="col-sm-6">
                @if($vacancy->user_id == Auth::user()->id)
                    <button class="btn btn-default btn-xs pull-right" v-on:click="exportcv('{{$user->slug}}')">
                        <span class="fa fa-file-pdf-o"></span> Exportar CV
                    </button>
                @endif
            </div>
        </div>
    </div>
</div>