<div class="vacancy vacancy--summary clearfix">
    <div class="row">
        <div class="vacancy-head">
            <div class="vacancy-options">
                <button class="btn btn-default dropdown-toggle" type="button" id="vacancy-{{@$vacancy->id}}-option-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                    <i class="fa fa-chevron-down"></i>
                </button>
                <ul class="dropdown-menu pull-right" aria-labelledby="vacancy-{{@$vacancy->id}}-option-dropdown">
                    @if($vacancy->is_mine)
                    <li><a href="{{route('editar-vaga',$vacancy->id)}}">Editar</a></li>
                    <li><a href="">Excluir</a></li>
                    @else
                    <li><a href="#">Denunciar</a></li>
                    <li><a href="#">Sugerir correção</a></li>
                    @endif
                </ul>
            </div>
            <div class="container-fluid">
                <div class="vacancy-infos">
                    <h5 class="vacancy-title">
                        <a target="_blank" href="{{url('v/'.@$vacancy->id)}}">{{@$vacancy->name}}</a>
                    </h5>
                    @if($vacancy->link)
                        <div class="small text-success">
                            {{ parse_url($vacancy->link, PHP_URL_HOST) }}
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-sm-12">
                            <h5 class="vacancy-company">
                                {{@$vacancy->company->name}}
                            </h5>
                            @if(@$vacancy->city)
                            <p class="vacancy-city">
                                <i class="fa fa-map-marker"></i>
                                {{@$vacancy->city->name.' - '.@$vacancy->city->state->code}} 
                                - {{@$vacancy->quantity > 1 ? @$vacancy->quantity.' vagas' : '1 vaga'}}
                            </p>
                            @endif
                            @if(@$vacancy->salary_description)
                                <p class="vacancy-salary small">
                                    Salário previsto:
                                    <strong class="text-success">{{$vacancy->salary_description}}</strong>
                                </p>
                            @endif
                        </div>
                        <div class="col-sm-12">
                            <div class="vacancy-tags pull-right">
                                @if(@$vacancy->tags)
                                    @forelse(@$vacancy->tags as $tag)
                                    <div class="label label-primary">{{$tag->name}}</div>
                                    @empty
                                    @endforelse
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
