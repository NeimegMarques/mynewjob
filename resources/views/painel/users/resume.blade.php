@extends('layouts.app',['page'=>'resume-view',
    'props'=>[
        ':user'=>$user,
        ':skills' => @$user->skills
        /*
        ':normal_skills' => @$user->normal_skills,
        ':recommended_skills' => @$user->recommended_skills->toJson()
        */
    ]
])

@section('head_tags')
    <?php 
    $title = $user->name.' '.$user->surname.' - '.@$user->occupation->title .' '.@$user->occupation->article.' '.(@$user->occupation->company_name != '' ? @$user->occupation->company_name : @$user->occupation->entity_name);
    $description = strip_tags($user->about);
    $keywords = '';
    if(count($user->skills))
    {
        foreach($user->skills as $skill)
        {
            $keywords .= $skill->tag->name.', ';
        }
    }
    $url = \Request::fullUrl();
    $type = "website";
    $image = $user->imagePath;
    ?>
    @include("includes.head_tags")               
@stop

@section('content')
<section class="bg-primary top-background-bar"></section>
@if(@$user->is_me)
{{-- <component is="edit-user-view" inline-template :url="url" :skills="skills"> --}}
@endif
<a id="inicio"></a>
{{-- @include('painel.users.includes.header') --}}
<section class="user-content @if(@$user->is_me) me @endif">
    <div class="container" v-cloak>
        <div class="row">
            <div class="col-sm-17">
                <div class="row">
                    <div class="container-fluid">
                        <div class="frame half user-header">
                            <div class="row">
                                <div class="col-sm-7">
                                    <user-image></user-image>
                                </div>
                                <div class="col-sm-17">
                                    <h2 class="name-wrapper">
                                        <span
                                            class="user-name" 
                                            data-type="text" 
                                            data-name="name" 
                                            data-placeholder="Seu nome" 
                                            data-emptytext="Vazio">{{$user->name}}</span>
                                        <span 
                                            class="user-surname" 
                                            data-type="text" 
                                            data-name="surname"
                                            data-placeholder="Seu sobrenome" 
                                            data-emptytext="Vazio">{{$user->surname}}</span>
                                    </h2>
                                    <div>
                                        @if($user->occupation)
                                        <p>
                                            {{@$user->occupation->title .' '.@$user->occupation->article.' '.(@$user->occupation->company_name != '' ? @$user->occupation->company_name : @$user->occupation->entity_name)}}
                                        </p>
                                        @endif
                                    </div>
                                    <div>
                                        <div class="detail-item">
                                            <i class="fa fa-map-marker"></i>
                                            <div class="detail-container"> 
                                                <span class="city-container" id="x-city-editable">
                                                {{@$user->city->name.' - '.@$user->city->state->code}}</span>
                                            </div>
                                        </div>
                                        @if(@Auth::check())
                                        <div class="detail-item">
                                            <i class="fa fa-phone"></i>
                                            <div class="detail-container"> 
                                                @if(Auth::user()->recruiter || $user->is_me)
                                                <span 
                                                    class="phones-container" 
                                                    data-name="phones"
                                                    data-placeholder='Use / para separar os telefones' 
                                                    data-title='Separe os telefones usando /'
                                                    data-emptytext='Informe seu telefone, use / para separar'>@foreach($user->phones as $i=>$ph)@if($i>0)&#13;<b>/</b>&#13;@endif{{strip_tags($ph->phone)}}@endforeach</span>
                                                @else
                                                <span class="phones-container text-muted">
                                                    Somente recrutadores podem ver o telefone para contato
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="detail-item">
                                            <i class="fa fa-envelope"></i>
                                            <div class="detail-container"> 
                                                <span 
                                                class="emails-container" 
                                                data-name="emails"
                                                data-placeholder="Escreva um email por linha" 
                                                data-emptytext="Informe seu email">
                                                    @foreach($user->emails as $i=>$email)
                                                        @if($i>0)
                                                            &#13;/&#13;
                                                        @endif
                                                        {{strip_tags($email->email)}}
                                                    @endforeach
                                                </span>
                                            </div>
                                        </div>
                                        @endif
                                    </div>
                                    <hr>
                                    <div>
                                        <div class="user-tab-menu">
                                            <button v-on:click="setCurrentPage('resume')" class="btn btn-lg btn-link" v-bind:class="{'selected': navigation.current_page == 'resume'}">
                                                Currículo
                                            </button>
                                            <?php 
                                            $seguidores = count($user->followers);
                                            ?>
                                            {{-- @if($seguidores > 0) --}}
                                            <button v-on:click="setCurrentPage('followers')" class="btn btn-lg btn-link" :class="{'selected': navigation.current_page == 'followers'}">
                                                Seguidores
                                                <span>{{$seguidores}}</span>
                                            </button>
                                            {{-- @endif --}}
                                            <?php 
                                            $seguindo = count($user->follows);
                                            ?>
                                            @if($seguindo > 0)
                                            <button v-on:click="setCurrentPage('following')" class="btn btn-lg btn-link" :class="{'selected': navigation.current_page == 'following'}">
                                                Seguindo
                                                <span>{{$seguindo}}</span>
                                            </button>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="container-fluid">
                        <div id="loaded-content">
                            @include('painel.users.includes._resume-content')
                            <div class="frame loading-div" v-show="loading_page">
                                <img src="{{asset('/img/loading.gif')}}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-7">
                <div class="frame half">
                    @if(@Auth::guest())
                        <div class="alert alert-info alert-square" style="margin-bottom: 0;">
                            Faça <a href="{{url('/login')}}"><b>login</b></a> para ver mais opções
                        </div>
                    @endif
                    @if( !@$user->is_me && @!\Request::has('view_as_visitor'))
                        @if(@Auth::check())
                            <button v-on:click="startChat" class="btn btn-lg btn-success btn-block">
                                <i class="fa fa-comment-o"></i>&nbsp;
                                Enviar mensagem
                            </button>
                            <follow-button :block="true" :lg="true" :uid="user.id" :following="{{$user->i_am_following?'true':'false'}}"></follow-button>
                        @endif
                    @endif
                    
                    @if(@Auth::check() && $user->candidate)
                        <button v-on:click="exportResume('{{$user->slug}}')" class="btn btn-lg btn-primary btn-block">
                            <i class="fa fa-file-pdf-o"></i>&nbsp;
                            Exportar currículo
                        </button>
                    @endif
                    @if(@Auth::check() && $user->id == @Auth::user()->id)
                        @if( @\Request::get('view_as_visitor'))
                            <a class="btn btn-lg btn-info btn-block" href="{{url('/u/'.$user->slug)}}">
                                <i class="fa fa-address-card"></i>&nbsp;
                                Ver perfil como dono
                            </a>
                        @else
                            <a class="btn btn-lg btn-info btn-block" href="{{url('/u/'.$user->slug.'?view_as_visitor=true')}}">
                                <i class="fa fa-address-card-o"></i>&nbsp;
                                Ver perfil como visitante
                            </a>
                        @endif
                    @endif
                </div>
                <div class="frame np">
                    @if($user->is_me)
                        <div class="p10">
                            <p class="title">VISUALIZAÇÕES DE PERFIL</p>
                            @if(count(@$profileviews))
                                @forelse($profileviews as $pv)
                                <div class="profileview-item">
                                    <a href="{{url('/u/'.$pv->viewer->slug)}}">
                                        <div class="image-wrapper">
                                            <img src="{{$pv->viewer->thumb}}" alt="" class="img-responsive">
                                        </div>
                                        <div class="user-info">
                                            {{@$pv->viewer->name.' '.@$pv->viewer->surname}}
                                        </div>
                                    </a>
                                </div>
                                @empty
                                Nenhuma visualização ainda.
                                @endforelse
                            @endif
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>
@if(@$user->is_me)
{{-- </component> --}}
@endif
@endsection

@if(@$user->is_me)
@push('script-bottom')
<script>
    $(function () {
        
        @forelse($user->skills as $skill)
            $('.skill-tags-input')
                .tagsinput('add',{ "str_id": {{$skill->tag->id}} , "name": "{!!$skill->tag->name!!}"});
        @empty
        @endforelse

        $('.user-name,.user-surname,.about-container').editable({
            url:site_url+'/user/setInfo',
            ajaxOptions: {
                type: 'POST',
                dataType: 'json'
            },
            escape:true,
            send:'always'
        });

        $('.user-video-input').editable({
            url:site_url+'/user/setInfo',
            ajaxOptions: {
                type: 'POST',
                dataType: 'json'
            },
            escape:true,
            send:'always',
            success:function(res){
                if($('.user-about-video').length)
                {
                    $('.user-about-video').replaceWith(res.view);
                }
                else
                {
                    $('.user-about-video-wrapper').html(res.view);
                }
            }
        });

        $('.social-link').editable({
            type:'text',
            url:site_url+'/user/setInfo',
            ajaxOptions: {
                type: 'POST',
                dataType: 'json'
            },
            escape:false,
            send:'always',
            success:function(res){
                console.log(res);
            }
        });

        $('.phones-container,.emails-container').editable({
            type:'text',
            url:site_url+'/user/setInfo',
            ajaxOptions: {
                type: 'POST',
                dataType: 'json'
            },
            escape:false,
            send:'always'
        });

        $('#x-city-editable').editable({
            type:'typeaheadjs',
            url:site_url+'/user/setInfo',
            ajaxOptions: {
                type: 'POST',
                dataType: 'json'
            },
            name:'city_id',
            escape:true,
            send:'always',
            typeaheadConfig:{
                hint: true,
                highlight: true,
                minLength: 3
            },
            params: function (params) {
                var data = {};
                data['city_id'] = $('input[name=x-edit-city_id]').val();
                return data;
            },
            success:function(){

            },
            typeahead: {
                name: 'cities',
                source: cities,
                display:'nameWithCode',
                templates: {
                    empty: [
                        '<div class="empty-message">',
                            'Não foi possível encontrar a cidade',
                        '</div>'
                    ].join('\n'),
                    suggestion: function(res){
                        return $('<div><strong>'+res.name+'</strong> – '+res.state.code+'</div>');
                    }
                },
                afterSelect:function(){
                    console.log('SELECIONADO');
                },
                limit:10
            }
        })
        .on('shown', function(){
            var e_cont = $('#x-city-editable').next('.editable-container');

            $(e_cont).find('.tt-input').on('typeahead:select', function(ev, suggestion) {
                $('body').find('input[name=x-edit-city_id]').remove();
                $('body').prepend($('<input class="hidden" name="x-edit-city_id" value="'+suggestion.id+'" />'));
            });
        });
    });
</script>
@endpush
@endif