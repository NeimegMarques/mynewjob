<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{

    public $appends = ['logoPath','thumbPath'];

    // protected static function boot()
    // {
    //     parent::boot();
    //     dd($model->name);
    //     static::saving(function ($model) {
    //         $model->slug = str_slug($model->name);
    //     });
    // }

    public function getIsMineAttribute()
    {
        @$users = $this->users()->pluck('users.id')->toArray();
        return @in_array(\Auth::user()->id, $users);
    }

    public function getUrlAttribute()
    {
        return url('/c/'.$this->slug);
    }
    
    public function getLogoPathAttribute()
    {
        return $this->logo ? 
                asset('storage/uploads/companies/'.$this->id.'/img/'.$this->logo):
                asset('/img/user-placeholder.jpg');
    }
    
    public function getThumbPathAttribute()
    {
        return $this->logo ? 
                asset('storage/uploads/companies/'.$this->id.'/img/thumb_'.$this->logo):
                asset('/img/user-thumb-placeholder.jpg');
    }


    //ADMINS
    public function users()
    {
        return $this->morphedByMany('App\User', 'companiable');
    }

    public function sectors()
    {
        return $this->morphToMany('App\Models\Sector', 'sectorable');
    }

    public function vacancies()
    {
        return $this->hasMany('App\Models\Vacancy');
    }

    public function city()
    {
        return $this->belongsTo('App\Models\City');
    }
}
