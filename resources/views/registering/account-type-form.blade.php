@extends('layouts.front',[
        'page'=>'account-type',
        'props'=>[]
    ])
@section('content')
<section class="callout">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-md-offset-6">
                {{Form::open(['url'=>url('info/account-type'),'class'=>'form-horizontal form-account-type', 'role'=>'form'])}}
                    <div class="form-group">
                        <h2 class="text-center">
                            Eu quero
                            <span v-cloak v-if="!ov && !pv">...</span>
                            <span v-cloak v-if="pv"> procurar</span>
                            <span v-cloak v-if="ov && pv"> e</span>
                            <span v-cloak v-if="ov"> oferecer</span>
                            <span v-cloak v-if="ov || pv">vagas</span>
                        </h2>
                    </div>
                    <br>
                    <div class="col-sm-18 col-sm-offset-3">
                        <div v-cloak class="form-group" v-if="chooseOne">
                            <div class="alert alert-info square">
                                Selecione ao menos um das opções abaixo
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="procurar_vagas" class="btn btn-default btn-block btn-lg clearfix">
                                <input v-model="pv" value="pv" type="checkbox" name="pv" id="procurar_vagas">   Procurar vagas
                            </label>
                        </div>
                        <div class="form-group">
                            <label for="oferecer_vagas" class="btn btn-default btn-block btn-lg clearfix">
                                <input v-model="ov" value="ov" type="checkbox" name="ov" id="oferecer_vagas">   Oferecer vagas
                            </label>
                        </div>
                        <div class="form-group" style="border-top: 1px solid rgba(255,255,255,0.1); padding-top: 15px;">
                            <button v-on:click="canProceed($event)" v-bind:disabled=" !ov && !pv" class="btn-submit-info-city btn btn-danger pull-right">Continuar <i class="fa fa-chevron-right"></i>
                                </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection
