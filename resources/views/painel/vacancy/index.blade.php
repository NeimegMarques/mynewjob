@extends('layouts.app',['page'=>'vacancies-view'])

@section('content')
<!-- <section class="bg-primary">
    <div class="container">
        {{Form::open(['url'=>url('/'),'class'=>'home-search-form'])}}
            <div class="row">
                <div class="col-sm-11">
                    <input placeholder="Palavras chave" type="text" class="form-control input-lg">
                </div>
                <div class="col-sm-11">
                    <input placeholder="Cidade" type="text" class="city-typeahead tt-query form-control input-lg">
                </div>
                <div class="col-sm-2">
                    <button class="btn btn-success btn-lg btn-block">
                        <i class="fa fa-search"></i>
                    </button>
                </div>
            </div>
        {{Form::close()}}
    </div>
</section> -->
<section class="bg-primary top-background-bar"></section>
{{-- <section>
    <div class="container">
        <div class="frame half">
            Incompleto
        </div>
    </div>
</section> --}}
<section class="jobs">
	<div class="container">
        <div class="frame">
            <div class="row">
                <div class="col-sm-6">
                    @include('painel.vacancy.includes._sidebar-search')
                </div>
                <div class="col-sm-18">
                    <div class="job-list">
                        @forelse($vacancies as $vacancy)
                            @include('painel.vacancy.includes._vacancy_summary')
                        @empty
                            Nenhuma vaga por aqui
                        @endforelse
                        {{ $vacancies->appends(Request::except('page'))->links() }}
                    </div> 
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
