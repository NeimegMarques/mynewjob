<div class="resume-item">
    @include('painel.users.includes.resume-item-options')
    <div class="resume-item-content">
        <h5 class="title">
            {{$education->title}} 
        </h5>
        <p class="start-end">
            {{!$education->end?'Desde ':''}}
            {{date('M Y',strtotime($education->start))}} {{($education->end != NULL) ? ' - '.date('M Y',strtotime($education->end)) : ''}}
        </p>
        <p class="company-name">
            {{$education->entity_name}}
        </p>
        <div class="description">
            @if(@$education->city)
            <p><i class="fa fa-map-marker"></i> {{ @$education->city?@$education->city->name.' - '.@$education->city->state->code:'' }} </p>
            @endif
            <p>{{$education->description}}</p>
        </div>
    </div>
    @if(@\Auth::user()->is_me)
        <div class="resume-item-form-content hidden">
            @include('painel.users.includes.education.form-education')
        </div>
    @endif
</div>