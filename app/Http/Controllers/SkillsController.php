<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SkillsController extends Controller
{
   	public function recommend( Request $request)
   	{
   		$skill = $request->skill;
   		$rec = \App\Models\Recommendation::where('skill_id',$skill['id'])->where('user_id',\Auth::user()->id)->withTrashed()->first();
   		if( $rec && !@$rec->trashed() )
   		{
   			$rec->delete();
   			return response()->json(['deleted'=>true]);
   		} else {
            if( !$rec ){
               $skill = \App\Models\Skill::find($skill['id']);
               if(\Auth::user()->id != $skill->user_id){
                  $notification = [
                     'sender_id'=>\Auth::user()->id,
                     'receiver_id'=>$skill->user_id,
                     'type'=>'notification',
                     'message'=> 'recomendou você pela experiência com <b>'.$skill->tag->name.'</b>',
                     'link'=>url('/u/'.\Auth::user()->slug),
                     'created_at'=>date('Y-m-d H:i:s'),
                     'updated_at'=>date('Y-m-d H:i:s')
                  ];
                  \App\Models\Notification::insert(@$notification);

                  $recommendation = new \App\Models\Recommendation;
                  $recommendation->user_id = \Auth::user()->id;
                  $recommendation->skill_id = $skill['id'];
                  if($recommendation->save()){
                     $recommendation->fresh();
                     return response()->json(['recommendation'=>$recommendation->load('user')]);
                  }
               }
            } else {
               $rec->restore();
               return response()->json(['recommendation'=>$rec->load('user')]);
            }
         }
   	}
}
