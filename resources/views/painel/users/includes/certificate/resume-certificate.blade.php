<div class="resume-item">
    @include('painel.users.includes.resume-item-options')
    <div class="resume-item-content">
        <div class="row">
            <div class="container-fluid">
                <h5 class="title">
                    {{$certificate->title}} 
                </h5>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-18">
                <p class="company-name">
                    {{@$certificate->entity_name}}
                </p>
                <div class="description">
                    <p>{{$certificate->description}}</p>
                </div>
            </div>
            <div class="col-sm-6">
                <!-- <img class="img-responsive" src="http://4.bp.blogspot.com/-CZ9yWg3O0Z8/Tl30K_v48qI/AAAAAAAAELE/xooCEpzO3EE/s1600/treinaweb-cursos-online.png" alt=""> -->
            </div>
        </div>
    </div>
    @if(@\Auth::user()->is_me)
        <div class="resume-item-form-content hidden">
            @include('painel.users.includes.certificate.form-certificate')
        </div>
    @endif
</div>