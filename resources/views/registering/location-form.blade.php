@extends('layouts.front')

@section('content')
<section class="callout">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-8">
                {{Form::open(['url'=>url('info/city'),'class'=>'form-horizontal', 'role'=>'form'])}}
                    <div class="form-group">
                        <div class="container-fluid">
                            <p>Selecione o estado</p>
                            <select name="state" class="form-control" id="select-state-before">
                                @foreach($states as $s)
                                    <option value="{{$s->id}}">{{$s->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group hidden info-loader text-center">
                        <img src="{{asset('/img/loading-for-dark.gif')}}" />
                    </div>
                    <div class="hidden form-group" id="select-city-after-container">
                        <div class="container-fluid">
                            <p>Agora selecione a cidade</p>
                            <select name="city" class="form-control" id="select-city-after">
                                
                            </select>
                        </div>
                    </div>
                    <div class="hidden form-group" id="info-location-btn-container">
                        <div class="container-fluid">
                            <button class="btn-submit-info-city btn btn-danger pull-right">Continuar <i class="fa fa-chevron-right"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection
