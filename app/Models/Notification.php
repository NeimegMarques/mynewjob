<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
	public $appends = ['formatted_message'];
	protected $fillable = ['name','sender_id','receiver_id','type','message','link','created_at','updated_at'];
	public function getFormattedMessageAttribute(){
		return ($this->sender_id ? '<b>'.$this->sender->name.' '.$this->sender->surname.'</b> ' : '').$this->message;
	}

    public function sender()
    {
    	return $this->belongsTo('App\User','sender_id')->select(['users.id', 'users.name', 'users.surname', 'users.slug', 'users.image']);
    }
}
