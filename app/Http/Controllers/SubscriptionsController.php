<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class SubscriptionsController extends Controller
{
	public function index(){

		$plan_id = \Input::get('p');
		$plan_id === '' ? 'pro':$plan_id;

		if(!\Auth::check()) return redirect('register?p='.$plan_id);
		return view('subscriptions.index')->withViewName('plans');
	}
	
	public function setPlan(Request $request){
		$plan = $request->select_plan;
		return redirect('pagar?p='.$plan);
	}
}
