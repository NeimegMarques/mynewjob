<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddZipCodeLatitudeAndLongitudeToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('users', function ($table) {
            $table->string('zip_code')->after('slug')->nullable();
            $table->string('longitude')->after('slug')->nullable();
            $table->string('latitude')->after('slug')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function ($table) {
            $table->dropColumn('zip_code');
            $table->dropColumn('longitude');
            $table->dropColumn('latitude');
        });
    }
}
