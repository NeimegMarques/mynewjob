<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewApply extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $vacancy;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user,$vacancy)
    {
        $this->user = $user;
        $this->vacancy = $vacancy;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject(str_ireplace('http://', '',env('APP_URL')).' - '.$this->user->name.' '.$this->user->surname.' candidatou-se à vaga '.$this->vacancy->name)->view('emails.newApply');
    }
}
