<div class="col-sm-6 col-xs-12">
    <div class="resume-item portfolio">
        <div class="resume-item-edit">
            <a style="padding: 5px;" class="btn btn-edit-resume-item btn-default" href="{{url('/portfolio/'.@$portfolio->slug.'/editar')}}">
                <i class="fa fa-pencil"></i>
            </a>
        </div>
        <div class="resume-item-content">
            <div class="img-wrapper" style="background: url({{@$portfolio->items[0]->thumb}}); background-size: cover; background-position: center;">
                {{-- <img src="" alt="" class="img-responsive"> --}}
            </div>
            <div class="portfolio-info">
                <a href="{{url('/portfolio/'.@$portfolio->slug)}}"></a>
                <i class="fa fa-search"></i>
                <h5 class="title">
                    {{@$portfolio->title}} 
                </h5>
            </div>
        </div>
        @if(@\Auth::user()->is_me)
            <div class="resume-item-form-content hidden">
                @include('painel.users.includes.portfolio.form-portfolio')
            </div>
        @endif
    </div>
</div>