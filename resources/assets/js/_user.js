module.exports = {

	init:function(){

    var th = this;

    $(function () {
      $(document).on("scroll", th.onScroll);

      th.onScroll();
      
      $('.user-ul-menu a[href^="#"]').on('click', function (e) {
        e.preventDefault();
        $(document).off("scroll");
        
        $('.user-ul-menu li').each(function () {
            $(this).removeClass('active');
        });
        $(this).closest('li').addClass('active');

        var target = this.hash,
            menu = target;
        
        var $target = $(target);
        
        $('html, body').stop().animate({
            'scrollTop': $target.offset().top-125
        }, 500, 'swing', function () {
            window.location.hash = target;
            $(document).on("scroll", th.onScroll);
        });
      });
    });
  },

  onScroll: function(event){
    var scrollPos = $(document).scrollTop();
    $('.user-ul-menu a').each(function () {
      var currLink = $(this);
      var refElement = $(currLink.attr("href"));

      if(refElement.length){
        var ept = refElement.position().top;
        var eh = refElement.innerHeight()+parseInt(refElement.css('margin-bottom').replace("px", ""));

        if ( ept <= scrollPos && ept + eh > scrollPos || $(this).attr('href') == '#inicio') {
            $('.user-ul-menu li').removeClass("active");
            currLink.closest('li').addClass("active");
        }
        else{
            currLink.closest('li').removeClass("active");
        }
      }
    });
  }
}