<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
	protected $appends = ['nameWithCode'];
	
    public function candidates(){
    	return $this->hasMany('App\Models\Candidate');
    }

    public function vacancies(){
        return $this->hasMany('App\Models\Vacancy');
    }

    public function state(){
    	return $this->belongsTo('App\Models\State');
    }

    public function getNameWithCodeAttribute(){
    	return $this->name.' - '.$this->state->code;
    }
}
