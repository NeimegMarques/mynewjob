<?php
namespace App\Http\Controllers\Users;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\State;
use App\User;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Carbon\Carbon;
use JavaScript;
use Illuminate\Support\Facades\Redis;
class UsersController extends Controller
{
    public function show($slug){

        //Redis::publish('message', json_encode(['room' => 'room_5','message'=>"Teste"]));
	    $user = User::where('slug',$slug)->first();
        $profileviews = \App\Models\Profileview::where('profile_id',@\Auth::user()->id)->take(5)->orderBy('id','DESC')->get();
        //dd($user->skills()->toSql());
        view()->share('user', $user);

        // if($user->is_me){
        //     $applications = $user->candidacies->load('vacancy.company');
        // }

        // JavaScript::put([
        //     'user'=>$user
        // ]);

	    return view('painel.users.resume')
            ->withProfileviews($profileviews)
            ->withUser($user);
    }

    public function getPage($slug, $page){

        $user = User::where('slug',$slug)->first();

        if($page == 'followers'){
            $view = view('painel.users.includes._followers')
                ->withUser($user)
                ->render();
        } elseif($page == 'following'){
            $view = view('painel.users.includes._following')
                ->withUser($user)
                ->render();
        }

        return response()->json(['view'=>$view]);
    }

    // public function exportcv(Request $request)
    // {
    //     $users = User::whereIn('id',$request->ids)->get();
    //     $pdf = \PDF::loadView('pdf.cv', $users);
    //     return $pdf->download('candidatos.pdf');
    // }

    public function searchByQuery(Request $request, $query)
    {
        return $this->profissionais($request, $query);
    }

    public function profissionais(Request $request, $query = NULL)
    {
        //dd($request->input());
        $search = User::orderBy('created_at','DESC')->active()->where('candidate',1);
        if($request->n)
        {
            $n = $request->n;
            $search->where(function($search) use ($request){
                $search->where(function($search) use ($request){
                    $search->where('name','LIKE','%'.$request->n.'%');
                })->orWhere(function($search) use ($request){
                    $search->where('surname','LIKE','%'.$request->n.'%');
                })->orWhere(function($search) use ($request){
                    $search->where('slug','LIKE','%'.$request->n.'%');
                });
            });
        }
        if($request->i == 1)
        {
            $search->where(function($search) use ($request){
                $search->where(function($search) use ($request){
                    $search->where('image','!=','');
                });
            });
        }
        if($request->k || $query)
        {
            $query = $request->k?: $query;
            $ks = explode(' ',$query);
            $search->where(function($search) use ($ks){
                $search->where(function($search) use ($ks){
                    $search->whereHas('companies', function ($q) use ($ks) {
                        foreach($ks as $i => $k)
                        {
                            if($i==0)
                            {   
                                $q->where('name', 'LIKE', '%'.$k.'%');
                            }
                            else
                            {
                                $q->orWhere('name', 'LIKE', '%'.$k.'%');
                            }   
                        }
                    });
                });
            });
        }

        if($request->city_id)
        {
            $city_id = $request->city_id;
            $cn = $request->cn;
            $search->where(function($search) use ($request){
                if($request->cn != '')
                {
                    $search->where('city_id',$request->city_id);
                }
                else
                {
                    //dd($request->input());
                    $request->merge(['city_id' => '']);
                }
            });
        }

        //\DB::enableQueryLog();
        $users = $search->paginate(15);
        // echo '<pre>'; 
        // print_r(\DB::getQueryLog());
        // echo '</pre>';

        $states = State::all();

        if($request->ajax())
        {
            return $users->toJson();    
        }

        return view('painel.users.profissionais')
            ->withUsers($users);
    }

    public function minhasVagas(){
        return view('painel.users.vagas')
            ->withUser(\Auth::user());
    }

    public function vagasCriadas(){
        return view('painel.users.vagascriadas')
            ->withUser(\Auth::user());
    }

    public function getByQuery($query){
        $users = User::select('id','name','surname','image','slug')
                        ->where('name','LIKE','%'.$query.'%')
                        ->orWhere('surname','LIKE','%'.$query.'%')
                        ->orWhere('slug','LIKE','%'.$query.'%')
                        ->with('city')
                        ->limit(6)
                        ->get();
        if(\Request::json()){
            return $users->toJson();
        }   
    }

    public function pageview(Request $request)
    {
        if(@\Auth::check() && @$request->uid != @\Auth::user()->id)
        {
            $uid = $request->uid;
            $pv = \App\Models\Profileview::whereDate('updated_at','>=',date('Y-m-d 00:00:00'))->where('user_id',@\Auth::user()->id)->where('profile_id',$uid)->first();
            if($pv)
            {
                $pv->updated_at = date('Y-m-d H:i:s');
                $pv->save();
            } else {
                $pv = new \App\Models\Profileview;
                $pv->user_id = @\Auth::user()->id;
                $pv->profile_id = $uid;
                $pv->save();
            }
        }
    }

    public function follow(Request $request)
    {
        if(@\Auth::check() && @$request->uid != @\Auth::user()->id)
        {
            $uid = $request->uid;
            $followed = \Auth::user()->follows->where('id',$uid)->first();
            if(@$followed)
            {
                $follow = \DB::table('follows')
                    ->where('user_id',\Auth::user()->id)
                    ->where('followed_id',$uid)
                    ->update(['deleted_at'=>\DB::raw('NOW()')]);
                //$follow->save();
                //\Auth::user()->follows()->detach($uid);
                $following = false;
            } else {
                $follow = \DB::table('follows')
                            ->where('user_id',\Auth::user()->id)
                            ->where('followed_id',$uid)
                            ->whereNotNull('deleted_at')
                            ->first();
                if( @$follow->user_id > 0 ){
                    $follow = \DB::table('follows')
                        ->where('user_id',\Auth::user()->id)
                        ->where('followed_id',$uid)
                        ->whereNotNull('deleted_at')
                        ->update([
                            'deleted_at' => null
                        ]);
                } else {
                    $follow = \DB::table('follows')->insert(
                        [
                            'user_id'=>\Auth::user()->id,
                            'followed_id'=>$uid
                        ]
                    );
                    $user = \App\User::find($uid);
                    
                    if(env('APP_ENV') != 'local') {
                        \Mail::to($user->email)->queue( new \App\Mail\NewFollower($user, \Auth::user()));
                    }
                    $notification = [
                        'sender_id'=>@\Auth::user()->id,
                        'receiver_id'=>$user->id,
                        'type'=>'notification',
                        'message'=> 'agora está seguindo você',
                        'link'=>url('/u/'.\Auth::user()->slug),
                        'created_at'=>date('Y-m-d H:i:s'),
                        'updated_at'=>date('Y-m-d H:i:s')
                    ];
                    \App\Models\Notification::insert(@$notification);
                }
                $following = true;
            }

            return response()->json(['following'=>$following]);
        }
    }

    public function edit($slug){
        $user = \App\User::where('slug',$slug)->first();
        return view('painel.users.edit')
            ->withUser($user);
    }

    public function setAccontType(Request $request){
        \Auth::user()->recruiter = $request->ov ? 1 : 0;
        \Auth::user()->candidate = $request->pv ? 1 : 0;
        \Auth::user()->save();
        return redirect('painel');
    }
    public function setAddress(Request $request){
        \Auth::user()->zip_code = sanitizeContent($request->zip_code);
        \Auth::user()->longitude = sanitizeContent($request->lng);
        \Auth::user()->latitude = sanitizeContent($request->lat);
        \Auth::user()->street = sanitizeContent($request->street);
        \Auth::user()->number = sanitizeContent($request->number);
        \Auth::user()->neighborhood = sanitizeContent($request->neighborhood);
        \Auth::user()->complement = sanitizeContent($request->complement);

        if($request->city != ''){
            $city = \App\Models\City::where('name',sanitizeContent($request->city))->first();
            if($city){
                \Auth::user()->city_id = @$city->id;
            }
        }

        \Auth::user()->save();
        return redirect('painel');
    }

    public function setCity(Request $request){
        \Auth::user()->city_id = $request->city;
        \Auth::user()->save();
        return redirect('painel');
    }

    public function storeInfo(Request $request){
        switch($request->t){
            case "basic": return $this->setBasicInfo($request);
            break;
            case "pass": return $this->setPassInfo($request);
            break;
            case "social": return $this->setSocialInfo($request);
            break;
        }
    }

    public function setBasicInfo(Request $request){
        $user = \Auth::user();
        $zoom = $request->zoom;
        $offset_x = ceil($request->x*-1);
        $offset_y = ceil($request->y*-1);
        $rotation = $request->rotation;

        $birth = explode('/',$request->birth);

        $birth = Carbon::create($birth[2], $birth[1], $birth[0],0)->toDateString();
        
        if($request->hasFile('user_image'))
        {
            $image = $request->file('user_image');
            
            if($image->isValid())
            {
                $filename = date('dmY_His').'.'.$image->getClientOriginalExtension();
                $image = \Image::make($image);
                
                $image_w = $image->width();
                $image_h = $image->height();

                $n_w = $zoom*$image_w;
                $n_h = $zoom*$image_h;

                $image->rotate($rotation|0);
                $image->resize($n_w,$n_h);
                $image->crop(300,300, $offset_x, $offset_y);

                @\File::makeDirectory(storage_path('app/public/uploads'));
                @\File::makeDirectory(storage_path('app/public/uploads/users'));
                @\File::makeDirectory(storage_path('app/public/uploads/users/'.\Auth::user()->id));
                @\File::makeDirectory(storage_path('app/public/uploads/users/'.\Auth::user()->id).'/img');
                @\File::makeDirectory(storage_path('app/public/uploads/users/'.\Auth::user()->id.'/img/profile'));

                $image->save(storage_path('app/public/uploads/users/'.\Auth::user()->id.'/img/profile/'.$filename));
                $thumb = $image->resize(50,50);
                $image->save(storage_path('app/public/uploads/users/'.\Auth::user()->id.'/img/profile/thumb_'.$filename));

                $user->image = $filename;
            }
        }

        $user->name = sanitizeContent(trim($request->name));
        $user->surname = sanitizeContent(trim($request->surname));
        $user->birth = $birth;
        if($user->save()){
            return back()->withMessage('Edição concluída.');
        }
    }

    public function setPassInfo(Request $request){

        $old_pass               = $request->current_pass;
        $password               = $request->password;
        $password_confirmation  = $request->password_confirmation;

        $user = \Auth::user();

        if(\Hash::check($old_pass, \Auth::user()->password))
        {
            if(strlen($password) >= 6)
            {
                if($password == $password_confirmation){
                    $user->password = \Hash::make($password);

                    if($user->save())
                    {
                        return redirect(URL::previous() . "#pass")->withMessage('Senha alterada com sucesso');
                    }
                    else
                    {
                        return redirect(URL::previous() . "#pass")->withError('Houve um erro ao tentar salvar a nova senha');
                    }
                }
                else
                {    
                    return redirect(URL::previous() . "#pass")->withError('A nova senha não é igual à confirmação da nova senha');
                }
            }
            else
            {
                return redirect(URL::previous() . "#pass")->withError('A senha precisa ter no mínimo 6 caracteres');
            }
        }
        else
        {
            return redirect(URL::previous() . "#pass")->withError('A senha atual informada não conincide. Tente novamente');
        }
    }

    public function setSocialInfo(Request $request){

        $social = \Auth::user()->social?:new \App\Models\SocialLink;;
        
        $links = $request->link;

        $a = [];
        foreach ($links as $k => $lk) {
            $k = sanitizeContent($k);
            $social->$k = sanitizeContent($lk);
        }

        if(\Auth::user()->social()->save($social)){
            return redirect(URL::previous() . "#social")->withMessage('Os links foram atualizados com sucesso');
        }

        return redirect(URL::previous() . "#social")->withError('Houve um erro ao tentar atualizar os links');
    }


    public function setInfo(Request $request){
        
        $user = \Auth::user();

        if($request->name == 'name')
        {
            $user->name = sanitizeContent(trim($request->value));
            if($user->save()){
                return response()->json(['message'=>'O nome foi alterado com sucesso']);
            }
        }
        if($request->name == 'surname')
        {
            $user->surname = sanitizeContent(trim($request->value));
            if($user->save()){
                return response()->json(['message'=>'O sobrenome foi alterado com sucesso']);
            }
        }
        elseif($request->name == 'about')
        {
            $user->about = sanitizeContent(trim($request->value));
            if($user->save()){
                return response()->json(['message'=>'A descrição foi alterada com sucesso']);
            }   
        }
        elseif($request->has('city_id'))
        {
            $user->city_id = sanitizeContent($request->city_id);
            if($user->save()){
                return response()->json(['message'=>'A cidade foi alterada com sucesso']);
            }   
        }
        elseif($request->name == 'image')
        {
            $image = sanitizeContent($request->value);
            list($type, $data) = explode(';', $image);
            list(, $data)      = explode(',', $data);

            $filename = md5(date('ymdHis')).'.'.explode('/',$type)[1];
            
            @\File::makeDirectory(storage_path('app/public/uploads'));
            @\File::makeDirectory(storage_path('app/public/uploads/users'));
            @\File::makeDirectory(storage_path('app/public/uploads/users/'.$user->id));
            @\File::makeDirectory(storage_path('app/public/uploads/users/'.$user->id).'/img');

            file_put_contents(storage_path('app/public/uploads/users/'.$user->id.'/img/'.$filename), base64_decode($data));
            $img = \Image::make(storage_path('app/public/uploads/users/'.$user->id.'/img/'.$filename));
            $img_w = $img->width();
            $img_h = $img->height();

            $img->widen(300);
            $img->save(storage_path('app/public/uploads/users/'.$user->id.'/img/'.$filename));
            $thumb = $img->widen(90 /*, function ($constraint) {
                $constraint->upsize();
            }*/);
            $img->save(storage_path('app/public/uploads/users/'.$user->id.'/img/thumb_'.$filename));

            $user->image = $filename;
            
            if($user->save()){
                return response()->json(['message'=>'A imagem foi alterada com sucesso']);
            }   
        }
        elseif($request->has('sockid'))
        {
            $user->socketid = sanitizeContent($request->sockid);
            if($user->save()){
                return response()->json(['message'=>'O socket é '.$request->sockid]);
            }   
        }
        elseif($request->name == 'video')
        {
            $url = sanitizeContent($request->value);
            if($url){

                if(strpos($url,'www.youtube.com/embed/'))
                {
                    $video = $url;
                }
                elseif(strpos($url,'//youtu.be/'))
                {
                    $parts = explode('.be/',$request->value);
                    $video_id = explode('&',$parts[1])[0];

                    $video = 'https://www.youtube.com/embed/'.$video_id;
                }
                elseif(strpos($url,'v='))
                {
                    $parts1 = explode('v=',$request->value);
                    $video_id = explode('&',$parts1[1])[0];
                    $video = 'https://www.youtube.com/embed/'.$video_id;
                }

                $user->video = $video;

            }
            else
            {
                $video = '';
                $user->video = '';
            }

            if($user->save()){
                $view = $video ? view('painel.users.includes.user-video')->withCandidate(\Auth::user())->withVideo($video)->render() : '';
                return response()->json(['view'=>$view,'message'=>'A URL do vídeo foi alterada com sucesso']);
            }   
        }
        elseif($request->name == 'phones')
        {
            $phones = explode('/', sanitizeContent(trim($request->value)));

            \DB::beginTransaction();
            $user->phones()->delete();

            foreach ($phones as $ph) {
                $phone = new \App\Models\Phone;
                $phone->phone = $ph;
                if(!$user->phones()->save($phone))
                {
                    \DB::rollBack();
                    return response()->json(['error'=>'Houve um erro ao tentar inserir o telefone: '.$ph]);
                }
            }

            \DB::commit();
            return response()->json(['message'=>'Os telefones foram incluídos com sucesso']);

        }
        elseif($request->name == 'emails')
        {
            $emails = explode(PHP_EOL, sanitizeContent(trim($request->value)));

            \DB::beginTransaction();
            $user->emails()->delete();

            foreach ($emails as $mail) {
                $email = new \App\Models\Email;
                $email->email = $mail;
                if(!$user->emails()->save($email))
                {
                    \DB::rollBack();
                    return response()->json(['error'=>'Houve um erro ao tentar inserir o email: '.$mail]);
                }
            }

            \DB::commit();
            return response()->json(['message'=>'Os emails foram incluídos com sucesso']);

        }
        elseif(preg_match('/(social\.[a-z_]+)/', $request->name))
        {
            $field = str_replace('social.','',$request->name);

            \DB::beginTransaction();
            
            $social = \Auth::user()->social?:new \App\Models\SocialLink;;
            $social->{$field} = $request->value;
            \Auth::user()->social()->save($social);

            \DB::commit();
            return response()->json(['message'=>'O link foi salvo com sucesso']);
        }
        elseif($request->has('skills')){
            $skills = $request->skills;
            $tags_ids = [];

            \DB::beginTransaction();

            foreach ($skills as $key => $sk) {
                if(is_numeric($sk['str_id']))
                {
                    $tags_ids[] = $sk['str_id'];
                }
                elseif($sk['name'] != '')
                {
                    $tag = \App\Models\Tag::where('name',$sk['name'])->first();
                    if( !@$tag ){
                        $new_tag = new \App\Models\Tag;
                        $new_tag->name = sanitizeContent($sk['name']);
                        $new_tag->tag = str_slug(sanitizeContent($sk['name']));
                        $new_tag->save();
                        $tags_ids[] = $new_tag['id'];
                    }
                }
            }

            $uskills = $user->skills->pluck('tag_id','id');
            $skills_to_delete = [];
            $skills_tags_ids = [];

            foreach ($uskills as $skill_id => $uskill) {
                if(!in_array($uskill, $tags_ids))
                {
                    $skills_to_delete[] = $uskill;
                } else {
                    unset($tags_ids[array_search($uskill, $tags_ids)]);
                }
            }


            if(count(@$skills_to_delete))
            {
                \DB::table('skills')->whereIn('tag_id', $skills_to_delete)->delete();
            }

            $now = Carbon::now('utc')->toDateTimeString();

            foreach ($tags_ids as $input_tag_id) {
                $index = array_search($input_tag_id, $skills_tags_ids);
                if( $index === false)
                {
                    $skills_to_insert[] = [
                        'tag_id' => $input_tag_id,
                        'user_id' => \Auth::user()->id
                    ];
                }
            }

            if(count(@$skills_to_insert))
            {
                \App\Models\Skill::insert($skills_to_insert);
            }

            //if( !$user->tags()->sync($tags_ids)){
                //\DB::rollBack();
                //return response()->json(['error'=>'Houve um erro ao tentar salvar as tags. Tente novamente.']);
            //};

            \DB::commit();
            $user = $user->fresh();
            return response()->json([
                'message'=>'As competências foram salvas com sucesso',
                'skills'=>$user->skills->load('tag'),
                'normal_skills'=>$user->normalSkills,
                'recommended_skills'=>$user->recommendedSkills
            ]);
        }
        else
        {
            return response()->json(['message'=>'Opss...  Parece que o programador estava dormindo pouco e deixou passar algum erro :(']);
        }
    }

    public function storeResumeItem(Request $request,$type,$id=NULL){
        $user = \Auth::user();

        if($type == "job")
        {
            $city_id = sanitizeContent($request->get('city_id'));
            
            $start = $request->get('job_year_from').'-'.$request->get('job_month_from').'-01';
            $end = $request->get('until_now') == 'on' ? NULL : $request->get('job_year_to').'-'.$request->get('job_month_to').'-01';
            $job = $id ? $user->jobs()->find($id): new \App\Models\Job;
            
            $job->title = sanitizeContent($request->get('job_name'));
            $job->description = sanitizeContent($request->get('job_description'));
            $job->start = $start;
            $job->end = $end;
            $job->company_name = sanitizeContent($request->get('company_name'));
            $job->city_id = is_numeric($city_id)?$city_id:null;
            
            if(!$id) $job->user_id = \Auth::user()->id;
            
            if($job->save())
            {
                $view = view('painel.users.includes.job.resume-job')->withJob($job)->render();
                return response()->json(['view'=>$view,'job'=>$job,'type'=>$type,'editing'=>$id?true:false]);
            } else { }
        }
        elseif($type == 'education')
        {
            $city_id = sanitizeContent($request->get('city_id'));

            $start = $request->get('education_year_from').'-'.$request->get('education_month_from').'-01';
            $end = $request->get('until_now') == 'on' ? NULL : $request->get('education_year_to').'-'.$request->get('education_month_to').'-01';
            $education = $id ? $user->educations()->find($id): new \App\Models\Education;
            $education->title = sanitizeContent($request->get('education_name'));
            $education->description = sanitizeContent($request->get('education_description'));
            $education->start = $start;
            $education->end = $end;
            $education->entity_name = sanitizeContent($request->get('entity_name'));
            $education->city_id = is_numeric($city_id)?$city_id:null;
            
            if(!$id) $education->user_id = \Auth::user()->id;
            
            if($education->save())
            {
                $view = view('painel.users.includes.education.resume-education')->withEducation($education)->render();
                return response()->json(['view'=>$view,'education'=>$education,'type'=>$type,'editing'=>$id?true:false]);
            } else { }
        }
        elseif($type == 'course')
        {
            $city_id = sanitizeContent($request->get('city_id'));

            $start = $request->get('course_year_from').'-'.$request->get('course_month_from').'-01';
            $end = $request->get('until_now') == 'on' ? NULL : $request->get('course_year_to').'-'.$request->get('course_month_to').'-01';
            $course = $id ? $user->courses()->find($id): new \App\Models\Course;
            $course->title = sanitizeContent($request->get('course_name'));
            $course->description = sanitizeContent($request->get('course_description'));
            $course->start = $start;
            $course->end = $end;
            $course->entity_name = sanitizeContent($request->get('entity_name'));
            $course->city_id = is_numeric($city_id)?$city_id:null;
            
            if(!$id) $course->user_id = \Auth::user()->id;
            
            if($course->save())
            {
                $view = view('painel.users.includes.course.resume-course')->withCourse($course)->render();
                return response()->json(['view'=>$view,'course'=>$course,'type'=>$type,'editing'=>$id?true:false]);
            } else {}
        }
        elseif($type == 'certificate')
        {
            $certificate = $id ? $user->certificates()->find($id): new \App\Models\Certificate;
            $certificate->title = sanitizeContent($request->get('certificate_name'));
            $certificate->description = sanitizeContent($request->get('certificate_description'));
            $certificate->entity_name = sanitizeContent($request->get('entity_name'));
            
            if(!$id) $certificate->user_id = \Auth::user()->id;
            
            if($certificate->save())
            {
                $view = view('painel.users.includes.certificate.resume-certificate')->withCertificate($certificate)->render();
                return response()->json(['view'=>$view,'certificate'=>$certificate,'type'=>$type,'editing'=>$id?true:false]);
            } else { }
        }
        elseif($type == 'language')
        {
            $language = $id ? $user->languages()->find($id): new \App\Models\Language;
            $language->title = sanitizeContent($request->get('language_name'));
            $language->level = sanitizeContent($request->get('language_level'));
            
            if(!$id) $language->user_id = \Auth::user()->id;
            
            if($language->save())
            {
                $view = view('painel.users.includes.language.resume-language')->withLanguage($language)->render();
                return response()->json(['view'=>$view,'language'=>$language,'type'=>$type,'editing'=>$id?true:false]);
            } else { }
        }
        elseif($type == 'portfolio')
        {
            //dd($request->input());
            \DB::beginTransaction();

            $title = $request->portfolio_title;
            $description = $request->portfolio_description;
            $images = $request->images;
            $tags   =   explode(',',$request->portfolio_tags);
            $from_form = $request->items;

            $portfolio = $id ? $user->portfolios()->find($id) : new \App\Models\Portfolio;
            
            if($id){
                //echo $portfolio->title;
                //echo '<br />';
                //echo $title;
                $slug = $title == $portfolio->title ? $portfolio->slug : rand(1000,1000000).'-'.str_slug($title);
                //dd($slug);
            }else{
                $slug = rand(1000,1000000).'-'.str_slug($title);
            }

            $portfolio->title = $title;
            $portfolio->slug = $slug;
            $portfolio->description = $description;
            $portfolio->position = 0;
            $portfolio->active = true;

            if(!$id) $portfolio->user_id = \Auth::user()->id;
            
            if($portfolio->save()){

                $tags_ids = [];
                foreach ($tags as $key => $tag) {
                    if(is_numeric($tag))
                    {
                        $tags_ids[] = $tag;
                    }
                    elseif($tag != '')
                    {
                        $db_tag = \App\Models\Tag::where('name',$tag)->first();
                        if( !@$db_tag ){
                            $new_tag = new \App\Models\Tag;
                            $new_tag->name = $tag;
                            $new_tag->tag = str_slug($tag);
                            $new_tag->save();
                            $tags_ids[] = $new_tag['id'];
                        }
                    }
                }
                $portfolio->tags()->sync(@$tags_ids);

                $items_ids = $portfolio->items->pluck('id')->toArray();
                if(count($items_ids)){
                    foreach($items_ids as $k => $item){
                        if( !in_array($item, $from_form )){
                            $portfolioitem = \App\Models\Portfolioitem::find($item);
                            $arr = explode('.',$portfolioitem->image);
                            $name = $arr[0];
                            $ext = $arr[1];
                            \Storage::delete('storage/uploads/portfolios/'.@$portfolio->id.'/'.@$portfolioitem->image);
                            \Storage::delete('storage/uploads/portfolios/'.@$portfolio->id.'/'.@$name.'_thumb.'.$ext);
                            $portfolioitem->delete();
                        }
                    }
                }

                @\File::makeDirectory(storage_path('app/public/uploads'));
                @\File::makeDirectory(storage_path('app/public/uploads/portfolios'));
                @\File::makeDirectory(storage_path('app/public/uploads/portfolios/'.$portfolio->id));
                if(count($images)){
                    foreach($images as $k => $image){
                        $image = sanitizeContent($image);
                        list($imgtype, $data) = explode(';', $image);
                        list( ,$data)      = explode(',', $data);

                        $filename = md5(date('ymdHis')).'-'.str_random(6);
                        $extension = '.'.explode('/',$imgtype)[1];

                        file_put_contents(storage_path('app/public/uploads/portfolios/'.$portfolio->id.'/'.$filename.$extension), base64_decode($data));
                        $img = \Image::make(storage_path('app/public/uploads/portfolios/'.$portfolio->id.'/'.$filename.$extension));
                        $img_w = $img->width();
                        $img_h = $img->height();

                        $img->widen(1024, function ($constraint) {
                            $constraint->upsize();
                        });
                        $img->save(storage_path('app/public/uploads/portfolios/'.$portfolio->id.'/'.$filename.$extension));
                        $thumb = $img->widen(290);
                        $img->save(storage_path('app/public/uploads/portfolios/'.$portfolio->id.'/'.$filename.'_thumb'.$extension));

                        $portfolioitem = new \App\Models\Portfolioitem;
                        $portfolioitem->portfolio_id = $portfolio->id;
                        $portfolioitem->image = $filename.$extension;
                        $portfolioitem->position = $k;
                        $portfolio->items()->save($portfolioitem);
                    }
                }


                if(!$id && \Auth::user()->followers->count()){
                    $notifications = [];
                    foreach(\Auth::user()->followers as $follower){
                        $notifications[] = [
                            'sender_id'=>@\Auth::user()->id,
                            'receiver_id'=>$follower->id,
                            'type'=>'notification',
                            'message'=> 'acabou de publicar um projeto nos portfolios',
                            'link'=>url('/portfolio/'.$portfolio->slug),
                            'created_at'=>date('Y-m-d H:i:s'),
                            'updated_at'=>date('Y-m-d H:i:s')
                        ];
                    }
                    \App\Models\Notification::insert(@$notifications);
                }

                \DB::commit();
                if($request->ajax()){
                    $view = view('painel.users.includes.portfolio.resume-portfolio')->withPortfolio($portfolio)->render();
                    return response()->json(['view'=>$view,'portfolio'=>$portfolio,'type'=>$type,'editing'=>$id?true:false]);
                }
                return redirect(url('/portfolio/'.$portfolio->slug));
            }

            \DB::rollback();

            

            // $user->image = $filename;
            
            // if($user->save()){
            //     return response()->json(['message'=>'A imagem foi alterada com sucesso']);
            // }   




            // /////////////////////////////////////////////////////////////////////////   
            // if($language->save())
            // {
            //     $view = view('painel.users.includes.language.resume-language')->withLanguage($language)->render();
            //     return response()->json(['view'=>$view,'language'=>$language,'editing'=>$id?true:false]);
            // } else { }
        }
    }

    public function deleteResumeItem(Request $request, $type ,$id)
    {
        $user = \Auth::user();
        if(in_array($type,['certificate','course','education','job','language','portfolio']))
        {
            $relation_name = $type.'s';
            $$type = $user->{$relation_name}()->find($id);

            if($$type && $$type->delete())
            {
                return response()->json(['message'=>'O item foi excluído com sucesso']);
            }
            else
            {
                return response()->json(['error'=>'Houve um erro ao tentar excluir o item'],'400');
            }
        }
    }

    public function exportCv($slug)
    { 
        if($slug  != ''){
            $user = \App\User::where('slug', $slug)->first();
        }

        if(!@$user)
        {
            return redirect('painel');
        }

        //return view('pdf.cv');
        $pdf = \PDF::setPaper('A4')->loadView('pdf.cv', ['users'=> [$user]]);
        return $pdf->download( @$user ? $user->name.' '.$user->surname.'.pdf' :'Currículo');
        //dd($request->input());
    }
}
