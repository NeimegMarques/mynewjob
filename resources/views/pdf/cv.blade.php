<!DOCTYPE html>
<html>
    
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Currículo</title>
        <meta name="description" content="An interactive getting started guide for Brackets.">
        <link rel="stylesheet" href="main.css">
        <style>
            .page-break {
                page-break-after: always;
            }
            html{
                margin: 0;
                padding: 0;
            }
            * {
                font-family: Helvetica !important;
                -webkit-box-sizing: border-box;
                   -moz-box-sizing: border-box;
                        box-sizing: border-box;
            }

            body{
                font-family:  Helvetica;
                padding: 2cm;
            }
            
            p{
                margin: 5px 0;
            }
            
            hr{
                margin: 10px 0 20px;
                height: 2px;
                border:0;
                background: #eee;
            }
            .title{
                color:#000;
                font-weight: 600;
                margin-bottom: 10px;
                margin-top: 5px;
            }
            .entity{
                color:cornflowerblue;
                font-size: 13px;
                margin: 0;
            }
            
            .muted{
                color:#999;
            }
            
            .wrapper{
                width: 17cm;
                display: block;
                clear: both;
                float: left;
            }
            .content{
                float: left;
                width: 17cm;
                display: block;
            }
            .content .username{
                font-size: 26px;
                margin-bottom: 4px;
            }
            .content .profession{
                font-size: 15px;
                margin: 0;
            }
            .content .address,
            .content .contact{
                font-size: 13px;
                color:#666;
                margin: 5px 0;
            }
            
            .text-default{
                font-size: 13px;
                color:#666;
            }
            .topic{
                position: relative;
                z-index: 9999;
            }
        </style>
    </head>
    <body>
        <?php $count = count($users);?>
        @foreach($users as $i => $user)
            <div class="wrapper">
                <div class="content">
                    <div class="topic">
                        <h1 class="username">{{$user->name.' '.$user->surname}}</h1>
                        <p class="profession">{{@$user->occupation->title}}</p>
                        <br>
                        @if(@Auth::user()->recruiter || $user->id == @Auth::user()->id)
                            <p class="address">
                                {{$user->street?$user->street.' ' : '' }}
                                {{$user->number != '' ? $user->number: '' }}
                                {{$user->street != '' ? ', ' : '' }} 
                                {{$user->neighborhood != '' ? $user->neighborhood.', ' : '' }}
                                {{$user->city? $user->city->name.', '.$user->city->state->name.', '.$user->city->state->country->name : '' }}
                                {{ $user->complement != '' ? '<br />'.$user->complement:'' }}
                            </p>
                            <p class="contact">
                                @if(count(@$user->phones))
                                    @forelse($user->phones as $phone)
                                        {{$phone->phone}}
                                    @empty
                                    @endforelse
                                @endif
                            </p>
                            <p class="contact">
                                @if(count(@$user->emails))
                                    @forelse(@$user->emails as $email)
                                        {{$email->email}}
                                    @empty
                                    @endforelse
                                @endif
                            </p>
                        @else
                            <p class="contact">Informações de contato e endereço estão disponíveis apenas para recrutadores</p>
                        @endif
                    </div>
                    <hr>
                    <div class="topic">
                        <h4 class="title">Sobre</h4>
                        <div class="text-default">
                            <p>{!!$user->about!!}</p>
                        </div>
                    </div>
                    <hr>
                    <div class="topic">
                        <h4 class="title">Experiência</h4>
                        <div class="text-default">
                            @if(count(@$user->jobs))
                                @foreach($user->jobs as $job)
                                    <p><b>{{$job->title}}</b></p>
                                    <p>
                                        <span class="entity">
                                            {{@$job->company_name}}
                                        </span>
                                        <span class="muted">
                                            {{!$job->end?'Desde ':''}}
                                            {{date('M Y',strtotime($job->start))}} {{($job->end != NULL) ? ' - '.date('M Y',strtotime($job->end)) : ''}}
                                        </span>
                                    </p>
                                    <br>
                                @endforeach
                            @endif
                        </div>
                    </div>
                    <hr>
                    <div class="topic">
                        <h4 class="title">Educação</h4>
                        <div class="text-default">
                            @if(count(@$user->educations))
                                @foreach($user->educations as $education)
                                    <p><b>{{$education->title}}</b></p>
                                    <p>
                                        <span class="entity">
                                            {{@$education->entity_name}}
                                        </span>
                                        <span class="muted">
                                            {{!$education->end?'Desde ':''}}
                                            {{date('M Y',strtotime($education->start))}} {{($education->end != NULL) ? ' - '.date('M Y',strtotime($education->end)) : ''}}
                                        </span>
                                    </p>
                                    <br>
                                @endforeach
                            @endif
                        </div>
                    </div>
                    <hr>
                    <div class="topic">
                        <h4 class="title">Idiomas</h4>
                        <div class="text-default">
                            @if(count(@$user->jobs))
                                @foreach($user->languages as $language)
                                    <p><b>{{languages($language->title)}}</b> <span class="muted">({{language_levels($language->level)}})</span></p>
                                    <br>
                                @endforeach
                            @endif
                        </div>
                    </div>
                    <hr>
                    <div class="topic">
                        <h4 class="title">Cursos</h4>
                        <div class="text-default">
                            @if(count(@$user->jobs))
                                @foreach($user->courses as $course)
                                    <p><b>{{$course->title}}</b></p>
                                    <p>
                                        <span class="entity">
                                            {{@$course->entity_name}}
                                        </span>
                                        <span class="muted">
                                            {{!$course->end?'Desde ':''}}
                                            {{date('M Y',strtotime($course->start))}} {{($course->end != NULL) ? ' - '.date('M Y',strtotime($course->end)) : ''}}
                                        </span>
                                    </p>
                                    <br>
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            @if($i < $count - 1)
            <div class="page-break"></div>
            @endif
        @endforeach
    </body>
</html>