module.exports = {

	init:function(){
    $(function(){

      $('.notifs-container').slimScroll({
        height:460,
        size: '6px',
      });

      $('.search-wrapper input').typeahead({
        hint: true,
        highlight: true,
        minLength: 3
      },
      {
        name: 'users',
        source: users,
        display:'name',
        templates: {
          suggestion: function(user){
            var t = ''+
              '<div>'+
                '<a href="'+site_url+'/u/'+user.slug+'">'+
                  '<div class="img-wrapper">'+
                    (user.thumb ? '<img src="'+user.thumb+'" />' : '')+
                  '</div>'+
                  '<div class="item-infos">'+
                    '<div class="item-title">'+user.name+'</div>'+
                    '<div class="item-info">'+(user.city ? user.city.name : '')+'</div>'+
                  '</div>'+
                '</a>'+
              '</div>';
            return $(t);
          },
          header: '<h4 class="tt-dataset-title">Usuários</h4>'
        }
      },
      {
        name: 'companies',
        source: companies,
        display:'name',
        templates: {
          suggestion: function(company){
            var t = ''+
              '<div>'+
                '<a href="'+site_url+'/company/'+company.id+'">'+
                  '<div class="img-wrapper">'+
                    (company.thumbPath ? '<img src="'+company.thumbPath+'" />' : '')+
                  '</div>'+
                  '<div class="item-infos">'+
                    '<div class="item-title">'+company.name+'</div>'+
                  '</div>'+
                '</a>'+
              '</div>';
            return $(t);
          },
          header: '<h4 class="tt-dataset-title">Empresas</h4>'
        }
      },
      {
        name: 'vacancies',
        source: vacancies,
        display:'name',
        templates: {
          suggestion: function(vacancy){
            var t = ''+
              '<div>'+
                '<a href="'+site_url+'/v/'+vacancy.id+'">'+
                  '<div class="img-wrapper">'+
                    (vacancy.thumbPath ? '<img src="'+vacancy.thumbPath+'" />' : '')+
                  '</div>'+
                  '<div class="item-infos">'+
                    '<div class="item-title">'+vacancy.name+'</div>'+
                  '</div>'+
                '</a>'+
              '</div>';
            return $(t);
          },
          header: '<h4 class="tt-dataset-title">Vagas</h4>'
        }
      })
      .on('typeahead:select', function(e,item){
        /* navigate to the selected item */
        window.location.href = item.link;
      });
    });
  }
}