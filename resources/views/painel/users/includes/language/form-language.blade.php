{{Form::open(['url'=>url('/user/resume/language/'.@$language->id)])}}
@if(@$language)
	{{Form::hidden('_method','PUT')}}
@endif
<div class="form-group">
	<label for="" class="control-label">
		Língua
	</label>
	{{Form::select('language_name',languages(),@$language->name,['class'=>'form-control input-sm','tabindex'=>'1'])}}
</div>
<div class="form-group">
	<label for="" class="control-label">
		Nível
	</label>
	{{Form::select('language_level',language_levels(),@$language->level,['class'=>'form-control input-sm','tabindex'=>'1'])}}
</div>
@include('painel.users.includes.resume-item-form-buttons',['item'=>@$language])
{{Form::close()}}