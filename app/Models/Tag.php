<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    public function users()
    {
        return $this->morphedByMany('App\User', 'taggable');
    }

    public function vacancies()
    {
        return $this->morphedByMany('App\Models\Vacancy', 'taggable');
    }
}
