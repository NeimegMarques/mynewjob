<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class PaymentController extends Controller
{
    public function index(Request $request){
        if($request->p =='gratis'){
            return $this->makeFreeSubscription($request);
        }else if($request->p == ''){
            return redirect(url('contratar'));
        };
    	return view('subscriptions.payment');
    }

    public function makeFreeSubscription(Request $request){
        $user = \Auth::user();
        $user->free = true;
        $user->save();

        return redirect('painel');
    }

    public function postPayment(Request $request){

    	$user = \Auth::user();

    	if($user->subscribed('gratis')){
            $current_plan = 'gratis';
    	}elseif($user->subscribed('pro')){
    		$current_plan = 'pro';
    	}elseif($user->subscribed('vip')){
            $current_plan = 'vip';
    	}

        if(@$current_plan != '')
    	{
    		$user->subscription($current_plan)->swap($request->plan);
    		$subscription = $user->subscription($current_plan);
    		$subscription->name = $request->plan;
    		$subscription->save();
    		return redirect('painel');
    	}
        

        if(@$request->plan == '')
        {
            return redirect(url('/contratar'));
        }
        $user->newSubscription($request->plan, $request->plan)
        ->create($request->stripeToken,[
            'email'=>@$request->stripeEmail
        ]);
        return redirect('painel');
    }
}
