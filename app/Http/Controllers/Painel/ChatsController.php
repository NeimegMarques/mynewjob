<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ChatsController extends Controller
{
    public function getLasts($page = 1)
    {
        $notReadMessages = \App\Models\Chat::where('read',0)
            ->where('receiver_id',@\Auth::user()->id)
            ->update(['notified' => TRUE ]);

    	$perPage = 10;
    	$groups = "SELECT SQL_CALC_FOUND_ROWS
    			u.id AS uid,
    			u.image AS image,
				ch.sender_id,
				ch.notified,
			    ch.receiver_id,
				u.name AS name,
				u.surname AS surname,
				ch.created_at,
			    ch.message AS message
			FROM chats ch
			INNER JOIN users u ON u.id = 
			    IF(ch.sender_id = ".\Auth::user()->id.",
			       ch.receiver_id,
			       ch.sender_id
			    )
			    WHERE ch.id IN(
			    	SELECT MAX(id) AS id FROM(
			        	SELECT id,
			        		sender_id AS id_with
			        	FROM chats
			        	WHERE receiver_id = ".\Auth::user()->id."
			        	UNION ALL
			        	SELECT id,
			        		receiver_id AS id_with
			        	FROM chats
			        WHERE sender_id = ".\Auth::user()->id.") AS t
			    	GROUP BY id_with)
			        ORDER BY ch.id DESC";
		$chats = \DB::select( \DB::raw($groups));

        foreach($chats as $key => $chat)
        {
            $notread = \App\Models\Chat::where('read',0)
            ->where('receiver_id',@\Auth::user()->id)
            ->where('sender_id',$chat->uid)
            ->count();
            $chats[$key]->unread = $notread;
        };

    	return response()->json(['chats'=>$chats,'notreadmessages'=>$notReadMessages]);
    }

    public function getByPartner($partner)
    {
    	//\DB::enableQueryLog();

    	$notReadMessages = \App\Models\Chat::where('read',0)
            ->where('receiver_id',@\Auth::user()->id)
            ->where('sender_id',$partner)
            ->update(['read' => TRUE ]);

    	$msgs = \App\Models\Chat::
    		join('users as s',\DB::raw('s.id'),'chats.sender_id')
    		//->join('users','receiver_id','id')
    		->where('receiver_id',\Auth::user()->id)
    		->where('sender_id',$partner)
    		->orWhere('sender_id',\Auth::user()->id)
    		->where('receiver_id', $partner)
    		->orderBy('id','DESC')
    		->take(25)
    		->get(['chats.*','s.name','s.surname']);
    	//$lastQuery = \DB::getQueryLog();
    	//dd($lastQuery);

    	$nmsgs = collect();
    	foreach ($msgs->sort() as $key => $msg) {
    		$nmsgs->push($msg);
    	}

    	return response()->json(['messages' => $nmsgs]);

    }
}
