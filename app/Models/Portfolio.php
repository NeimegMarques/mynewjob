<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Portfolio extends Model
{
	public function items()
	{
		return $this->hasMany('\App\Models\Portfolioitem');
	}

    public function user()
    {
    	return $this->belongsTo('\App\User');
    }

    public function tags()
    {
        return $this->morphToMany('App\Models\Tag', 'taggable');
    }

    public function getNextAttribute()
    {
    	$next = \App\Models\Portfolio::where('id','>',$this->id)->where('user_id', $this->user_id)->first();
    	if( !$next ){
    		$next = \App\Models\Portfolio::where('id','<',$this->id)->where('user_id', $this->user_id)->orderBy('id','DESC')->first();
    	}
    	return $next->slug;
    }

    public function getPrevAttribute()
    {
    	$prev = \App\Models\Portfolio::where('id','<', $this->id)->where('user_id', $this->user_id)->orderBy('id','DESC')->first();
    	if( !$prev ){
			$prev = \App\Models\Portfolio::where('id','>',$this->id)->where('user_id', $this->user_id)->first();    		
    	}
    	return $prev->slug;
    }
}
