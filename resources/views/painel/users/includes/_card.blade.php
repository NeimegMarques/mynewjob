<div class="user-card clearfix">
    <div class="user-image">
        <a href="{{url('u/'.@$user->slug)}}"><img src="{{@$user->imagePath}}" alt="" class="img-responsive"></a>
    </div>
    <div class="user-content">
        <h5 class="user-name"><a target="_blank" href="{{url('u/'.@$user->slug)}}">{{$user->name.' '.$user->surname}}</a></h5>
        @if(@$user->occupation->title)
            <p class="occupation">
                <i class="fa fa-briefcase"></i> {{@$user->occupation->title}}
            </p>
        @endif
        <ul class="user-infos">
            @if($user->city)
                <li class="user-info"><i class="fa fa-map-marker"></i> {{@$user->city->name}}</li>
            @endif
        </ul>
        @if($user->id != @Auth::user()->id)
            <div class="clearfix"><follow-button :sm="true" :uid="{{$user->id}}" :following="{{$user->i_am_following?'true':'false'}}"></follow-button></div>
        @endif
    </div></a>
</div>