
{{Form::open(['url'=>url('/user/resume/education/'.@$education->id)])}}
@if(@$education)
	{{Form::hidden('_method','PUT')}}
@endif
<div class="form-group">
	<label for="" class="control-label">
		Curso
	</label>
	{{Form::text('education_name',@$education->title,['class'=>'form-control input-sm','tabindex'=>'1'])}}
</div>
<div class="form-group">
	<label for="" class="control-label">
		Entidade
	</label>
	{{Form::text('entity_name',@$education->entity_name,['class'=>'form-control input-sm','tabindex'=>'1'])}}
</div>
<div class="form-group">
	<label for="" class="control-label">
		Cidade
	</label>
	<p class="clearfix">
		{{Form::text('education_city',@$education->city ? @$education->city->name.' - '.@$education->city->state->code:'',['class'=>'form-control input-sm city-typeahead','tabindex'=>'1'])}}
	</p>
</div>
<div class="form-group">
	<label for="" class="control-label">
		Período
	</label>
	<div class="row">
		<div class="time-range">
			<div class="container-fluid">
				<div class="time-from">
					<div class="month-from">
						{{Form::select('education_month_from',months(),date('m',strtotime(@$education->start)),['class'=>'form-control input-sm','tabindex'=>'1'])}}
					</div>
					<div class="year-from">
						{{Form::select('education_year_from',years(),date('Y',strtotime(@$education->start)),['class'=>'form-control input-sm','tabindex'=>'1'])}}
					</div>
				</div>
				<div class="range-until">
					até
				</div>
				<div class="time-to">
					<div class="time-to-container @if(!@$education->end) hidden @endif">
						<div class="month-to">
							{{Form::select('education_month_to',months(),date('m',strtotime(@$education->end)),['class'=>'form-control input-sm','tabindex'=>'1'])}}
						</div>
						<div class="year-to">
							{{Form::select('education_year_to',years(),date('Y',strtotime(@$education->end)),['class'=>'form-control input-sm','tabindex'=>'1'])}}
						</div>
					</div>

					<div class="until-now-text @if(@$education->end) hidden @endif">agora</div>

					<label for="{{@$education?@$education->id.'-':''}}education-until-now-select" class="until-now-label">
						<input @if(!@$education->end) checked @endif tabindex="1" class="styled-checkbox until-now-checkbox" id="{{@$education?@$education->id.'-':''}}education-until-now-select" type="checkbox" name="until_now">
						Ainda estou cursando
					</label>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="form-group">
	<label for="" class="control-label">
		Descrição
	</label>
	{{Form::textarea('education_description',@$education->description,['rows'=>'4','class'=>'form-control input-sm','tabindex'=>'1'])}}
</div>
<hr>
@include('painel.users.includes.resume-item-form-buttons',['item'=>@$education])
@if(old('city_id') || @$education)
    {{Form::hidden('city_id',old('city_id')?:$education->city_id)}}
@endif
{{Form::close()}}