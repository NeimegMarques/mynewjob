@extends('layouts.front')

@section('content')
<section class="callout">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-md-offset-6">
                {{Form::open(['url'=>url('info/zip_code'),'class'=>'form-horizontal', 'role'=>'form'])}}
                    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                        <div class="col-sm-24">
                            @if(session()->has('message'))
                                <div class="alert alert-info">{{ session('message') }}</div>
                            @endif
                            <br>
                            <br>
                            <p class="text-center">Não recebeu o email com o link de confirmação de conta?</p>
                            <br>
                            <p class="text-center">
                                <a href="{{url('resend')}}" class="btn btn-success btn-lg">Enviar novamente o link de confirmação</a>
                            </p>
                        </div>
                    </div>
                    <div class="info-input-container hidden">
                        <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                            <div class="container-fluid">
                                <input placeholder="Nome da rua, Número, Bairro, Cidade, Estado" tabindex="1" type="text" class="form-control info-input-address">
                            </div>
                        </div>
                    </div>
                    <div class="form-group hidden info-loader text-center">
                        <img src="{{asset('/img/loading-for-dark.gif')}}" />
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection
