<div class="resume-item">
    @include('painel.users.includes.resume-item-options')
    <div class="resume-item-content">
        <h5 class="title">
            {{$course->title}} 
        </h5>
        <p class="start-end">
            {{!$course->end?'Desde ':''}}
            {{date('M Y',strtotime($course->start))}} {{($course->end != NULL) ? ' - '.date('M Y',strtotime($course->end)) : ''}}
        </p>
        <p class="company-name">
            {{@$course->entity_name}}
            @if(@$course->city)
            <span class="small text-muted">
                <i class="fa fa-map-marker text-info"></i> {{ @$course->city?@$course->city->name.' - '.@$course->city->state->code:'' }}
            </span>
            @endif
        </p>
        <div class="description">
            <p>{{$course->description}}</p>
        </div>
    </div>
    @if(@\Auth::user()->is_me)
        <div class="resume-item-form-content hidden">
            @include('painel.users.includes.course.form-course')
        </div>
    @endif
</div>