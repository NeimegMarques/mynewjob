<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public $with = ['hashtags','images'];
    public function user()
    {
    	return $this->belongsTo('\App\User');
    }

    public function comments()
    {
    	return $this->morphMany('App\Models\Comment','commentable');
    }

    public function likes()
    {
        return $this->morphToMany('App\User', 'likeable')->select(['users.id', 'users.name', 'users.surname', 'users.slug']);
    }

    public function images()
    {
    	return $this->morphMany('App\Models\Image','imageable');
    }

    public function hashtags()
    {
    	return $this->belongsToMany('App\Models\Posttag');
    }
}
