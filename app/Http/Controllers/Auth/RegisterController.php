<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Mail\ConfirmAccount;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/painel';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'surname' => 'required|max:255',
            'username' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */

    protected function create(array $data)
    {

        //$this->redirectTo = $data['p']?'pagar?p='.$data['p']:'contratar';
        $this->redirectTo = 'confirm';
        session()->flash('message', 'Enviamos um link para confirmar sua conta. Verifique sua caixa de entrada de emails.');

        $slug = $this->createSlug($data['username'],$data['name']);
        
        $name = $data['name'];
        $surname = $data['surname'];

        $user = User::create([
            'name' => trim($name),
            'surname' => trim($surname),
            'slug' => trim($slug),
            'email' => trim($data['email']),
            'password' => bcrypt($data['password']),
        ]);

        @$email = new \App\Models\Email;
        @$email->email = $user->email;
        @$user->emails()->save(@$email);

        $initials = substr($name, 0, 1).($surname != '' ? substr($surname, 0, 1) : substr($name, 1, 1));

        $colors = ['#aa00ff','#ff6600','#005ce6','#00b33c','#009999','#59b300','#4d4dff','#4d88ff','#66cc00','#006699','#6666ff','#B452CD','#483D8B','#3D59AB','#33A1C9','#00CED1','#03A89E','#00CD66','#388E8E','#9ACD32','#9BCD9B'];
        
        $color = $colors[rand(0,count($colors)-1)];

        $image = \Image::canvas(295,295,$color);
        for($i = 295; $i >= 148; $i-=2){
            $image->text(strtoupper($initials), $i, $i, function($font) use($color){
                $font->file('fonts/google/source_sans_pro/SourceSansPro-Regular.ttf');
                $font->size(200);
                $font->color(adjustBrightness($color, -30));
                $font->align('center');
                $font->valign('middle');
            });
        }

        for($i = 158; $i >= 148; $i-=2){
            $image->text(strtoupper($initials), $i, $i, function($font) use($color){
                $font->file('fonts/google/source_sans_pro/SourceSansPro-Regular.ttf');
                $font->size(200);
                $font->color(adjustBrightness($color, -50));
                $font->align('center');
                $font->valign('middle');
            });
        }
        $image->text(strtoupper($initials), 147.5, 147.5, function($font) {
            $font->file('fonts/google/source_sans_pro/SourceSansPro-Regular.ttf');
            $font->size(200);
            $font->color('#ffffff');
            $font->align('center');
            $font->valign('middle');
        });
        @\File::makeDirectory(storage_path('app/public/uploads/users/placeholders'));

        if(!@\File::exists(storage_path('app/public/uploads/users/placeholders/'.$initials.'.jpg'))){
            $image->save(storage_path('app/public/uploads/users/placeholders/'.$initials.'.jpg'));
            $thumb = $image->resize(null, 60, function ($constraint) {
                        $constraint->aspectRatio();
                    });
            $thumb->save(storage_path('app/public/uploads/users/placeholders/'.$initials.'-thumb.jpg'));
        }

        if(env('APP_ENV') != 'local'){
            \Mail::to($data['email'])->send( new ConfirmAccount($user));
        }

        $notification = [
             'receiver_id'=>$user->id,
             'type'=>'notification',
             'message'=> 'Obrigado por juntar-se a nós. <br />Agora você pode ir para sua <b>página de pefil</b> editar suas informações pessoais e profissionais. Boa sorte!',
             'link'=>url('/u/'.$user->slug),
             'created_at'=>date('Y-m-d H:i:s'),
             'updated_at'=>date('Y-m-d H:i:s')
        ];
        \App\Models\Notification::insert(@$notification);

        return $user;

    }

    protected function createSlug($username,$name = NULL)
    {
        $slug = str_slug($username,'_');
        $rand = rand(100,999);
        $rand = $rand === 171 ? 172 : $rand;
        return !$this->slugExists($slug) ? $slug : (!$this->slugExists(str_slug($name)) ? str_slug($name) : $this->createSlug($slug.'_'.$rand));
    }

    protected function slugExists($slug)
    {
        $user = User::where('slug',$slug)->first();

        return $user === null ? false : true;
    }
}
