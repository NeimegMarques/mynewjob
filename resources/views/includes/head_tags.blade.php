<title>{{$title}}</title>
<meta name="description" content="{{$description}}"/>
<meta name="keywords" content="{{$keywords}}"/>
<meta name="url" content="{{$url}}"/>
<meta property="fb:app_id" content="1555887301393105"/>
<meta property="og:url" content="{{$url}}"/>
<meta property="og:type" content="{{$type}}"/>
<meta property="og:title" content="{{$title}}"/>
<meta property="og:description" content="{{$description}}"/>
<meta property="og:image" content="{{$image}}"/>
<meta property="og:site_name" content="{{env('APP_NAME')}}"/>
<meta property="og:locale" content="PT_br"/>

@include("includes.head_common")