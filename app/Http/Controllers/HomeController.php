<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $newvacancies = \App\Models\Vacancy::orderBy('id','DESC')->take(10)->active()->get();
        $users = \App\User::count();
        $jobs = \App\Models\Vacancy::active()->count();
        $companies = \App\Models\Company::count();
        return view('front')
            ->withNewVacancies($newvacancies)
            ->withUsers($users)
            ->withCompanies($companies)
            ->withJobs($jobs);
    }
    
    public function confirm()
    {
    	return view('registering.confirm-email');	
    }

    public function confirmAccount()
    {
        $token = \Request::input('token');
        $user = \App\User::where('email_token',$token)->first();
        $user->confirmed = true;
        $user->save();
        return redirect('painel');
    }

    public function resendConfirmEmail()
    {
    	$user = \Auth::user();
    	if(strlen($user->email_token) < 30){
    		$user->email_token = str_random(30);
    		$user->save();
    	}
    	\Mail::to($user->email)->send( new \App\Mail\ConfirmAccount($user));
    	session()->flash('message', 'Enviamos um link para confirmar sua conta. Verifique sua caixa de entrada de emails.');
    	return redirect('confirm');	
    }
}
