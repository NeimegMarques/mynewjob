<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class ConfirmedAccountMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null, $inverseCheck = null)
    {
        if( @Auth::check() && !@Auth::user()->confirmed){
            return redirect('confirm');
        }

        return $next($request);
    }
}
