<?php

namespace App\Http\Controllers\Timeline;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TimelineController extends Controller
{
    public function index()
    {
    	return view('timeline.index');
    }
}
