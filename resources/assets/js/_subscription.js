module.exports = {

    init:function(){
        var th = this;
        $(function(){
            $(document).on('click','.plans-list-selector .plan',function(){
                $('.plan').each(function(i,item){
                    var radio = $(item).find('input[type=radio]');
                    $(radio).prop("checked",false);
                });

                var rdio = $(this).find('input[type=radio]');

                $(rdio).prop("checked",true);
                
                $(this).closest('form').find('button').removeAttr('disabled');
                
            });
        });
    }
}