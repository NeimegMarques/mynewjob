<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct(){
    	session([
    		'user_id'=>@\Auth::user()->id
    	]);
    }

    public function suggestion(\Illuminate\Http\Request $request)
    {
    	if(env('APP_ENV') != 'local'  || 1) {
            \Mail::to(env('MAIL_USERNAME'))->send( new \App\Mail\NewSuggestion(\Auth::user(),$request));
        }

    	return response()->json($request->input());
    }
}
