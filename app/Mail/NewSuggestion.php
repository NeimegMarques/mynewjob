<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewSuggestion extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $request;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user,$request)
    {
        $this->user = $user;
        $this->request = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject(($this->request->suggestion_type == 'suggestion' ? 'Sugestão':'Erro').' - '.$this->user->name.' '.$this->user->surname)
                    ->replyTo($this->user->email, $this->user->name.' '.$this->user->surname)
                    ->view('emails.newSuggestion');
    }
}
