<div class="topbar">
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">
    
                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
    
                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ @Auth::check() ? url('/painel') : url('/')}}">
                    My<span class="text-primary">New</span>Job
                </a>
            </div>
    
            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                @if(@Auth::check())
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav hidden-xs">
                    <li>
                        <div class="search-wrapper">
                            <input placeholder="Pesquisar pessoas" type="text" class="form-control" value="{{Request::is('u/*') ? $user->slug :''}}">
                        </div>
                    </li>
                </ul>
                <ul class="nav navbar-nav nav-notif hidden-xs">
                    <li class="dropdown" v-cloak>
                        <a href="#" v-on:click="showchat = true" class="dropdown-toggle" type="button" id="dropdownMenuMessages" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            <i class="glyphicon glyphicon-comment"></i>
                            <span v-if="notreadmessages > 0" class="notif-counter">@{{notreadmessages}}</span>
                            <span v-if="someFriendTyping" class="notif-friend-typing">
                                <img class="img-responsive" src="{{asset('img/typing.svg')}}" alt="...">
                            </span>
                        </a>
                    </li>
                    <notifications v-ref:notifications 
                        :url="url" 
                        :notnotifiednotif="notnotifiednotifications"
                        inline-template>
                        <li class="dropdown" v-cloak>
                            <div class="dropdown-menu-arrow"></div>
                            <ul class="dropdown-menu" id="dropdown-menu-notifications">
                                <li class="dropdown-header">
                                    Notificações
                                </li>
                                <div class="notifs-container">
                                    <li v-for="notif in notifications" v-bind:class="{'not-seen': !notif.seen}" class="clearfix notif-item" v-bind:data-nid="@{{notif.id}}">
                                        <div class="notif-img-wrapper">
                                            <img src="@{{ notif.sender ? notif.sender.thumb : $root.url+'/logo-mini.png' }}" class="img-responsive">
                                        </div>
                                        <div class="notif-content">
                                            <div class="notif-title">
                                            @{{{notif.formatted_message}}}</div>
                                        </div>
                                        <a href="@{{notif.link}}"></a>
                                    </li>
                                    <li v-show="loading_notifications" class="notif-item clearfix text-center">
                                        <br><br>
                                        <span><img src="{{asset('img/typing.svg')}}" alt=""></span>
                                    </li>
                                </div>
                            </ul>
                            <a href="#" v-on:click="loadnotifications()" class="dropdown-toggle" type="button" id="dropdownMenuNotifications" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                <i class="glyphicon glyphicon-globe"></i>
                                <span v-if="notnotifiednotifications > 0" class="notif-counter">@{{notnotifiednotifications}}</span>
                            </a>
                        </li>
                    </notifications>
                </ul>
                <ul class="nav navbar-nav">
                    <li><a href="{{ url('/painel') }}">vagas</a></li>
                    <li><a href="{{ url('/timeline') }}">Timeline</a></li>
                    <li><a style="background: #3c3; margin: 10px 0; border-radius: 4px; color: #fff; padding: 6px 10px; margin-top: 8px;
    " href="{{ url('v/new') }}">Publicar vaga</a></li>
                    <li><a href="{{ url('/profissionais') }}">profissionais</a></li>
                </ul>
                @endif

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li class="login-register-links"><a href="{{ url('/login') }}">Entrar</a></li>
                        <li class="login-register-links"><a href="{{ url('/register') }}">Registrar</a></li>
                    @else
                        <li>
                            <a href="{{url('u/'.Auth::user()->slug)}}">
                                <span class="prof-name">
                                    {{ Auth::user()->name }}
                                </span>
                                <span class="prof-image hidden-xs">
                                    <img src="{{Auth::user()->thumb}}" alt="" class="img-responsive">
                                </span>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                menu
                            </a>
    
                            <div class="dropdown-menu" role="menu">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="menu-item">
                                            <a href="{{ url('v/new') }}">Publicar vaga</a>
                                        </div>
                                        <div class="menu-item">
                                            <a href="{{url('/minhas-vagas')}}">Vagas que me candidatei</a>
                                        </div>
                                        <div class="menu-item">
                                            <a href="{{url('/vagas-criadas')}}">Vagas que criei</a>
                                        </div>
                                        <div class="menu-item">
                                            <a href="{{url('/u/'.@Auth::user()->slug)}}">Configurações da conta</a>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="menu-item">
                                            {{Form::open(['url'=>url('logout')])}}
                                                <button class="btn-link btn-block text-left"><i class="fa fa-btn fa-sign-out"></i> Sair</button>
                                            {{Form::close()}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>
    <nav class="navbar navbar-default navbar-sub visible-xs">
        <div class="container">
            @if(@Auth::check())
            <ul class="nav nav-pills">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        <i class="fa fa-search"></i>
                    </a>
                    <ul class="dropdown-menu mobile-search-dropdown">
                        <li>
                            <div class="search-wrapper">
                                <input placeholder="Pesquisar pessoas" type="text" class="form-control" value="{{Request::is('u/*') ? $user->slug :''}}">
                            </div>
                        </li>
                    </ul>
                </li>
                <li class="pull-right">
                    <a href="#" v-on:click="showchat = true" class="dropdown-toggle" type="button">
                        <i class="glyphicon glyphicon-comment"></i>
                        <span v-if="notreadmessages > 0" class="notif-counter">@{{notreadmessages}}</span>
                        <span v-if="someFriendTyping" class="notif-friend-typing">
                            <img class="img-responsive" src="{{asset('img/typing.svg')}}" alt="...">
                        </span>
                    </a>
                </li>
                <li class="pull-right">
                    <a href="#" v-on:click="showchat = true" class="dropdown-toggle" type="button">
                        <i class="glyphicon glyphicon-globe"></i>
                        <span v-if="notreadmessages > 0" class="notif-counter">@{{notreadmessages}}</span>
                    </a>
                </li>
            </ul>
            @else
            <div class="text-center" style="padding: 10px 0;">
                <a style="display: inline;" href="{{url('login')}}">Entrar</a>&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp; 
                <a style="display: inline;" href="{{url('register')}}">Criar conta</a>
            </div>
            @endif
        </div>
    </nav>
</div>