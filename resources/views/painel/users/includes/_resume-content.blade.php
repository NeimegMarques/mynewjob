<div class="div social-links" v-show="navigation.current_page == 'resume' && !loading_page">
    <span class="personal-website">
        @if(@$user->is_me || @$user->social->website != '')
            <span class="btn btn-sm btn-primary">
                <i class="fa fa-globe"></i>
            </span>
            &nbsp;
        @endif
        @if(!$user->is_me)
        <a href="{{str_replace('linkedin.com','',@$user->social->website)}}" target="_blank">{{preg_replace('/(https?:\/\/)/', '', @$user->social->website)}}</a>
        @else
        <span 
            class="social-link" 
                data-type="text" 
                data-name="social.website" 
                data-placeholder="Tem um site?" 
                data-emptytext="www.seusite.com">{{@$user->social->website}}</span>
        @endif
    </span>
    <ul class="pull-right social-menu">
        @if(@$user->social->github || $user->is_me)
        <li>
            <a target="_blank" href="{{@$user->social->github}}" class="btn btn-xs btn-github link-github">
                <i class="fa fa-github"></i>
            </a>
            @if($user->is_me)
                <span 
                class="social-link" 
                    data-type="text" 
                    data-name="social.github"
                    data-placeholder="Github" 
                    data-emptytext="">{{@$user->social->github}}</span>
            @endif
        </li>
        @endif
        @if(@$user->social->bitbucket || $user->is_me)
        <li>
            <a target="_blank" href="{{@$user->social->bitbucket}}" class="btn btn-xs btn-bitbucket link-bitbucket">
                <i class="fa fa-bitbucket"></i>
            </a>
            @if($user->is_me)
                <span 
                class="social-link" 
                    data-type="text" 
                    data-name="social.bitbucket"
                    data-placeholder="Bitbucket" 
                    data-emptytext="">{{@$user->social->bitbucket}}</span>
            @endif
        </li>
        @endif
        @if(@$user->social->stackoverflow || $user->is_me)
        <li>
            <a target="_blank" href="{{@$user->social->stackoverflow}}" class="btn btn-xs btn-stackoverflow link-stackoverflow">
                <i class="fa fa-stack-overflow"></i>
            </a>
            @if($user->is_me)
                <span 
                class="social-link" 
                    data-type="text" 
                    data-name="social.stackoverflow"
                    data-placeholder="Stack Overflow" 
                    data-emptytext="">{{@$user->social->stackoverflow}}</span>
            @endif
        </li>
        @endif
        @if(@$user->social->facebook || $user->is_me)
        <li>
            <a target="_blank" href="{{@$user->social->facebook}}" class="btn btn-xs btn-facebook link-facebook">
                <i class="fa fa-facebook"></i>
            </a>
            @if($user->is_me)
                <span 
                class="social-link" 
                    data-type="text" 
                    data-name="social.facebook"
                    data-placeholder="Facebook" 
                    data-emptytext="">{{@$user->social->facebook}}</span>
            @endif
        </li>
        @endif
        @if(@$user->social->twitter || $user->is_me)
        <li>
            <a target="_blank" href="{{@$user->social->twitter}}" class="btn btn-xs btn-twitter link-twitter">
                <i class="fa fa-twitter"></i>
            </a>
            @if($user->is_me)
                <span 
                class="social-link" 
                    data-type="text" 
                    data-name="social.twitter"
                    data-placeholder="Twitter" 
                    data-emptytext="">{{@$user->social->twitter}}</span>
            @endif
        </li>
        @endif
        @if(@$user->social->google_plus || $user->is_me)
        <li>
            <a target="_blank" href="{{@$user->social->google_plus}}" class="btn btn-xs btn-google-plus link-google-plus">
                <i class="fa fa-google-plus"></i>
            </a>
            @if($user->is_me)
                <span 
                class="social-link" 
                    data-type="text" 
                    data-name="social.google_plus"
                    data-placeholder="Google Plus" 
                    data-emptytext="">{{@$user->social->google_plus}}</span>
            @endif
        </li>
        @endif
        @if(@$user->social->pinterest || $user->is_me)
        <li>
            <a target="_blank" href="{{@$user->social->pinterest}}" class="btn btn-xs btn-pinterest link-pinterest">
                <i class="fa fa-pinterest"></i>
            </a>
            @if($user->is_me)
                <span 
                class="social-link"
                    data-type="text"
                    data-name="social.pinterest"
                    data-placeholder="Pinterest" 
                    data-emptytext="">{{@$user->social->pinterest}}</span>
            @endif
        </li>
        @endif
        @if(@$user->social->behance || $user->is_me)
        <li>
            <a target="_blank" href="{{@$user->social->behance}}" class="btn btn-xs btn-behance link-behance">
                <i class="fa fa-behance"></i>
            </a>
            @if($user->is_me)
                <span 
                class="social-link"
                    data-type="text"
                    data-name="social.behance"
                    data-placeholder="Behance" 
                    data-emptytext="">{{@$user->social->behance}}</span>
            @endif
        </li>
        @endif
        @if(@$user->social->instagram || $user->is_me)
        <li>
            <a target="_blank" href="{{@$user->social->instagram}}" class="btn btn-xs btn-instagram link-instagram">
                <i class="fa fa-instagram"></i>
            </a>
            @if($user->is_me)
                <span 
                class="social-link"
                    data-type="text"
                    data-name="social.instagram"
                    data-placeholder="Instagram" 
                    data-emptytext="">{{@$user->social->instagram}}</span>
            @endif
        </li>
        @endif
        @if(@$user->social->dribbble || $user->is_me)
        <li>
            <a target="_blank" href="{{@$user->social->dribbble}}" class="btn btn-xs btn-dribbble link-dribbble">
                <i class="fa fa-dribbble"></i>
            </a>
            @if($user->is_me)
                <span 
                class="social-link"
                    data-type="text"
                    data-name="social.dribbble"
                    data-placeholder="Dribbble" 
                    data-emptytext="">{{@$user->social->dribbble}}</span>
            @endif
        </li>
        @endif
    </ul>
</div>
<div class="frame" v-show="navigation.current_page == 'resume'">
    @if($user->about || $user->video || @$user->is_me)
    <div class="resume-group" id="competencias">
        <div class="row">
            <div class="container-fluid">
                <h4 class="system-title">
                    <i class="system-title-icon fa fa-file-text-o"></i>
                    Biografia
                </h4>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-22 col-sm-offset-2">
                @if($user->about || @$user->is_me)
                <div 
                    class="about-container" 
                    data-type="textarea" 
                    data-name="about"
                    data-placeholder="Escreva algo sobre você" 
                    data-emptytext="Escreva algo sobre você">{!!$user->is_me ? strip_tags(@$user->about) : @$user->about!!}</div>
                @endif
                <br>
                @if($user->video || @$user->is_me)
                    <div class="user-about-video-wrapper">
                        @if($user->video)
                            @include('painel.users.includes.user-video')
                        @endif
                    </div>

                    @if(@$user->is_me)
                        <div>
                            <br>
                            <h5 class="title">URL do seu vídeo de apresentação no YouTube</h5>
                            <div 
                                class="user-video-input" 
                                data-type="text" 
                                data-name="video"
                                data-placeholder="Insira a URL do seu vídeo no Youtube" 
                                data-emptytext="Insira a URL do seu vídeo no Youtube">
                                @if(!$user->video)
                                    <i class="fa fa-youtube-play"></i>
                                @else
                                    {{$user->video}}
                                @endif
                            </div>
                        </div> 
                    @endif
                @endif
            </div>
        </div>
    </div>
    @endif
    @if( $user->is_me || $user->skills->count() )
    <div class="resume-group" id="competencias">
        <div class="row">
            <div class="container-fluid">
                <h4 class="system-title">
                    <i class="system-title-icon fa fa-trophy"></i>
                    Competências
                </h4>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-22 col-sm-offset-2">
                <div class="skill-tags"  v-show="!showTagsInput">
                    <div class="row recommended-skills" v-show="skills.length">
                        <div class="container-fluid">
                            <div class="recommendedskills-container">
                                <div class="skill-item" v-for="skill in skills" v-if="skill.recommendations.length > 0">
                                    <div class="tag label label-info">
                                        @{{skill.tag.name}}
                                    </div>
                                    <div class="pull-right recommendators">
                                        @if(Auth::check() && !$user->is_me)
                                            @if(Auth::user()->id != $user->id)
                                            <button v-on:click="skillsIVoted.indexOf(skill.id) == -1 ? recommend(skill) : ''" class="btn btn-xs btn-danger btn-recommend">
                                                <i class="fa fa-plus"></i>
                                            </button>

                                            <button v-if="skillsIVoted.indexOf(skill.id) != -1" v-on:click="recommend(skill)" class="btn btn-xs btn-primary btn-disrecommend">
                                                <i class="fa fa-remove"></i> Desfazer recomendação
                                            </button>
                                            @endif
                                        @endif
                                        <img v-for="recom in skill.recommendations" src="@{{recom.user.thumb}}" alt="" class="img-responsive">
                                    </div>
                                    <div class="line"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row normal-skills" v-show="skills.length">
                        <div class="container-fluid">
                            <h5 class="title" v-if="hasRecommendedSkills & hasNormalSkills">Outras competências</h5>
                            <div class="skills-tags-container">
                                @if(Auth::check() && !$user->is_me)
                                    <div class="input-group skill-item" v-for=" skill in skills" v-if="skill.recommendations.length == 0">
                                        <span class="tag label label-info input-group-addon" id="skill-@{{$skill->id}}">
                                            @{{skill.tag.name}}
                                        </span>
                                        @if(Auth::user()->id != $user->id)
                                            <button v-if="skillsIVoted.indexOf(skill.id) == -1" v-on:click="recommend(skill)" type="button" class="btn btn-danger btn-xs" aria-describedby="skill-@{{skill.id}}">
                                                <i class="fa fa-plus"></i>
                                            </button>
                                        @endif
                                    </div>
                                @else
                                    <div class="tag label label-info" v-for="skill in skills" v-if="skill.recommendations.length == 0">
                                        @{{skill.tag.name}}
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>

                    @if(@$user->is_me)
                    <div class="row">
                        <div class="container-fluid">
                            <button v-on:click="showTagsInput = true" class="btn btn-sm btn-default btn-edit-skills">
                                <span class="fa fa-pencil"></span>
                                Editar
                            </button>
                        </div>
                    </div>
                    @endif
                </div>

                @if(@$user->is_me)
                    <div class="skills-edit-container" v-show="showTagsInput">
                        <div class="row">
                            <div class="container-fluid">
                                <select class="skill-tags-input" multiple="multiple">
                                    @if(count($user->skills))
                                        @foreach($user->skills as $skill)
                                        <option value="{{$skill->tag->id}}">{{$skill->tag->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                                <button v-on:click="saveSkills($event)" type="button" data-loading-text="Salvando..." class="btn btn-danger btn-sm btn-save-skills">
                                    <span class="fa fa-save"></span>
                                     Salvar
                                </button>
                                <button v-on:click="showTagsInput = false" type="button" class="btn btn-default btn-sm btn-cancel-edit-skills">
                                     Cancelar
                                </button>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
    @endif

    @if( $user->is_me || $user->educations->count() )
    <!-- EDUCAÇÃO -->
    <div class="resume-group" id="educacao">
        <div class="row">
            <div class="container-fluid">
                <h4 class="system-title">
                    <i class="system-title-icon fa fa-graduation-cap"></i>
                    Educação
                </h4>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-22 col-sm-offset-2">
                @if(@$user->is_me)
                    <div class="resume-item">    
                        <button class="btn btn-sm btn-link btn-block btn-add-resume-item">
                            <span class="fa fa-plus"></span>
                            Adicionar curso
                        </button>
                        <div class="resume-item-form-content hidden">
                            @include('painel.users.includes.education.form-education')
                        </div>
                    </div>
                @endif
                @forelse($user->educations as $education)
                    @include('painel.users.includes.education.resume-education')
                @empty
                @endforelse
            </div>
        </div>
    </div>
    <!-- END EDUCAÇÂO -->
    @endif

    @if( $user->is_me || $user->jobs->count() )
    <!-- EXPERIÊNCIA -->
    <div class="resume-group" id="trabalho">
        <div class="row">
            <div class="container-fluid">
                <h4 class="system-title">
                    <i class="system-title-icon fa fa-suitcase"></i>
                    Experiência
                </h4>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-22 col-sm-offset-2">
                @if(@$user->is_me)
                    <div class="resume-item">    
                        <button class="btn btn-sm btn-link btn-block btn-add-resume-item">
                            <span class="fa fa-plus"></span>
                            Adicionar trabalho
                        </button>
                        <div class="resume-item-form-content hidden">
                            @include('painel.users.includes.job.form-job')
                        </div>
                    </div>
                @endif
                @forelse($user->jobs as $job)
                    @include('painel.users.includes.job.resume-job')
                @empty
                @endforelse
            </div>
        </div>
    </div>
    <!-- END EXPERIÊNCIA -->
    @endif

    @if( $user->is_me || $user->languages->count() )
    <!-- IDIOMAS -->
    <div class="resume-group" id="idiomas">
        <div class="row">
            <div class="container-fluid">
                <h4 class="system-title">
                    <i class="system-title-icon fa fa-globe"></i>
                    Idiomas
                </h4>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-22 col-sm-offset-2">
                @if(@$user->is_me)
                    <div class="resume-item">    
                        <button class="btn btn-sm btn-link btn-block btn-add-resume-item">
                            <span class="fa fa-plus"></span>
                            Adicionar idioma
                        </button>
                        <div class="resume-item-form-content hidden">
                            @include('painel.users.includes.language.form-language')
                        </div>
                    </div>
                @endif
                @forelse($user->languages as $language)
                    @include('painel.users.includes.language.resume-language')
                @empty
                @endforelse
            </div>
        </div>
    </div>
    <!-- END IDIOMAS -->
    @endif

    @if( $user->is_me || $user->certificates->count() )
    <!-- CERTIFICADOS -->
    <div class="resume-group" id="certificados">
        <div class="row">
            <div class="container-fluid">
                <h4 class="system-title">
                    <i class="system-title-icon fa fa-certificate"></i>
                    Certificados
                </h4>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-22 col-sm-offset-2">
                @if(@$user->is_me)
                    <div class="resume-item">    
                        <button class="btn btn-sm btn-link btn-block btn-add-resume-item">
                            <span class="fa fa-plus"></span>
                            Adicionar certificado
                        </button>
                        <div class="resume-item-form-content hidden">
                            @include('painel.users.includes.certificate.form-certificate')
                        </div>
                    </div>
                @endif
                @forelse($user->certificates as $certificate)
                    @include('painel.users.includes.certificate.resume-certificate')
                @empty
                @endforelse
            </div>
        </div>
    </div>
    <!-- END CERTIFICADOS -->
    @endif

    @if( $user->is_me || $user->courses->count() )
    <!-- CURSOS -->
    <div class="resume-group" id="cursos">
        <div class="row">
            <div class="container-fluid">
                <h4 class="system-title">
                    <i class="system-title-icon fa fa-book"></i>
                    Cursos
                </h4>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-22 col-sm-offset-2">
                @if(@$user->is_me)
                    <div class="resume-item">    
                        <button class="btn btn-sm btn-link btn-block btn-add-resume-item">
                            <span class="fa fa-plus"></span>
                            Adicionar curso
                        </button>
                        <div class="resume-item-form-content hidden">
                            @include('painel.users.includes.course.form-course')
                        </div>
                    </div>
                @endif
                @forelse($user->courses as $course)
                    @include('painel.users.includes.course.resume-course')
                @empty
                @endforelse
            </div>
        </div>
    </div>
    <!-- END CURSOS -->
    @endif
    
    @if( $user->is_me || $user->portfolios->count() )
    <!-- PORTFOLIO -->
    <div class="resume-group" id="portfolio">
        <div class="row">
            <div class="container-fluid">
                <h4 class="system-title">
                    <i class="system-title-icon fa fa-image"></i>
                    Portfólio
                </h4>
            </div>
        </div>
        <div class="row">
            <div class="container-fluid">
                @if(@$user->is_me)
                    <div class="resume-item">    
                        <button class="btn btn-sm btn-link btn-block btn-add-resume-item">
                            <span class="fa fa-plus"></span>
                            Adicionar projeto
                        </button>
                        <div class="resume-item-form-content hidden">
                            @include('painel.users.includes.portfolio.form-portfolio')
                        </div>
                    </div>
                @endif
                <div class="row">
                    @forelse($user->portfolios as $portfolio)
                        @include('painel.users.includes.portfolio.resume-portfolio')
                    @empty
                    @endforelse
                </div>
            </div>
        </div>
    </div>
    <!-- END PORTFOLIO -->
    @endif

</div>