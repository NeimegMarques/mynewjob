<?php
namespace App\Http\ViewComposers;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;
//use Menu;

class PainelComposer {


    /**
     * Bind data to the view.
     *
     * @param  View $view
     * @return void
     */
    public function compose(View $view)
    {
        $notNotifiedNotifications = \App\Models\Notification::where('notified',0)
            ->where('receiver_id',@\Auth::user()->id)
            ->count();

        // $notreadmessages = \App\Models\Chat::where('read',0)
        //     ->where('receiver_id',@\Auth::user()->id)
        //     ->groupBy('sender_id')
        //     ->count();

        // $view->with('notreadmessages',@$notreadmessages);
        $view->with('notnotifiednotifications',@$notNotifiedNotifications);



        // MANTIVE SÓ DE EXEMPLO

        // $view->with('currentUser', Auth::user());

        // Menu::make('admin', function ($menu)
        // {

        //     $UserCompany = Auth::user()->group->name; // Find user which Company
        //     $menu->add($UserCompany, '/')->data('permissions', ['view_home_page']);
        //     $menu->add('Reseller', '/')->data('permissions', ['view_home_page']);
        //     $menu->add('Users', 'dashboard')->data('permissions', ['view_about_page']);
        //     $menu->add('Log', '/admin')->data('permissions', ['access_admin']);
        //     $menu->add('Configurations', '/admin')->data('permissions', ['access_admin']);
        // })
        // ->filter(function ($item)
        // {
        //     $sat = Auth::check() && Auth::user()->canAtLeast($item->data('permissions')) ?: false;

        //     return ($sat);
        // });
    }
}
