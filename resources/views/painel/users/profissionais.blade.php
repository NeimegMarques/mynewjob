@extends('layouts.app',['page'=>'profissionais-view'])

@section('content')
<section class="user-content">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                @include('painel.users.includes._sidebar-search')
            </div>
            <div class="col-sm-18">
                <div class="row">
                    <div class="container-fluid">
                        @forelse($users as $i => $user)
                            @if($i == 0 || $i%2 == 0 )
                                @if($i > 0)
                                    </div>
                                @endif
                                <div class="row row-eq-height">
                            @endif
                            <div class="col-sm-12 mb20">
                                @include('painel.users.includes._card')
                            </div>
                        @empty
                            <p class="well well-sm text-primary">Nenhum resultado encontrado</p>
                        @endforelse
                        </div>
                    </div>
                    {{ $users->appends(Request::except('page'))->links() }}
                </div>
            </div>
        </div>
    </div>
</section>
@if(@$user->is_me)
</component>
@endif
@endsection