<meta name="application-name" content="MyNewJob">
<meta name="msapplication-TileColor" content="#2c3e50">
<meta name="apple-mobile-web-app-title" content="MyNewJob">
<link rel="shortcut icon" href="{{asset('/favicon.ico')}}">
<link rel="apple-touch-icon" sizes="180x180" href="{{asset('/apple-touch-icon.png')}}">
<link rel="icon" type="image/png" href="{{asset('/favicon-32x32.png')}}" sizes="32x32">
<link rel="icon" type="image/png" href="{{asset('/favicon-16x16.png')}}" sizes="16x16">
<link rel="manifest" href="{{asset('/manifest.json')}}">
<link rel="mask-icon" href="{{asset('/safari-pinned-tab.svg')}}" color="#5bbad5">
<meta name="theme-color" content="#2c3e50">