<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Tag;

class TagsController extends Controller
{
    public function getByQueryWithFake($query){

    	$tag = Tag::where('tag',$query)->orWhere('name',$query)->limit(10)->first();

    	$tags = Tag::where('tag','LIKE','%'.$query.'%')->get();

    	$tags = $tags->map(function($item){
    		return $item->setAttribute('str_id',$item->id);
    	});

    	if(!$tag)
    	{
    		$free = new Tag;
	    	$free->name = $query;
	    	$free->tag = str_slug($query);
	    	$free->str_id = $query; // SEM slug PORQUE PRECISO RETORNAR EXATAMENTE COMO O USUÁRIO DIGITOU
                                    // NA HORA DE SALVAR QUE EU FAÇO O slug

	    	$tags->add($free);
    	}

    	if(\Request::json()){
    		return $tags->toJson();
    	}
    }
}
