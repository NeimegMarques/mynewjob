<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGithubBitbucketAndStackoverflowToSociallinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sociallinks', function (Blueprint $table) {
            $table->string('github')->after('instagram')->nullable();
            $table->string('bitbucket')->after('instagram')->nullable();
            $table->string('stackoverflow')->after('instagram')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sociallinks', function (Blueprint $table) {
            $table->dropColumn('github')->after('instagram');
            $table->dropColumn('bitbucket')->after('instagram');
            $table->dropColumn('stackoverflow')->after('instagram');
        });
    }
}
