@extends('layouts.front',[
        'page'=>'address',
        'props'=>[]
    ])

@section('content')
<section class="callout">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-md-offset-6">
                {{Form::open(['url'=>url('info/address'),'class'=>'form-horizontal', 'role'=>'form'])}}
                    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                        <div class="col-sm-24">
                            <h4>Informe seu CEP para localizarmos as melhores vagas para você</h4>
                            <input v-bind:disabled="!cepInputEnabled" v-model="cep" placeholder="CEP" type="text" name="zip_code" class="info-cep form-control" />
                        </div>
                    </div>
                    <div class="info-input-container" v-if="showAddressInput">
                        <div class="form-group" v-if="cepError">
                            <div class="container-fluid">
                                <div style="margin-top:20px;" class="alert alert-warning small alert-square location-error text-danger">
                                    <i class="fa fa-warning"></i>@{{cepError}}
                                </div>
                            </div>
                        </div>
                        <div class="form-group" v-if="hint">
                            <div class="container-fluid">
                                <div style="margin-top:20px;" class="alert alert-info small alert-square">
                                    <i class="fa fa-info"></i>@{{hint}}
                                </div>
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                            <div class="container-fluid">
                                <input
                                id="addressInput"
                                v-model="formated_address"
                                value="@{{
                                    formated_address
                                }}"
                                placeholder="Nome da rua, Número, Bairro, Cidade, Estado" tabindex="1" type="text" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div v-if="loading" class="form-group info-loader text-center">
                        <img src="{{asset('/img/loading-for-dark.gif')}}" />
                    </div>
                    <div class="frame address-result text-primary" v-if="addr.coordenadas.lat != '' && addr.coordenadas.lon != ''">
                        <input class="hidden" type="hidden" name="lat" value="@{{addr.coordenadas.lat}}" autocomplete="false">
                        <input class="hidden" type="hidden" name="lng" value="@{{addr.coordenadas.lon}}" autocomplete="false">
                        <input class="hidden" type="hidden" name="street" value="@{{addr.logradouro}}" autocomplete="false">
                        <input class="hidden" type="hidden" name="neighborhood" value="@{{addr.bairro}}"  autocomplete="false">
                        <input class="hidden" type="hidden" name="city" value="@{{addr.cidade}}" autocomplete="false">
                        <input class="hidden" type="hidden" name="state" value="@{{addr.estado}}" autocomplete="false">
                        <input class="hidden" type="hidden" name="number" value="@{{addr.numero}}" autocomplete="false">
                        <input class="hidden" type="hidden" name="country" value="@{{addr.pais}}" autocomplete="false">
                        <div class="form-group">
                            <h4>@{{formated_address}}</h4>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-18">
                                    <label for="">Rua</label>
                                    <input class="form-control" id="streetInput" value="@{{addr.logradouro}}" type="text" name="strt" autocomplete="false">
                                </div>
                                <div class="col-sm-6">
                                    <label for="">Número</label>
                                        <input class="form-control" id="numberInput" value="@{{addr.numero}}" type="text" name="number" autocomplete="false">
                                    </div>
                            </div>
                        </div>
                        <div class="form-group" v-show="askedForComplement">
                            <label for="">Algum complemento?</label>
                            <input class="form-control" id="complementInput" type="text" v-model="complement" name="complement" autocomplete="false" placeholder="Apartamento, Bloco, etc.">
                        </div>
                        <div class="form-group text-primary">
                            <hr />

                            <button v-if="!askedForComplement" v-on:click="askedForComplement = true" type="button" class="btn btn-danger pull-right">
                                Prosseguir <i class="fa fa-btn fa-chevron-right"></i>
                            </button>
                            <button v-if="askedForComplement" type="submit" class="btn btn-danger pull-right">
                                Prosseguir <i class="fa fa-btn fa-chevron-right"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<div>
    <div id="map" style="height: 500px;"></div>
</div>
@endsection

@push('script-head')
<script src="https://maps.googleapis.com/maps/api/js?key={{env('MAPS_API_KEY')}}&language=pt-BR"
    async defer></script>
@endpush
